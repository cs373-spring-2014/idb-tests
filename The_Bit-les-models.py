from django.db import models

class Album(models.Model):
	'''
	Album - contains information about the album. It contains information such as
			it's name, tracks in the album, it's genre and other useful links.
	An ID field is used as the primary key. The primary key is the album name.
	albums are identified by their name
	albums have ManyToMany relation with Artist and RecordCompanies
	Urls to Album Images, videos and sellers are a CharField of 255 characers to prevent abuse of
		server resources.
	'''
	name = models.CharField(max_length=128)
	# Artists = albumObject.artist_set.all()  Only the once in database
	# ndbArtists = albumObject.ndbartist_set.all()
	year_released = models.IntegerField()
	# Tracks = albumObject.track_set.all()
	# Genres = albumObject.genre_set.all()
	peak_chart = models.IntegerField()
	length = models.CharField(max_length=50)
	cover = models.CharField(max_length=255)
	# Record companies that produced this album = albumObject.recordcompany_set.all()
	# ndbLabel = albumObject.ndblabel_set.all()
	# Producers = albumObject.member_set.all()
	sales = models.FloatField()
	citations = models.CharField(max_length=255)
	# Artwork = albumObject.image_set.all()
	# MusicVideos = albumObject.video_set.all()
	sellers = models.CharField(max_length=255)
	description = models.TextField(max_length=4096)

	def __str__(self):
		return self.name


class Track(models.Model):
	'''
	Track - contains information about the track inside an album. It has ManytoOne relation with Album
	Track name is restricted to 128 characers to prevent abuse of server resources.
	Tracks are identified by their names 
	'''
	track_name = models.CharField(max_length=128) 
	album = models.ForeignKey(Album)

	def __str__(self):
		return self.track_name

class Genre(models.Model):
	'''
	Genre - contains information about the genre of an album. It has ManytoOne relation with Album
	Genre name is restricted to 128 characers to prevent abuse of server resources.
	Genres are identified by their names 
	'''
	genre_type = models.CharField(max_length=128)
	album = models.ForeignKey(Album)

	def __str__(self):
		return self.genre_type


# class Producer(models.Model):
# 	'''
# 	Producer - contains information about the producers of an album. It has ManytoOne relation with Album
# 	Producer name is restricted to 128 characers to prevent abuse of server resources.
# 	Producers are identified by their names 
# 	'''
# 	name = models.CharField(max_length=128)
# 	album = models.ForeignKey(Album)

# 	def __str__(self):
# 		return self.name

class Artist(models.Model):
	'''
	Artist - contains information about the artist or the band. If it's a band then 
			 members field will contain all band members names.
	Artist can be a solo artist or a Band.
	An ID field is used as the primary key. The primary key is the artist name.
	Artists are identified by their name
	Artist have ManyToMany relation with Album and RecordCompanies
	Urls to Artist Images, facebook_link and twitter_link are a CharField of 255 characers to prevent abuse of
		server resources.
	'''
	band_name = models.CharField(max_length=255)
	# Members = artistObject.member_set.all()
	hometown = models.CharField(max_length=255)
	years_active = models.CharField(max_length=30)
	#start_year = models.IntegerField()
	#end_year = models.IntegerField()
	albums = models.ManyToManyField(Album, verbose_name="Albums produced")
	# to get a list of all albums (albums = ArtistObject.albums.all())
	# ndbAlbums = AO.ndbalbum_set.all()
	# Record companies they worked with = AO.recordcompany_set.all()
	# ndbRecordCompanies = AO.ndblabel_set.all()
	imagel = models.CharField(max_length=255)
	# Photos = artistObject.image_set.all()
	# ArtistVideos = artistObject.video_set.all()
	# AlbumTiles = artistObject.tiles_set.all()
	facebook = models.CharField(max_length=255)
	twitter = models.CharField(max_length=255)
	google_map = models.CharField(max_length=255)
	citations = models.CharField(max_length=255)
	description = models.TextField(max_length=4096)

	def __str__ (self):
		return self.band_name

# class ArtistVideo(models.Model):
# 	'''
# 	Artist Video - generally a music video of an artist's performance. It has a ManytoOne relation with Artist
# 	YouTube link is restricted to 255 characers to prevent abuse of server resources.
# 	Artist videos are identified by their links
# 	'''
# 	youtube_link = models.CharField(max_length=255)
# 	artist = models.ForeignKey(Artist)

# 	def __str__(self):
#  		return self.youtube_link

# class Photo(models.Model):
# 	'''
# 	Photo - a photo of the band or artist. It has a ManytoOne relation with Artist
# 	Photo link is restricted to 255 characers to prevent abuse of server resources.
# 	Photos are identified by the link to the images
# 	'''
# 	photo_link = models.CharField(max_length=255)
# 	artist = models.ForeignKey(Artist)

# 	def __str__(self):
# 		return self.photo_link

class Tile(models.Model):
	'''
	Tile - the cover image of each album the artist has produced. It has a ManyToOne releation with Artist
	Tile link is restricted to 255 characers to prevent abuse of server resources.
	Tiles are identified by the link to the images
	'''
	tile_link = models.CharField(max_length=255)
	artist = models.ForeignKey(Artist)

	def __str__(self):
		return self.tile_link

class RecordCompany(models.Model):
	'''
	RecordCompany - contains information about the RecordCompany. It contains
					  information such as it's name, founders, when was it founded
					  it's location and other useful information about the RecordCompany.
	An ID field is used as the primary key. The primary key is the RecordCompany name.
	RecordCompanies are identified by their name
	RecordCompany have ManyToMany relation with Album and Artist
	Urls to RecordCompany Images, facebook_link and twitter_link are a CharField of 255 characers to prevent abuse of
		server resources.
	'''
	name = models.CharField(max_length=128)
	albums = models.ManyToManyField(Album, verbose_name="Albums produced")
	# To Get all the albums from database Company created = RCO.albums.all()
	# ndbAlbums = RCO.ndbalbum_set.all()
	artists = models.ManyToManyField(Artist, verbose_name="Artists worked with")
	# To Get all the artists from database Company worked with = RCO.artists.all()
	# ndbArtists = RCO.ndbartist_set.all()
	founded = models.IntegerField()
	# Founders = RecordCompanyObject.member_set.all()
	location = models.CharField(max_length=255)
	map_link = models.CharField(max_length=255)
	parent_company = models.CharField(max_length=128)
	logo = models.CharField(max_length=255)
	website = models.CharField(max_length=255)
	citations = models.CharField(max_length=255)
	# Pictures = RecordCompanyObject.image_set.all()
	# LabelVideos = RecordCompanyObject.video_set.all()
	facebook = models.CharField(max_length=255)
	twitter = models.CharField(max_length=255)
	description = models.TextField(max_length=4096)

	def __str__(self):
		return self.name

# class Founder(models.Model):
# 	'''
# 	Founder - contains information about the founders of a RecordCompany. It has ManytoOne relation with RecordCompany
# 	Founder name is restricted to 128 characers to prevent abuse of server resources.
# 	Founders are identified by their names 
# 	'''
# 	name = models.CharField(max_length=128)
# 	record_company = models.ForeignKey(RecordCompany)

# 	def __str__(self):
# 		return self.name

# class LabelVideo(models.Model):
# 	'''
# 	Label Video - a video promoted by a label, whether actually music or not. It has a ManytoOne relation with RecordCompany
# 	YouTube link is restricted to 255 characers to prevent abuse of server resources.
# 	Label videos are identified by their links
# 	'''
# 	youtube_link = models.CharField(max_length=255)
# 	record_company = models.ForeignKey(RecordCompany)

# 	def __str__(self):
#  		return self.youtube_link

# class Picture(models.Model):
# 	'''
# 	Picture - artwork from the album such as the front cover photo. It has a ManytoOne relation with RecordCompany
# 	Picture link is restricted to 255 characers to prevent abuse of server resources.
# 	Pictures are identified by the link to the images
# 	'''
# 	picture_link = models.CharField(max_length=255)
# 	record_company = models.ForeignKey(RecordCompany)

# 	def __str__(self):
# 		return self.picture_link


class Ndbartist(models.Model):
	'''
	Ndbartist - contains information about the non database artist. It has ManytoOne relation with Album or RecordCompany
	Ndbartist name is restricted to 128 characers to prevent abuse of server resources.
	Ndbartist are identified by their names
	'''
	name = models.CharField(max_length = 128)
	album = models.ForeignKey(Album, blank = True, null = True, verbose_name = "Related Album")
	label = models.ForeignKey(RecordCompany, blank = True, null = True, verbose_name = "Related Record Company")

	def __str__(self):
		return self.name

class Ndbalbum(models.Model):
	'''
	Ndbalbum - contains information about the non database album. It has ManytoOne relation with Artist or RecordCompany
	Ndbalbum name is restricted to 128 characers to prevent abuse of server resources.
	Ndbalbum are identified by their names
	'''
	name = models.CharField(max_length = 128)
	artist = models.ForeignKey(Artist, blank = True, null = True, verbose_name = "Related Album")
	label = models.ForeignKey(RecordCompany, blank = True, null = True, verbose_name = "Related Record Company")

	def __str__(self):
		return self.name

class Ndblabel(models.Model):
	'''
	Ndblabel - contains information about the non database Record Company. It has ManytoOne relation with Artist or Album
	Ndblabel name is restricted to 128 characers to prevent abuse of server resources.
	Ndblabel are identified by their names
	'''
	name = models.CharField(max_length = 128)
	artist = models.ForeignKey(Artist, blank = True, null = True, verbose_name = "Related Album")
	album = models.ForeignKey(Album, blank = True, null = True, verbose_name = "Related Record Company")

	def __str__(self):
		return self.name

class Member(models.Model):
	'''
	Member - contains information about the band members if connected to Artist, the Producer if connected to 
			Album and founder if connected to Record Company. It has ManytoOne relation with Artist
	Member name is restricted to 128 characers to prevent abuse of server resources.
	Members are identified by their names 
	'''
	name = models.CharField(max_length=128)
	artist = models.ForeignKey(Artist, blank = True, null = True, verbose_name = "Band Member" )
	album = models.ForeignKey(Album, blank = True, null = True, verbose_name = "Producer of Album" )
	label = models.ForeignKey(RecordCompany, blank = True, null = True, verbose_name = "Founder of Record Company" )

	def __str__(self):
		return self.name

class Video(models.Model):
	'''
	Music Video - a music video of a song from the album set, or video that artist created or a ad video for a record Company.
				 It has a ManytoOne relation with Album and/or RecordCompany and/or Artist
	YouTube link is restricted to 255 characers to prevent abuse of server resources.
	Music videos are identified by their links
	'''
	youtube_link = models.CharField(max_length=255)
	artist = models.ForeignKey(Artist, blank = True, null = True, verbose_name = "Artist video" )
	album = models.ForeignKey(Album, blank = True, null = True, verbose_name = "Album Video" )
	label = models.ForeignKey(RecordCompany, blank = True, null = True, verbose_name = "Record Company Video" )

	def __str__(self):
		return self.youtube_link

class Image(models.Model):
	'''
	Artwork - artwork from the album such as the front cover photo. It has a ManytoOne relation with Album
	Artwork link is restricted to 255 characers to prevent abuse of server resources.
	Artwork is identified by the link to the images
	'''
	artwork_link = models.CharField(max_length=255)
	artist = models.ForeignKey(Artist, blank = True, null = True, verbose_name = "Artist image" )
	album = models.ForeignKey(Album, blank = True, null = True, verbose_name = "Album image" )
	label = models.ForeignKey(RecordCompany, blank = True, null = True, verbose_name = "Record Company image" )

	def __str__(self):
		return self.artwork_link
