#!/usr/bin/env python3

from django.test import TestCase, LiveServerTestCase
from urllib.request import urlopen, Request
from json import dumps, loads
from urllib.error import HTTPError
from sxsw.models import *
from django.db.utils import IntegrityError
from sxsw.views import *


RESPONSE_1 = { "id": 2 }

CHILDISH_GAMBINO = {
    'website': 'http://iamdonald.com/',
    'origin': 'United States', 
    'name': 'Childish Gambino', 
    'instruments': ['bass', 'drums', 'guitar', 'sampler', 'sequencer', 'synthesizer', 'vocals'], 
    'twitter': 'DonaldGlover', 
    'labels': ['Glass Note', 'Island'], 
    'youtube': 'ChildishGambinoVEVO', 
    'year_established': 2002, 
    'facebook': 'iamdonald', 
    'members': ['Donald Glover'], 
    'genre': 'Hip Hop', 
    'id': 1,
    'video': 'http://www.youtube.com/embed/qL1B_r9nC9k?rel=0', 
    'twitter_embed': '<a class="twitter-timeline"  href="https://twitter.com/DonaldGlover"  data-widget-id="450766307894362112">Tweets by @DonaldGlover</a>',
    'image': 'http://hypetrak.com/images/2013/10/childish-gambino-3005.jpg',
    'sponsors': [1],
    'events': [1]
}


CHILDISH_GAMBINO_SMALL = [ {
        "genre": "Hip Hop",
        "id": 1,
        "name": "Childish Gambino",
        "members":["Donald Glover"],
        "sponsors": [1],
        "events": [1]
    }
]


YOUNG_AND_SICK= {

    "image": "http://cdn.thefader.com/wp-content/uploads/2012/11/mmoths_1-hi-res.jpg",
    "members": ["Nick"],
    "origin": "Los Angeles, California, United States",
    "facebook": "youngandsick",
    "youtube": "YoungandSickVEVO", 
    "labels": ["Harvest Records"], 
    "id": 2, 
    "sponsors": [2, 3, 6, 4, 1], 
    "twitter": "", 
    "events": [3, 5, 1], 
    "genre": "R&B", 
    "instruments": ["drums", "synthesizer", "sequencer", "sampler"], 
    "year_established": 2011, 
    "name": "Young & Sick", 
    "website": "http://youngandsick.com/", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/search?q=%23YoungAndSick\"  data-widget-id=\"451136331704324096\">Tweets about \"#YoungAndSick\"</a>", 
    "video": "http://www.youtube.com/embed/WcA63vBqrgQ?rel=0"
}

YOUNG_AND_SICK_SMALL=[ { "genre": "R&B",
        "id": 2,
        "name": "Young & Sick",
        "members":["Nick"],
        "events": [3, 5, 1],
        "sponsors": [2, 3, 6, 4, 1]
    }
]

CASHMERE_CAT={

    "image": "http://3.bp.blogspot.com/-hb5nM37dVZ4/UoKELOzicdI/AAAAAAAAAPg/zvAUTew_jEs/s1600/cashmere+cat.png", 
    "members": ["Magnus August Hoiberg"], 
    "origin": "Halden, Norway", 
    "facebook": "cashmerecatofficial", 
    "youtube": "thisisluckyme", 
    "labels": ["Pelican Fly", "Lucky Me Records"], 
    "id": 3, "sponsors": [4, 1], 
    "twitter": "CASHMERECAT", 
    "events": [1], 
    "genre": "Electronic", 
    "instruments": ["drums", "synthesizer", "sequencer", "vocals"], 
    "year_established": 2003, 
    "name": "Cashmere Cat", 
    "website": "https://soundcloud.com/cashmerecat", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/CASHMERECAT\"  data-widget-id=\"451136189391577088\">Tweets by @CASHMERECAT</a>", 
    "video": "http://www.youtube.com/embed/be-LbSaEzZQ?rel=0"
}

CASHMERE_CAT_SMALL=[{   "genre": "Electronic",
        "id": 3,
        "name": "Cashmere Cat",
        "members": ["Magnus August Hoiberg"],
        "sponsors": [4, 1], 
        "events": [1]
    }
]

DELL = {
    "website": "http://dell.com", 
    "established": "1984-11-04", 
    "name": "Dell", 
    "twitter": "Dell", 
    "CEO": "Michael Dell", 
    "industry": "technology", 
    "events": [1],
    "artists": [1],
    "id": 1,
    "map_url": "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=CTmu5Wtl0o53FS7iBQIdp3D1-CmRIggeTMzCgDEKnXgWJqdAkQ&amp;q=Dell+loc:+Round+Rock,+Texas,+United+States&amp;aq=&amp;sll=33.930229,-118.135&amp;sspn=0.127475,0.264187&amp;ie=UTF8&amp;hq=Dell&amp;hnear=Round+Rock,+Williamson+County,+Texas&amp;t=m&amp;z=14&amp;iwloc=A&amp;cid=11183683824276853971&amp;ll=30.48435,-97.665764&amp;output=embed", 
    "facebook": "Dell", 
    "location": "Round Rock, Texas, United States", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/Dell\"  data-widget-id=\"450787358749638656\">Tweets by @Dell</a>", 
    "image": "http://upload.wikimedia.org/wikipedia/commons/4/48/Dell_Logo.svg"
}

DELL_SMALL = [
    {
        "name": "Dell",
        "id": 1, 
        "industry": "technology",
        "events": [1],
        "artists": [1]
    }
]

TACO_BELL={

    "image": "http://reasonablediet.com/wordpress/wp-content/uploads/2012/02/taco-bell-logo.jpeg", 
    "map_url": "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=CTmu5Wtl0o53FS7iBQIdp3D1-CmRIggeTMzCgDEKnXgWJqdAkQ&amp;q=taco+bell+loc:+Downey,+CA&amp;aq=&amp;sll=33.657208,-117.746478&amp;sspn=1.023049,2.113495&amp;ie=UTF8&amp;hq=taco+bell&amp;hnear=&amp;t=m&amp;ll=33.930229,-118.135&amp;spn=0.099172,0.125141&amp;output=embed", 
    "facebook": "tacobell", 
    "id": 1, 
    "artists": [1, 2, 3], 
    "twitter": "TacoBell", 
    "events": [1, 2], 
    "website": "http://www.tacobell.com/", 
    "CEO": "Greg Creed", 
    "name": "Taco Bell", 
    "location": "Downey, California, United States", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/TacoBell\"  data-widget-id=\"451136008268939265\">Tweets by @TacoBell</a>", 
    "industry": "food", 
    "established": "1962-03-21"
}

TACO_BELL_SMALL = [
    {
        "name": "Taco Bell",
        "id": 2, 
        "industry": "food",
        "events": [1, 2],
        "artists": [1, 2, 3]
    }
]

CONVERSE= {

    "image": "http://upload.wikimedia.org/wikipedia/commons/3/30/Converse_logo.svg", 
    "map_url": "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=converse+loc:+North+Andover,+Massachusetts,+United+States&amp;aq=&amp;sll=30.506039,-97.654479&amp;sspn=0.066185,0.132093&amp;ie=UTF8&amp;hq=converse&amp;hnear=North+Andover,+Essex+County,+Massachusetts&amp;t=m&amp;ll=42.700772,-71.13022&amp;spn=0.006295,0.011609&amp;output=embed", 
    "facebook": "converse", 
    "id": 3, "artists": [5, 2, 7], 
    "twitter": "Converse", 
    "events": [3], 
    "website": "http://www.converse.com", 
    "CEO": "Jim Calhoun", 
    "name": "Converse", 
    "location": "North Andover, Massachusetts, United States", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/Converse\"  data-widget-id=\"451135912978546688\">Tweets by @Converse</a>", 
    "industry": "apparel", 
    "established": "1908-02-01"
}

CONVERSE_SMALL = [
    {
        "name": "Converse",
        "id": 3, 
        "industry": "apparel",
        "artists": [5, 2, 7],
        "events": [3]
    }
]



WOODIE = {
    "location_name": "Red River Stage",
    "id": 1,
    "sponsors":[1],
    "artists":[1],
    "name": "Woodie Awards", 
    "image": "http://www.cmj.com/wp-content/uploads/2014/03/Woodie-Award-mtvU-330x330.jpg", 
    "location_address": "Red River & E 1st St, Austin, Texas", 
    "map_url": "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Red+River+%26+E+1st+St,+Austin,+Texas&amp;aq=&amp;sll=30.264584,-97.741446&amp;sspn=0.016587,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=Red+River+St+%26+E+1st+St,+Austin,+Texas&amp;t=m&amp;z=14&amp;ll=30.261806,-97.738812&amp;output=embed", 
    "date": "2014-03-16", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/search?q=%23WoodieAwards\"  data-widget-id=\"451133957996699648\">Tweets about \"#WoodieAwards\"</a>", 
    "type": "outdoor", 
    "description": "MTV's Woodie Festival is back. Plan for an unforgettable musical lineup, free food, drinks and surprises! Come nightfall the Woodie Awards will take over the event for an epic night of music performances."
}

WOODIE_SMALL = [
   {
        "name": "Woodie Awards",
        "id": 1,
        "date": "2014-03-16",
        "sponsors":[1],
        "artists":[1]
    }
]

HYPE_HOTEL={
    "type": "indoor", 
    "image": "http://static.hypem.net/rev_1393347163/images/sxsw2014/rsvp_header.png", 
    "description": "5 days of music picked by awesome blogs.", 
    "date": "2014-03-11", 
    "map_url": "https://maps.google.com/maps?q=3rd+and+San+Jacinto,+Austin,+TX&amp;ie=UTF8&amp;hq=&amp;hnear=San+Jacinto+Blvd+%26+E+3rd+St,+Austin,+Texas+78701&amp;t=m&amp;z=14&amp;ll=30.264584,-97.741446&amp;output=embed", 
    "location_name": "Hype Hotel", 
    "location_address": "3rd and San Jacinto, Austin, TX", 
    "id": 1, 
    "sponsors": [4, 1], 
    "name": "Hype Hotel", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/search?q=%23HypeHotel\"  data-widget-id=\"451135757730578432\">Tweets about \"#HypeHotel\"</a>", 
    "artists": [2, 3]
}

HYPE_HOTEL_SMALL = [
   {
        "name": "Hype Hotel",
        "id": 1,
        "date": "2014-03-11",
        "sponsors": [4, 1],
        "artists": [2, 3]
    }
]

FADER_FORT = {

    "type": "outdoor", 
    "image": "http://posting.tucsonweekly.com/images/blogimages/2014/03/11/1394585572-image.jpg", 
    "description": "Anual four-day party featuring cover stars, first-time performers, and tons of other FADER favorites", 
    "date": "2014-03-12", 
    "map_url": "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=1101+E.+5th+Street,+Austin,+Texas&amp;aq=&amp;sll=30.261806,-97.738812&amp;sspn=0.008294,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=1101+E+5th+St,+Austin,+Texas+78702&amp;ll=30.26181,-97.738817&amp;spn=0.008294,0.016512&amp;t=m&amp;z=14&amp;output=embed", 
    "location_name": "Fader SXSW Installation", 
    "location_address": "1101 E. 5th Street, Austin, Texas", 
    "id": 3, "sponsors": [2, 3], 
    "name": "Fader Fort", 
    "twitter_embed": "<a class=\"twitter-timeline\"  href=\"https://twitter.com/search?q=%23FaderFort\"  data-widget-id=\"451135584841367552\">Tweets about \"#FaderFort\"</a>", 
    "artists": [6, 5, 2]
}

FADER_FORT_SMALL = [
   {
        "name": "Fader_Fort",
        "id": 3,
        "date": "2014-03-12",
        "sponsors": [2, 3], 
        "artists": [6, 5, 2]
    }
]


LONG_TEXT = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer venenatis odio
lectus, sed pharetra est convallis non. Nulla vestibulum massa et enim
scelerisque, sed pretium felis dictum. Sed ac metus id nunc luctus molestie ac
eu est. Duis eget auctor erat, ac hendrerit eros. Morbi sed justo ornare,
consectetur diam eu, malesuada dolor. Donec tempor libero metus, ac volutpat
justo ultrices non. Morbi aliquam viverra arcu, imperdiet dapibus ipsum
sollicitudin ut.

Pellentesque condimentum mi et velit semper imperdiet. Vivamus rhoncus imperdiet
magna, non vulputate sem. Lorem ipsum dolor sit amet, consectetur adipiscing
elit. Duis et tortor ut dui imperdiet ultricies ac eu sapien. Aliquam interdum
augue a dapibus posuere. Donec ac fringilla massa. Morbi a vehicula dolor. In a
neque adipiscing, euismod nisi tempus, tincidunt dui. Sed id feugiat dui,
ullamcorper ultrices lacus. Nullam eu nunc vitae sem varius ornare.

Suspendisse rutrum malesuada nisl, nec ullamcorper nisl volutpat nec. Integer
quis enim pretium, accumsan neque nec, rhoncus lorem. In tempus sem id quam
venenatis, ut accumsan ante facilisis. Aliquam vel lobortis magna. Quisque at
bibendum ante, vel fringilla justo. Donec porttitor, sem id condimentum ornare,
magna turpis fringilla elit, laoreet imperdiet ante urna a dui. Pellentesque
tellus nunc, scelerisque eu odio eu, ultrices condimentum tortor. Cum sociis
natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc
eget faucibus est, id adipiscing magna. Pellentesque congue odio non est
consectetur rutrum. Interdum et malesuada fames ac ante ipsum primis in
faucibus. Nunc eu mi neque. Maecenas pretium odio quis viverra egestas. Vivamus
at magna eget neque mattis cursus. Nullam mauris lorem, sagittis nec sapien a,
porta semper libero.

Integer velit orci, elementum quis mauris vel, mattis fringilla libero.
Suspendisse eget lectus velit. Vestibulum suscipit ligula eget risus congue,
quis tempor mi lobortis. Ut leo lectus, imperdiet varius lacus in, eleifend
venenatis enim. Pellentesque at pellentesque magna, in scelerisque purus.
Quisque pharetra elit nec lorem tincidunt faucibus. Aenean blandit at augue sit
amet ultrices. Donec laoreet ac risus eget tristique. Donec laoreet laoreet
felis vel consequat. Aenean lacinia sapien tempus, luctus quam eu, molestie
nibh. Proin sit amet ultricies libero, nec varius neque. Nam a facilisis dui.
Nulla vel leo sed tortor tincidunt aliquam. Phasellus cursus pulvinar nunc, id
malesuada felis molestie id. Sed condimentum, augue in molestie bibendum, est
est pellentesque tellus, a consectetur orci magna ut urna. Morbi consectetur,
sapien sit amet sodales bibendum, turpis nunc blandit odio, ac pulvinar ante
purus ac nisl.

Morbi faucibus felis ac bibendum accumsan. Nunc congue rhoncus sodales. Cras
quis pellentesque dolor. Aliquam at eleifend orci, sit amet lobortis diam. Duis
pellentesque varius vestibulum. Pellentesque hendrerit mi nec condimentum
tristique. Nullam imperdiet pulvinar euismod. Vestibulum ante ipsum primis in
faucibus orci luctus et ultrices posuere cubilia Curae; Ut eu pellentesque
lorem, non venenatis nibh. Morbi luctus velit eget gravida porta. Vivamus
tincidunt quam lectus, id euismod dui vestibulum vel. Nullam mattis interdum
lectus at suscipit.

Ut quis turpis quis est faucibus placerat sed feugiat odio. Nam nisl dui, cursus
ac vehicula at, ultrices sed augue. Pellentesque vestibulum adipiscing dolor,
sit amet pellentesque massa dignissim vitae. Nulla at ornare elit. Praesent enim
massa, rhoncus vel ante vel, ornare vehicula quam. Nulla cursus nunc justo, in
fermentum elit venenatis a. Integer at blandit velit, eget elementum ante.
Vestibulum laoreet sed nisl porta volutpat. Sed ac vulputate odio, nec ultrices
libero. Fusce ac scelerisque metus, vitae ornare lectus. Nullam vitae risus eu
turpis sodales ultricies a eget nibh. Quisque dictum venenatis urna, sit amet
fermentum lacus tristique non. Cras mollis sit amet est quis tristique. Fusce
sagittis, erat vitae euismod euismod, purus metus laoreet nibh, in euismod orci
velit eu est. Phasellus et enim ac libero pulvinar convallis in ut turpis.
Pellentesque malesuada pulvinar elementum. """

LONGEST_NAME_IN_THE_WORLD = "Adolph Blaine Charles David Earl Frederick Gerald Hubert \
Irvin John Kenneth Lloyd Martin Nero Oliver Paul Quincy Randolph Sherman Thomas Uncas \
Victor William Xerxes Yancy Zeus Wolfe­schlegelstein­hausenberger­dorffvoraltern­waren­gewiss\
enhaft­schaferswessen­schafewaren­wohlgepflege­und­sorgfaltigkeit­beschutzen­von­angreifen­durch­ihr\
raubgierigfeinde­welyche­voraltern­zwolftausend­jahres­vorandieerscheinen­wander­ersteer­dem­enschde\
rrassumschiff­gebrauchlicht­als­sein­ursprung­von­kraftgestart­sein­lange­fahrt­hinzwischen­sternartigr\
aum­auf­der­suchenach­diestern­welche­gehabt­bewohnbar­planeten­kreise­drehen­sich­und­wohin­der­neurasse­vo\
n­verstandigmen­schlichkeit­konnte­fortplanzen­und­sicher­freuen­anlebens­langlich­freude­und­ruhe­mit­ni\
cht­ein­furcht­vor­angreifen­von­anderer­intelligent­geschopfs­von­hinzwischensternartigraumhi, Senior"

LONG_MAP = "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geoc\
ode=&amp;q=1101+E.+5th+Street,+Austin,+Texas&amp;aq=&amp;sll=30.261806,-97.738812&amp;\
sspn=0.008294,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=1101+E+5th+St,+Austin,+Texas+Austin,\
+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+\
Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,\
+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+\
Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+\
Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin\
,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin\
,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+Austin,+Texas+78702&amp;ll=30.26181\
,-97.738817&amp;spn=0.008294,0.016512&amp;t=m&amp;z=14&amp;output=embed"

# Removes lists-typed items from dict
def strip_lists(info):
    return {val : elem for val, elem in info.items() if type(elem) not in [list]}

# tests values of subset dict {} match values of super-set dict {}
def test_subset(sub, super_set):
    for key, value in sub.items():
        if super_set[key] != value:
            return False
    return True

# ----------------------
# RESTful API Unit Tests
# SXSWE
# ----------------------

class test_API(LiveServerTestCase) :
    fixtures = ['sxsw.json']

    
    # ARTISTS
    def test_post_artists (self):
        values = dumps(CHILDISH_GAMBINO).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/artists/", data=values, headers=headers)
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = RESPONSE_1
        self.assertEqual(response.getcode(), 201)
        self.assertTrue(expected == response_body)

    def test_get_artists (self):
        request = Request(self.live_server_url+"/api/artists")
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = CHILDISH_GAMBINO_SMALL
        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_body)

    def test_get_artist (self):
        request = Request(self.live_server_url+"/api/artists/1/")
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = CHILDISH_GAMBINO
        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_body)

    def test_get_artist_badID (self):
        try:
            request = Request(self.live_server_url+"/api/artists/7/")
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_put_artists (self):
        CHILDISH_GAMBINO['name'] = "NEW NAME"
        values = dumps(CHILDISH_GAMBINO).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/artists/1/", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        response_body = response.read()
        self.assertEqual(response.getcode(), 204)
        CHILDISH_GAMBINO['name'] = "Childish Gambino"

    def test_put_artists_badID (self):
        try:
            values = dumps(CHILDISH_GAMBINO).encode("utf-8")
            headers = {"Content-Type": "application/json"}
            request = Request(self.live_server_url+"/api/artists/3/", data=values, headers=headers)
            request.get_method = lambda: 'PUT'
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_delete_artists (self):
        request = Request(self.live_server_url+"/api/artists/1/")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        response_body = response.read()
        self.assertEqual(response.getcode(), 204)

    def test_get_artist_sponsors (self):
        request = Request(self.live_server_url+"/api/artists/1/sponsors/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)
        expected = DELL_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    def test_get_artist_events (self):
        request = Request(self.live_server_url+"/api/artists/1/events/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)

        expected = WOODIE_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    # SPONSORS
    def test_get_sponsors (self):
        request = Request(self.live_server_url+"/api/sponsors/")
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = DELL_SMALL
        self.assertTrue(expected == response_body)
        self.assertEqual(response.getcode(), 200)

    def test_get_sponsor (self):
        request = Request(self.live_server_url+"/api/sponsors/1/")
        response = urlopen(request)
        self.assertEqual(response.getcode(), 200)
        response_body = loads(response.read().decode("utf-8"))
        expected = DELL
        self.assertTrue(expected == response_body)

    def test_get_sponsor_badID (self):
        try:
            request = Request(self.live_server_url+"/api/sponsor/20/")
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_post_sponsors (self):
        values = dumps(DELL).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/sponsors/", data=values, headers=headers)
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))

        expected = RESPONSE_1
        self.assertEqual(response.getcode(), 201)
        self.assertTrue(expected == response_body)

    def test_put_sponsors (self):
        values = dumps(DELL).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/sponsors/1/", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)

        response_body = response.read()
        self.assertEqual(response.getcode(), 204)

    def test_put_sponsorS_badID (self):
        try:
            values = dumps(DELL).encode("utf-8")
            headers = {"Content-Type": "application/json"}
            request = Request(self.live_server_url+"/api/sponsors/3/", data=values, headers=headers)
            request.get_method = lambda: 'PUT'
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_delete_sponsors (self):
        request = Request(self.live_server_url+"/api/sponsors/1/")
        request.get_method = lambda: 'DELETE'

        response = urlopen(request)
        response_body = response.read()
        
        self.assertEqual(response.getcode(), 204)

    def test_get_sponsor_events (self):
        request = Request(self.live_server_url+"/api/sponsors/1/events/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)

        expected = WOODIE_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    def test_get_sponsor_artists (self):
        request = Request(self.live_server_url+"/api/sponsors/1/artists/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)
        
        expected = CHILDISH_GAMBINO_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    # EVENTS
    def test_get_events (self):
        request = Request(self.live_server_url+"/api/events/")
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = WOODIE_SMALL
        self.assertTrue(expected == response_body)
        self.assertEqual(response.getcode(), 200)

    def test_get_event (self):
        request = Request(self.live_server_url+"/api/events/1/")
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))
        expected = WOODIE
        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_body)
        
    def test_get_event_badID (self):
        try:
            request = Request(self.live_server_url+"/api/event/7/")
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_post_events (self):
        values = dumps(WOODIE).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/events/", data=values, headers=headers)
        response = urlopen(request)
        response_body = loads(response.read().decode("utf-8"))

        expected = RESPONSE_1

        self.assertEqual(response.getcode(), 201)
        self.assertTrue(expected == response_body)

    def test_put_events (self):
        values = dumps(WOODIE).encode("utf-8")
        headers = {"Content-Type": "application/json"}
        request = Request(self.live_server_url+"/api/events/1/", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)

        response_body = response.read()
        self.assertEqual(response.getcode(), 204)

    def test_put_events_badID (self):
        try:
            values = dumps(WOODIE).encode("utf-8")
            headers = {"Content-Type": "application/json"}
            request = Request(self.live_server_url+"/api/events/3/", data=values, headers=headers)
            request.get_method = lambda: 'PUT'
            response = urlopen(request)
        except HTTPError as ex:
            self.assertTrue(ex.getcode() == 404)
        else:
            self.assertTrue(False)

    def test_delete_events (self):
        request = Request(self.live_server_url+"/api/events/1/")
        request.get_method = lambda: 'DELETE'

        response = urlopen(request)
        response_body = response.read()
        
        self.assertEqual(response.getcode(), 204)

    def test_get_events_sponsors (self):
        request = Request(self.live_server_url+"/api/events/1/sponsors/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)

        expected = DELL_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    def test_get_events_artists (self):
        request = Request(self.live_server_url+"/api/events/1/artists/")
        response = urlopen(request)
        response_body = response.read().decode("utf-8")
        response_dict = loads(response_body)
        
        expected = CHILDISH_GAMBINO_SMALL

        self.assertEqual(response.getcode(), 200)
        self.assertTrue(expected == response_dict)

    def test_artist_fromJSON_1_basic(self):
        artist=Artist()
        info=CHILDISH_GAMBINO.copy()
        del info['sponsors']
        del info['events']

        artist.FromJSON(dumps(info))

        self.assertTrue(artist.name ==info['name'])
        self.assertTrue(artist.website ==info['website'])
        self.assertTrue(artist.origin ==info['origin'])
        self.assertTrue(artist.twitter ==info['twitter'])
        self.assertTrue(artist.youtube ==info['youtube'])
        self.assertTrue(artist.year_established ==info['year_established'])
        self.assertTrue(artist.facebook ==info['facebook'])
        self.assertTrue(artist.genre ==info['genre'])
        self.assertTrue(type(artist.id) ==int)
        self.assertTrue(artist.video ==info['video'])
        self.assertTrue(artist.twitter_embed ==info['twitter_embed'])
        self.assertTrue(artist.image ==info['image'])

    def test_artist_fromJSON_100_Sponsors(self):
        # get a valid sponsor ID
        sponsor1_id = Sponsor.objects.all()[0].id

        artist=Artist()
        info=YOUNG_AND_SICK.copy()
        sponsors = [sponsor1_id]*100
        info['sponsors'] = sponsors
        info['events'] = []

        artist.FromJSON(dumps(info))

        self.assertTrue(artist.name ==info['name'])
        self.assertTrue(artist.website ==info['website'])
        self.assertTrue(artist.origin ==info['origin'])
        self.assertTrue(artist.twitter ==info['twitter'])
        self.assertTrue(artist.youtube ==info['youtube'])
        self.assertTrue(artist.year_established ==info['year_established'])
        self.assertTrue(artist.facebook ==info['facebook'])
        self.assertTrue(artist.genre ==info['genre'])
        self.assertTrue(type(artist.id) == int)
        self.assertTrue(artist.video ==info['video'])
        self.assertTrue(artist.twitter_embed ==info['twitter_embed'])
        self.assertTrue(artist.image ==info['image'])
        self.assertTrue({sponsor.id for sponsor in artist.sponsors.all()} == set(sponsors))
        self.assertTrue(len(artist.events.all()) == 0)

    def test_artist_fromJSON_3_Invalid(self):
        artist=Artist()
        info=CASHMERE_CAT.copy()
        del info['sponsors']
        info['events'] = ["BLAH!"]

        errored = False
        try:
            artist.FromJSON(dumps(info))
        except ValueError as e:
            errored = True
    
        self.assertTrue(errored)


    def test_event_fromJSON_1_basic(self):

        event=Event()
        info=WOODIE.copy()
        del info['sponsors']
        del info['artists']
        event.FromJSON(dumps(info))

        self.assertTrue(event.name ==info['name'])
        self.assertTrue(event.location_name ==info['location_name'])
        self.assertTrue(event.image ==info['image'])
        self.assertTrue(event.location_address ==info['location_address'])
        self.assertTrue(event.map_url ==info['map_url'])
        self.assertTrue(event.date ==info['date'])
        self.assertTrue(event.twitter_embed ==info['twitter_embed'])
        self.assertTrue(event.type ==info['type'])
        self.assertTrue(event.description ==info['description'])

    def test_event_fromJSON_2_Long_Description(self):

        event=Event()
        info=HYPE_HOTEL.copy()
        del info['sponsors']
        info['artists'] = [100000000, 8]
        
        info['description'] = LONG_TEXT

        errored = False
        try:
            event.FromJSON(dumps(info))
        except:
            errored = True
    
        self.assertTrue(errored)

    def test_event_fromJSON_3_Invalid_PKey(self):

        event=Event()
        info=FADER_FORT.copy()
        del info['sponsors']
        del info['artists']
        event.FromJSON(dumps(info))

        self.assertTrue(event.name ==info['name'])
        self.assertTrue(event.location_name ==info['location_name'])
        self.assertTrue(event.image ==info['image'])
        self.assertTrue(event.location_address ==info['location_address'])
        self.assertTrue(event.map_url ==info['map_url'])
        self.assertTrue(event.date ==info['date'])
        self.assertTrue(event.twitter_embed ==info['twitter_embed'])
        self.assertTrue(event.type ==info['type'])
        self.assertTrue(event.description ==info['description'])

    def test_sponsor_fromJSON_1_basic(self):

        sponsor=Sponsor()
        info=DELL.copy()
        del info['events']
        del info['artists']
        sponsor.FromJSON(dumps(info))

        self.assertTrue(sponsor.name ==info['name'])
        self.assertTrue(sponsor.website ==info['website'])
        self.assertTrue(sponsor.established ==info['established'])
        self.assertTrue(sponsor.twitter ==info['twitter'])
        self.assertTrue(sponsor.CEO ==info['CEO'])
        self.assertTrue(sponsor.industry ==info['industry'])
        self.assertTrue(sponsor.map_url ==info['map_url'])
        self.assertTrue(sponsor.facebook ==info['facebook'])
        self.assertTrue(sponsor.location ==info['location'])
        self.assertTrue(sponsor.twitter_embed ==info['twitter_embed'])
        self.assertTrue(sponsor.image ==info['image'])

    def test_sponsor_fromJSON_2_null_twitter(self):
        sponsor=Sponsor()
        info=TACO_BELL.copy()
        del info['events']
        del info['artists']
        info['twitter_embed'] = None
        
        errored = False
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        try:
            sponsor.FromJSON(dumps(info))
        except IntegrityError: # doesn't catch because dict is stringified
            errored = True
        self.assertFalse(errored)


    def test_sponsor_fromJSON_3(self):

        sponsor=Sponsor()
        info=CONVERSE.copy()
        del info['events']
        del info['artists']
        sponsor.FromJSON(dumps(info))

        self.assertTrue(sponsor.name ==info['name'])
        self.assertTrue(sponsor.website ==info['website'])
        self.assertTrue(sponsor.established ==info['established'])
        self.assertTrue(sponsor.twitter ==info['twitter'])
        self.assertTrue(sponsor.CEO ==info['CEO'])
        self.assertTrue(sponsor.industry ==info['industry'])
        self.assertTrue(sponsor.map_url ==info['map_url'])
        self.assertTrue(sponsor.facebook ==info['facebook'])
        self.assertTrue(sponsor.location ==info['location'])
        self.assertTrue(sponsor.twitter_embed ==info['twitter_embed'])
        self.assertTrue(sponsor.image ==info['image'])

    def test_artist_toJSON_1_basic(self):
        info = CHILDISH_GAMBINO.copy()
        del info['id']
        
        stripped = strip_lists(info)
        artist = Artist(**stripped)
        artist.save()

        comp = loads(artist.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_artist_toJSON_2_Long_Name(self):
        info = YOUNG_AND_SICK.copy()
        info['name'] = LONGEST_NAME_IN_THE_WORLD[100]
        del info['id']
        
        stripped = strip_lists(info)
        artist = Artist(**stripped)
        artist.save()

        comp = loads(artist.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_artist_toJSON_100_Sponsors(self):
        info = YOUNG_AND_SICK.copy()
        del info['id']
        
        sponsors = [1]*100

        # ignores lists
        stripped = strip_lists(info)
        artist = Artist(**stripped)
        artist.save()
        artist.sponsors = sponsors

        comp = loads(artist.ToJSON())

        self.assertTrue(set(comp['sponsors']) == set(sponsors))

    def test_event_toJSON_1_basic(self):
        info = HYPE_HOTEL.copy()
        del info['id']
        
        stripped = strip_lists(info)
        event = Event(**stripped)
        event.save()

        comp = loads(event.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_event_toJSON_2_Long_Map(self):
        info = HYPE_HOTEL.copy()
        info['description'] = LONG_TEXT
        del info['id']
        
        stripped = strip_lists(info)
        model = Event(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_event_toJSON_3_Id_Not_Unique(self):
        info = WOODIE.copy()
        del info['id']
        keys = [m.id for m in Event.objects.all()]

        stripped = strip_lists(info)
        event = Event(**stripped)
        event.save()

        comp = loads(event.ToJSON())

        self.assertTrue(not comp['id'] in keys)

    def test_sponsor_toJSON_1_basic(self):
        info = TACO_BELL.copy()
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_sponsor_toJSON_2_dataset_2(self):
        info = DELL.copy()
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_sponsor_toJSON_3_Quote_In_Name(self):
        info = DELL.copy()
        info['name'] = "\""
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_artist_toSmallJSON_1_basic(self):
        info = CHILDISH_GAMBINO.copy()
        del info['id']
        
        stripped = strip_lists(info)
        artist = Artist(**stripped)
        artist.save()

        comp = loads(artist.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_artist_toSmallJSON_2_Id_Not_Unique(self):
        info = YOUNG_AND_SICK.copy()
        del info['id']
        keys = [m.id for m in Artist.objects.all()]

        stripped = strip_lists(info)
        artist = Artist(**stripped)
        artist.save()

        comp = loads(artist.ToSmallJSON())

        self.assertTrue(not comp['id'] in keys)

    def test_artist_toSmallJSON_3_Name_Newline(self):
        info = CHILDISH_GAMBINO.copy()
        info['name'] = "\n\n\n\n\n\nNANANANANA... BATMAN!"
        del info['id']
        
        stripped = strip_lists(info)
        model = Artist(**stripped)
        model.save()

        comp = loads(model.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))
    
    def test_event_toSmallJSON_1_basic(self):
        info = HYPE_HOTEL.copy()
        del info['id']
        
        stripped = strip_lists(info)
        event = Event(**stripped)
        event.save()

        comp = loads(event.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_event_toSmallJSON_2_Empty_Name(self):
        info = FADER_FORT.copy()
        info['name'] = ''
        del info['id']
        
        stripped = strip_lists(info)
        event = Event(**stripped)
        event.save()

        comp = loads(event.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_event_toSmallJSON_3_Long_Name(self):
        info = FADER_FORT.copy()
        info['name'] = LONGEST_NAME_IN_THE_WORLD[100]
        del info['id']
        
        stripped = strip_lists(info)
        event = Event(**stripped)
        event.save()

        comp = loads(event.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_sponsor_toSmallJSON_1_basic(self):
        info = TACO_BELL.copy()
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToSmallJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_sponsor_toSmallJSON_2_db2(self):
        info = CONVERSE.copy()
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(strip_lists(comp) == stripped)

    def test_sponsor_toSmallJSON_3_Quote_In_Name(self):
        info = DELL.copy()
        info['name'] = "\""
        del info['id']
        
        stripped = strip_lists(info)
        model = Sponsor(**stripped)
        model.save()

        comp = loads(model.ToJSON())
        del comp['id']

        self.assertTrue(test_subset(strip_lists(comp), stripped))

    def test_search_no_results(self):
        search_term = "asdfdsfa"
        search_dict = build_search_dict(search_term)
        self.assertTrue(search_dict["num_results"] == 0)

    def test_search_and(self):
        search_term = "Childish"
        search_dict = build_search_dict(search_term)
        self.assertTrue(search_dict["num_results"] == 3)
        self.assertTrue(search_dict["artists_and"][0].name == "Childish Gambino")
        self.assertTrue(search_dict["sponsors_and"][0].name == "Dell")
        self.assertTrue("childish" in search_dict["sponsors_and"][0].searchResult)
        self.assertTrue(search_dict["events_and"][0].name == "Woodie Awards")
        self.assertTrue("childish" in search_dict["events_and"][0].searchResult)

    def test_search_or(self):
        search_term = "Childish Fader"
        search_dict = build_search_dict(search_term)
        self.assertTrue(search_dict["num_results"] == 3)
        self.assertTrue(search_dict["artists_or"][0].name == "Childish Gambino")
        self.assertTrue(search_dict["sponsors_or"][0].name == "Dell")
        self.assertTrue("childish" in search_dict["sponsors_or"][0].searchResult)
        self.assertTrue(search_dict["events_or"][0].name == "Woodie Awards")
        self.assertTrue("childish" in search_dict["events_or"][0].searchResult)

print ("SXSWE-tests.py")

print("Done.")