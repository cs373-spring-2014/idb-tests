#!/usr/bin/env python3

from django.db import models
from django.utils.safestring import mark_safe
import json
import re
import string

# ------------
# serialize_model_strings
# Collect all data from models related to model m as string data
# Return a JSON dictionary of the serialized data
# ------------

def serialize_model_strings(m):
    d = {}
    d['images'] = [image.url for image in m.images.all()]
    d['videos'] = [video.url for video in m.videos.all()]
    d['external_links'] = [el.url for el in m.external_links.all()]
    d['citations'] = [cit.url for cit in m.citations.all()]

    if isinstance(m, Game):
        d['studios'] = m.studios.all().values('id', 'name')
        d['consoles'] = m.consoles.all().values('id', 'name')
    elif isinstance(m, Console):
        games = m.game_set.all()
        d['games'] = games.values('id', 'name')
        d['studios'] = []
        for g in games:            
            for s in g.studios.values('id', 'name'):
                if s not in d['studios']:
                    d['studios'].append(s)
    elif isinstance(m, Studio):
        games = m.game_set.all()
        d['games'] = games.values('id', 'name')
        d['consoles'] = []
        for g in games:
            for c in g.consoles.values('id', 'name'):
                if c not in d['consoles']:
                    d['consoles'].append(c)
    return d

# ------------
# serialize_model_ids
# Collect all data from models related to model m as id data
# Return a JSON dictionary of the serialized data
# ------------

def serialize_model_ids(m):
    d = {}
    d['images'] = [image.id for image in m.images.all()]
    d['videos'] = [video.id for video in m.videos.all()]
    d['external_links'] = [el.id for el in m.external_links.all()]
    d['citations'] = [cit.id for cit in m.citations.all()]

    if isinstance(m, Game):
        d['studios'] = [s.id for s in m.studios.all()]
        d['consoles'] = [c.id for c in m.consoles.all()]
    elif isinstance(m, Console):
        games = m.game_set.all()
        d['games'] = [g.id for g in games.all()]
        d['studios'] = []
        for g in games:
            d['studios'].extend([s.id for s in g.studios.all() if s.id not in d['studios']])
    elif isinstance(m, Studio):
        games = m.game_set.all()
        d['games'] = [g.id for g in games.all()]
        d['consoles'] = []
        for g in games:
            d['consoles'].extend([c.id for c in g.consoles.all() if c.id not in d['consoles']])
    return d

# ------------
# Genre  
# Represents a video game genre
# ------------

class Genre(models.Model): 
    name = models.CharField(max_length=50)

# ------------
# URL 
# Represents a URL for URL-based content
# ------------

class URL(models.Model): 
    name = models.CharField(max_length=50)
    url = models.URLField(max_length=500)

# ------------
# Developer 
# Represents a Developer
# ------------

class Developer(models.Model):
    name = models.CharField(max_length=50)

# ------------
# Game 
# Represents a video game 
# ------------

class Game(models.Model):
    name = models.CharField(max_length=100)
    publish_date = models.DateField()
    genre = models.ForeignKey(Genre)
    description = models.TextField()
    handle_id = models.CharField(max_length=50)
    widget_id = models.CharField(max_length=50)
    consoles = models.ManyToManyField('Console')
    studios = models.ManyToManyField('Studio')
    images = models.ManyToManyField(URL, related_name='g_images')
    videos = models.ManyToManyField(URL, related_name='g_videos')
    external_links = models.ManyToManyField(URL, related_name='g_external_links')
    citations = models.ManyToManyField(URL, related_name='g_citations')

    def to_json(self, using_strings):
        if using_strings:
            d = serialize_model_strings(self)
            d['genre'] = self.genre.name
        else:
            d = serialize_model_ids(self)
            d['genre'] = self.genre.id
        return {"id": self.id,
                "name": self.name,
                "publish_date": str(self.publish_date),
                "genre": d['genre'],
                "description": self.description,
                "handle_id": self.handle_id,
                "widget_id": self.widget_id,
                "consoles": d['consoles'],
                "studios": d['studios'],
                "images": d['images'],
                "videos": d['videos'],
                "external_links": d['external_links'],
                "citations": d['citations']}


# ------------
# Console 
# Represents a video game console 
# ------------

class Console(models.Model):
    name = models.CharField(max_length=100)
    release_date = models.DateField()
    developers = models.ManyToManyField('Developer') # NOT A STUDIO!
    description = models.TextField()
    cpu = models.CharField(max_length=100)
    memory = models.CharField(max_length=100)
    storage = models.CharField(max_length=100)
    graphics = models.CharField(max_length=100)
    images = models.ManyToManyField(URL, related_name='c_images')
    videos = models.ManyToManyField(URL, related_name='c_videos')
    external_links = models.ManyToManyField(URL, related_name='c_external_links')
    citations = models.ManyToManyField(URL, related_name='c_citations')

    def to_json(self, using_strings):
        if using_strings:
            d = serialize_model_strings(self)
            d['developers'] = [dev.name for dev in self.developers.all()]
        else:
            d = serialize_model_ids(self)
            d['developers'] = [dev.id for dev in self.developers.all()]
        return {"id": self.id,
                "name": self.name,
                "developers": d['developers'],
                "release_date": str(self.release_date),
                "cpu": self.cpu,
                "description": self.description,
                "memory": self.memory,
                "storage": self.storage,
                "graphics": self.graphics,
                "games": d['games'],
                "studios": d['studios'],
                "images": d['images'],
                "videos": d['videos'],
                "external_links": d['external_links'],
                "citations": d['citations']}


# ------------
# Studio 
# Represents a video game studio 
# ------------

class Studio(models.Model):
    name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    location = models.CharField(max_length=50)
    description = models.TextField()
    map_embed = models.ForeignKey(URL)
    handle_id = models.CharField(max_length=50)
    widget_id = models.CharField(max_length=50)
    images = models.ManyToManyField(URL, related_name='s_images')
    videos = models.ManyToManyField(URL, related_name='s_videos')
    external_links = models.ManyToManyField(URL, related_name='s_external_links')
    citations = models.ManyToManyField(URL, related_name='s_citations')
    
    def to_json(self, using_strings):
        if using_strings:
            d = serialize_model_strings(self)
            d['map_embed'] = self.map_embed.url
        else:
            d = serialize_model_ids(self)
            d['map_embed'] = self.map_embed.id
        return {"id": self.id,
                "name": self.name,
                "date_of_birth": str(self.date_of_birth),
                "location": self.location,
                "map_embed": d['map_embed'],
                "description": self.description,
                "handle_id": self.handle_id,
                "widget_id": self.widget_id,
                "consoles": d['consoles'],
                "games": d['games'],
                "images": d['images'],
                "videos": d['videos'],
                "external_links": d['external_links'],
                "citations": d['citations']}


def search_objects(query_string, model_type):
    terms = normalize_query(query_string)
    found_entries_AND = []
    found_entries_OR = []
    for object in model_type.objects.all():
        model_string = model_to_string(object, model_type)
        found_occurrences, found_all_terms, found_any_terms = find_all_occurrences(model_string, terms)
        if found_all_terms:
            result_string = mark_safe(generate_result_string(model_string, found_occurrences, object, model_type))
            found_entries_AND.append((object, result_string))
        if found_any_terms > 0:
            result_string = mark_safe(generate_result_string(model_string, found_occurrences, object, model_type))
            found_entries_OR.append((object, result_string))
    return found_entries_AND, found_entries_OR

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    for p in string.punctuation:
        query_string = query_string.replace(p, ' ')
        
    return [normspace(' ', str((t[0] or t[1]).strip().lower())) for t in findterms(query_string)] 

def generate_result_string(model_string, found_occurrences, object, model_type):
    result_string = ''
    object_fields = ['id', 'name']

    if isinstance(object, Game):
        object_fields += ['publish_date', 'genre', 'consoles', 'studios', 'handle_id', 'description']
    elif isinstance(object, Console):
        object_fields += ['release_date', 'developers', 'cpu', 'memory', 'storage', 'graphics', 'games', 'studios', 'description']
    elif isinstance(object, Studio):
        object_fields += ['date_of_birth', 'location', 'games', 'consoles', 'description']
    else:
        assert(False)
        return None

    for key in object_fields:
        if len(found_occurrences[key]) > 0:
            result_string = result_string + '  ' + bold_found_words(model_string[key], found_occurrences[key])
  
    return result_string


def model_to_string(object, model_type):
    fields = {}

    # Fields common to all model_types
    fields['id'] = "ID: " + str(object.id)
    fields['name'] = "Name: " + str(object.name)
    fields['description'] = "Description: " + str(object.description)

    all_games = Game.objects.all()
    all_consoles = Console.objects.all()
    all_studios = Studio.objects.all()

    if isinstance(object, Game):
        # fields['publish_date'] = "Publish date: " + str(object.publish_date.strftime("%B %d, %Y"))
        fields['publish_date'] = "Publish date: " + str(object.publish_date)
        fields['genre'] = "Genre: " + str(object.genre.name)
        fields['handle_id'] = "Twitter Handle: " + str(object.handle_id)
        console_names = [str(console.name) for console in object.consoles.all()]
        fields['consoles'] = "Consoles: " + ", ".join(console_names)
        studio_names = [str(studio.name) for studio in object.studios.all()]
        fields['studios'] = "Studios: " + ", ".join(studio_names)

    elif isinstance(object, Console):
        # fields['release_date'] = "Release date: " + str(object.release_date.strftime("%B %d, %Y"))
        fields['release_date'] = "Release date: " + str(object.release_date)
        developers = [str(developer.name) for developer in object.developers.all()]
        fields['developers'] = "Developers: " + ", ".join(developers)
        fields['cpu'] = "CPU: " + str(object.cpu)
        fields['memory'] = "Memory: " + str(object.memory)
        fields['storage'] = "Storage: " + str(object.storage)
        fields['graphics'] = "Graphics: " + str(object.graphics)

        games = object.game_set.all()
        studios = []

        for s in all_studios:
            for g in games:
                if (g in s.game_set.all()) and (s not in studios):
                    studios.append(s)

        game_names = [str(game.name) for game in games]
        fields['games'] = "Games: " + ", ".join(game_names)
        studio_names = [str(studio.name) for studio in studios]
        fields['studios'] = "Studios: " + ", ".join(studio_names)


    elif isinstance(object, Studio):
        # fields['date_of_birth'] = "Founded: " + str(object.date_of_birth.strftime("%B %d, %Y"))
        fields['date_of_birth'] = "Founded: " + str(object.date_of_birth)
        fields['location'] = "Location: " + str(object.location)

        games = object.game_set.all()
        consoles = []

        for c in all_consoles:
            for g in games:
                if (g in c.game_set.all()) and (c not in consoles):
                    consoles.append(c)

        game_names = [str(game.name) for game in games]
        fields['games'] = "Games: " + ", ".join(game_names)
        console_names = [str(console.name) for console in consoles]
        fields['consoles'] = "Consoles: " + ", ".join(console_names)


    else:
        assert(False)
        fields = None


    return fields


# ------------
# find_all_occurrences
# Returns a list representing the starting index of all occurrences
# of search terms
# ------------
def find_all_occurrences(query_strings, terms):
    found_terms = [False] * len(terms)
    found_instances = {}
    found_all_terms = True
    found_any_terms = False
    for key in query_strings:
        found_string_instances = []
        for i in range(0, len(terms)):
            indexes = [m.start() for m in re.finditer(terms[i], query_strings[key].lower())]
            if len(indexes) > 0:
                found_string_instances += [(terms[i], index) for index in indexes]
                found_terms[i] = True
            found_instances[key] = found_string_instances
    for found_this_term in found_terms:
        if found_this_term == False:
            found_all_terms = False
        else:
            found_any_terms = True
    return (found_instances, found_all_terms, found_any_terms)


# ------------
# bold_found_words
# Returns an HTML string that represents a string, bolded with instances
# of search terms in occurrences
# ------------
def bold_found_words(found_string, occurrences):
    begin_bold_str = '<b>'
    end_bold_str = '</b>'
    bolded_string = ''
    ptr = 0

    occurrences.sort(key=lambda tup: tup[1])

    for word, index in occurrences:
        bolded_string = bolded_string + found_string[ptr:index] + begin_bold_str + found_string[index:index + len(word)] + end_bold_str
        ptr = index + len(word)

    bolded_string += found_string[ptr:]

    return bolded_string
