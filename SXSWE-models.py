from django.db import models
import json

# to arbitarily execute sql code:
#from django.db import connection
#cursor = connection.cursor()
#cursor.execute("")

"""
returns the value of an attribute if it exists
otherwise it returns the default value
"""
def get_val(value, default):
    if value:
        return value
    else:
        return default

"""
Creates the references for the ManyToManyFields.
Takes in a list of primary keys, the model class
The value of create determines if a new object must be constructed.
"""
def linkify_list(supplied_list, modelclass, create=False):
    new_list = []
    for key in supplied_list:
        try:
            obj = modelclass.objects.get(pk=key)
        except modelclass.DoesNotExist:
            if create:
                obj = modelclass(name=key)
                obj.save()
            else:
                raise modelclass.DoesNotExist()
        new_list.append(obj)
    return new_list

'''
Base class which stubs out serialization methods called
by restful views.
'''
class SerializableModel(models.Model): 
    def ToJSON(self):
        raise NotImplementedError()
    
    def FromJSON(self, json):
        raise NotImplementedError()

'''
This class describes an instrument used by
a band.
'''
class Instrument(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    
    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

'''
This class describes a person in a band.
'''
class Person(models.Model):
    name = models.CharField(primary_key=True, max_length=100)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name
'''
This class describes a label used by
a band.
'''
class Label(models.Model):
    name = models.CharField(primary_key=True, max_length=100)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


'''
This is the class which describes the model for an artist
performing at at SXSW. All of the information needed when
displaying an artist on our webpage will be contained here.
'''
class Artist(models.Model):
    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    year_established = models.IntegerField()
    origin = models.CharField(max_length=100)
    website = models.URLField()

    # e.g. ["bass", "drums", "guitar", "synthesizer", "sequencer", "sampler", "vocals"],
    instruments = models.ManyToManyField('Instrument', related_name='artists')

    facebook = models.CharField(max_length=100)
    twitter = models.CharField(max_length=100)
    twitter_embed = models.TextField()
    youtube = models.CharField(max_length=100)
    
    #e.g. ["Donald Glover"],
    members = models.ManyToManyField('Person', related_name='artists')
    
    labels = models.ManyToManyField('Label', related_name='artists')

    # e.g. "http://upload.wikimedia.org/wikipedia/commons/3/3a/CG_Coachella.jpg"
    image = models.URLField()

    video = models.URLField()
    
    # our many to many fields
    sponsors = models.ManyToManyField('Sponsor', related_name='artists')
    events = models.ManyToManyField('Event', related_name='artists')
    

    def __str__(self):
        return self.name


    """
    Given an Artist object, returns the abbreviated JSON representation of its fields
    """
    def ToSmallJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['genre'] = self.genre
        response['members'] = [members.name for members in self.members.all()]
        response['sponsors'] = [sponsor.id for sponsor in self.sponsors.all()]
        response['events'] = [event.id for event in self.events.all()]
        return json.dumps(response)


    """
    Given an Artist object, returns the full JSON representation of its fields
    """
    def ToJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['genre'] = self.genre
        response['year_established'] = self.year_established
        response['origin'] = self.origin
        response['website'] = self.website
        response['instruments'] = [instrument.name for instrument in self.instruments.all()]
        response['facebook'] = self.facebook
        response['twitter'] = self.twitter
        response['twitter_embed'] = self.twitter_embed
        response['youtube'] = self.youtube
        response['members'] = [members.name for members in self.members.all()]
        response['labels'] = [labels.name for labels in self.labels.all()]
        response['image'] = self.image
        response['video'] = self.video
        response['sponsors'] = [sponsor.id for sponsor in self.sponsors.all()]
        response['events'] = [event.id for event in self.events.all()]
        return json.dumps(response)
    
    """
    Populates the fields of an Artist object with the values in a given JSON
    """
    def FromJSON(self, body):
        values = json.loads(body)
        self.name = get_val(values.get('name'), self.name)
        self.genre = get_val(values.get('genre'), self.genre)
        self.year_established = get_val(values.get('year_established'), self.year_established)
        self.origin = get_val(values.get('origin'), self.origin)
        self.website = get_val(values.get('website'), self.website)
        self.save()
        
        instruments_list = values.get('instruments')

        if instruments_list:
            self.instruments = linkify_list(instruments_list, Instrument, create=True)

        self.facebook = get_val(values.get('facebook'), self.facebook)
        self.twitter = get_val(values.get('twitter'), self.twitter)
        self.twitter_embed = get_val(values.get('twitter_embed'), self.twitter_embed)
        self.youtube = get_val(values.get('youtube'), self.youtube)
        self.save()
        
        members_list = values.get('members')
        if members_list:
            self.members = linkify_list(members_list, Person, create=True)            
        self.save()

        labels_list = values.get('labels')
        if labels_list:
            self.labels = linkify_list(labels_list, Label, create=True)
        
        sponsors_list = values.get('sponsors')
        if sponsors_list:
            self.sponsors = linkify_list(sponsors_list, Sponsor)

        events_list = values.get('events')
        if events_list:
            self.events = linkify_list(events_list, Event)

        self.image = get_val(values.get('image'), self.image)
        self.video = get_val(values.get('video'), self.video)
        self.save()
        return self


'''
This class represents the sponsor data. Sponsors
are connected to artists and events. They can be found
through either, or by themselves on the website.
'''
class Sponsor(models.Model):
    name = models.CharField(max_length=100)
    established = models.DateField()
    location = models.TextField()
    industry = models.CharField(max_length=100)

    CEO = models.CharField(max_length=100)

    # e.g. "http://upload.wikimedia.org/wikipedia/commons/3/3a/CG_Coachella.jpg"
    image = models.URLField()

    facebook = models.CharField(max_length=100)
    twitter = models.CharField(max_length=100)
    twitter_embed = models.TextField()

    website = models.URLField()
    map_url = models.TextField()

    # our many to many fields
    events = models.ManyToManyField('Event', related_name='sponsors')
    

    def __str__(self):
        return self.name

    """
    Given an Sponsor object, returns the abbreviated JSON representation of its fields
    """
    def ToSmallJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['industry'] = self.industry
        response['artists'] = [artist.id for artist in self.artists.all()]
        response['events'] = [event.id for event in self.events.all()]
        return json.dumps(response)


    """
    Given an Sponsor object, returns the full JSON representation of its fields
    """
    def ToJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['industry'] = self.industry
        response['established'] = str(self.established)
        response['location']=self.location
        response['CEO']=self.CEO
        response['image']=self.image
        response['facebook']=self.facebook
        response['twitter']=self.twitter
        response['twitter_embed']=self.twitter_embed
        response['website']=self.website
        response['map_url']=self.map_url

        response['artists'] = [artist.id for artist in self.artists.all()]
        response['events'] = [event.id for event in self.events.all()]

        return json.dumps(response)

    """
    Populates the fields of an Sponsor object with the values in a given JSON
    """
    def FromJSON(self, body):
        values = json.loads(body)
        self.name = get_val(values.get('name'), self.name)
        self.industry = get_val(values.get('industry'), self.industry)
        self.established = get_val(values.get('established'), self.established)
        self.location = get_val(values.get('location'), self.location)
        self.CEO = get_val(values.get('CEO'), self.CEO)
        self.image = get_val(values.get('image'), self.image)
        self.facebook = get_val(values.get('facebook'), self.facebook)
        self.twitter = get_val(values.get('twitter'), self.twitter)
        self.twitter_embed = get_val(values.get('twitter_embed'), self.twitter_embed)
        self.website = get_val(values.get('website'), self.website)
        self.map_url = get_val(values.get('map_url'), self.map_url)

        self.save()

        events_list = values.get('events')
        if events_list:
            self.events = linkify_list(events_list, Event)


        artists_list = values.get('artists')
        if artists_list:
            self.artists = linkify_list(artists_list, Artist)

        self.save()
        return self

'''
An event describes a single event (e.g. concert, competition, etc.) at SXSW.
It is connected to both artists and sponsors and can be accessed from either.
'''
class Event(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateField()
    description = models.TextField()
    type = models.CharField(max_length=100)
    image = models.URLField()
    location_name = models.CharField(max_length=100)
    location_address = models.TextField()
    map_url = models.TextField()
    twitter_embed = models.TextField()

    def __str__(self):
        return self.name


    """
    Given an Event object, returns the abbreviated JSON representation of its fields
    """
    def ToSmallJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['date'] = str(self.date)
        response['artists'] = [artist.id for artist in self.artists.all()]
        response['sponsors'] = [sponsor.id for sponsor in self.sponsors.all()]
        return json.dumps(response)

    """
    Given an Event object, returns the full JSON representation of its fields
    """
    def ToJSON(self):
        response = {}
        response['id'] = self.id
        response['name'] = self.name
        response['date'] = str(self.date)
        response['description']=self.description
        response['type']=self.type
        response['image']=self.image
        response['location_name']=self.location_name
        response['location_address']=self.location_address
        response['map_url']=self.map_url
        response['twitter_embed']=self.twitter_embed

        response['artists'] = [artist.id for artist in self.artists.all()]
        response['sponsors'] = [sponsor.id for sponsor in self.sponsors.all()]



        return json.dumps(response)

    """
    Populates the fields of an Event object with the values in a given JSON
    """
    def FromJSON(self, body):
        values = json.loads(body)
        self.name = get_val(values.get('name'), self.name)
        self.date = get_val(values.get('date'), self.date)
        self.description = get_val(values.get('description'), self.description)
        self.type = get_val(values.get('type'), self.type)
        self.image = get_val(values.get('image'), self.image)
        self.location_name = get_val(values.get('location_name'), self.location_name)
        self.location_address = get_val(values.get('location_address'), self.location_address)
        self.map_url = get_val(values.get('map_url'), self.map_url)
        self.twitter_embed = get_val(values.get('twitter_embed'), self.twitter_embed)
        self.save()

        sponsors_list = values.get('sponsors')
        if sponsors_list:
            self.sponsors = linkify_list(sponsors_list, Sponsor)

        artists_list = values.get('artists')
        if artists_list:
            self.artists = linkify_list(artists_list, Artist)

        self.save()

        return self
