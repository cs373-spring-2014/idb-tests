from django.db import models
from django.core.exceptions import ObjectDoesNotExist
    
class Division(models.Model):
    """
    Class model to represent a League of Legends division.
    Contains a division's name, tier, and subtier.
    """
    divisionName = models.CharField(max_length=30)
    tier = models.CharField(max_length=30)
    subtier = models.CharField(max_length=30)
    region = models.CharField(max_length=5)
    video = models.TextField()
    picture = models.TextField()

    # Reads all division objects
    @staticmethod
    def api_get_all():
        divisions = Division.objects.all()
        json = []
        for div in divisions:
            one = {
                "id":           div.id,
                "divisionName": str(div.divisionName),
            }
            json.append(one)
        return json

    # Reads a division object
    @staticmethod
    def api_get_one(id):
        try:
            div = Division.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        player = models.get_model('players', 'Player')
        team = models.get_model('teams', 'Team')
        entities = player.objects.filter(soloQueueDivision=div.id)
        if len(entities) == 0:
            print("There were no PLAYERS")
            entities = team.objects.filter(division3v3ID=div.id)
        if len(entities) == 0:
            print("There were no 3v3 TEAMS")
            entities = team.objects.filter(division5v5ID=div.id)
        if len(entities) == 0:
            print("There were no 5v5 TEAMS")
            entities = []
        try:
            entities = [[p.playerName, p.id] for p in entities]
        except AttributeError:
            entities = [[t.teamName, t.id] for t in entities]
        json = {
            "id":           div.id,
            "divisionName": str(div.divisionName),
            "tier":         str(div.tier),
            "subtier":      str(div.subtier),
            "region":       str(div.region),
            "video":        str(div.video),
            "picture":      str(div.picture),
            "players":      entities
        }
        return json

    # Creates a division object
    @staticmethod
    def api_post(request, test=False):
        body = request.body
        body = loads(str(body))
        d = Division(**body)
        """
        d = Division(
            divisionName=body["divisionName"],
            tier=body["tier"],
            subtier=body["subtier"],
            region=body["region"],
            video=body["video"],
            picture=body["picture"])
        """
        json = {"divisionName": d.divisionName, "picture": d.picture}
        if not test:
            d.save()
            return json
        else:
            return (json, d)
    
    # Updates a division object
    @staticmethod
    def api_put(id, request, test=False):
        body = request.body
        body = loads(str(body))
        try:
            d = Division.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        for key in body:
            d.__dict__[key] = body[key]
        """
        d.divisionName = body["divisionName"]
        d.tier = body["tier"]
        d.subtier = body["subtier"]
        d.region = body["region"]
        d.video = body["video"]
        d.picture = body["picture"]
        """
        if not test:
            d.save()
        else:
            return d

    # Deletes a division object
    @staticmethod
    def api_delete(id, test=False):
        try:
            division = Division.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        if not test:
            division.delete()
        else:
            return division

    @staticmethod
    def getPlayersFromSoloQueueDivisionID(id):
        page = "players"
        player = models.get_model('players', 'Player')
        entities = player.objects.filter(soloQueueDivision=id)
        if(len(entities) == 0):
            team = models.get_model('teams', 'Team')
            entities = team.objects.filter(division3v3ID=id)
            page = "teams"
            if(len(entities) == 0):
                entities = team.objects.filter(division5v5ID=id)

        # At this point, entities should be non-empty
        it = iter(entities)
        listofplayers = []
        if page == "players":
            for e in it:
                listofplayers.append([e.playerName, e.id, page])
        else:
            for e in it:
                listofplayers.append([e.teamName, e.id, page])

        return listofplayers

    # method that returns a JSON object of the stuff we have to plug-in
    @staticmethod
    def get_by_id(id):
        try:
            d = Division.objects.get(pk=id)
        except ObjectDoesNotExist:
            return {}
        entities = Division.getPlayersFromSoloQueueDivisionID(id)
        entitytype = entities[0][2].capitalize()
        division =   {"id"           : id,
                      "divisionName" : d.divisionName,
                      "tier"         : d.tier,
                      "subtier"      : d.subtier,
                      "region"       : d.region,
                      "video"        : d.video,
                      "picture"      : d.picture,
                      "players"      : entities,
                      "entitytype"   : entitytype
                    }
        return division

    def __str__(self):
        return self.divisionName
        
class Player(models.Model):
    """
    Class model to represent a League of Legends player.
    Contains a player's name, id, division, level, kills, 
    wins, region, and summoner icon information.
    """
    playerName = models.CharField(max_length=30)
    soloQueueDivision = models.ForeignKey('divisions.Division') # one-to-many: multiple players in one division
    summonerLevel = models.IntegerField()
    champKills = models.IntegerField()
    minionKills = models.IntegerField()
    wins = models.IntegerField()
    summonerIconID = models.IntegerField()
    region = models.CharField(max_length=5)
    twitter = models.TextField()
    widgetID = models.TextField()
    facebook = models.TextField()
    twitch = models.TextField()
    video = models.TextField()
    picture = models.TextField()

    # Reads all player objects
    @staticmethod
    def api_get_all():
        players = Player.objects.all()
        json = []
        for player in players:
            one = {
                "playerName":       str(player.playerName),
                "id":               player.id,
                "summonerIconID":   player.summonerIconID,
            }
            json.append(one)
        return json

    # Reads a player object
    @staticmethod
    def api_get_one(id):
        try:
            player = Player.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        team = models.get_model('teams', 'Team')
        teams = team.objects.filter(members__id=id)
        teams = [
                    {
                        "id":       team.id,
                        "teamName": team.teamName
                    }
                for team in teams]
        json = {
            "id": player.id,
            "playerName": str(player.playerName),
            "soloQueueDivision": player.soloQueueDivision.id,
            "summonerLevel": player.summonerLevel,
            "champKills": player.champKills,
            "minionKills": player.minionKills,
            "wins": player.wins,
            "summonerIconID": str(player.summonerIconID),
            "region": str(player.region),
            "twitter": str(player.twitter),
            "widgetID": str(player.widgetID),
            "facebook": str(player.facebook),
            "twitch": str(player.twitch),
            "video": str(player.video),
            "picture": str(player.picture),
            "teams": teams,
        }
        return json

    # Creates a player object
    # when test is true, returns the player object for inspection and doesn't change the db
    @staticmethod
    def api_post(request, test=False):
        division = models.get_model('divisions', 'Division')
        body = request.body
        body = loads(str(body))
        body["soloQueueDivision"] = division.objects.get(pk=body["soloQueueDivision"])
        player = Player(**body)
        json = {"playerName": player.playerName, "id": player.id} 
        if not test:
            player.save()
            json["id"] = player.id
            return json
        else:
            return (json, player)

    # Updates a player object
    # when test is true, returns the player object for inspection and doesn't change the db
    @staticmethod
    def api_put(id, request, test=False):
        division = models.get_model('divisions', 'Division')
        body = request.body
        body = loads(str(body))
        try:
            player = Player.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        body["soloQueueDivision"] = division.objects.get(pk=body["soloQueueDivision"])
        for key in body:
            player.__dict__[key] = body[key]
        if not test:
            player.save()
            body["soloQueueDivision"].save()
        else:
            return player

    # Deletes a player object
    # when test is true, returns the play object for inspection and doesn't change the db
    @staticmethod
    def api_delete(id, test=False):
        try:
            player = Player.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        if not test:
            player.delete()
        else:
            return player

    # method that returns a JSON object of the stuff we have to plug-in
    @staticmethod
    def get_by_id(id):
        # create a JSON-like object with everything Emir needs e.g.
        # player["playerName"], player["summonerLevel"]
        # use getTierSubtier(id)
        try:
            p = Player.objects.get(pk=id)
        except ObjectDoesNotExist:
            return {}
        divisionInfo = Player.getDivisionInfo(id)
        teamInfo = Player.getTeamInfo(id)
        player = {"id"                    : id,
                  "playerName"            : p.playerName,
                  "teamInfo"              : teamInfo,
                  "soloQueueDivisionID"   : divisionInfo[0],
                  "soloQueueDivisionName" : divisionInfo[1],
                  "summonerLevel"         : p.summonerLevel,
                  "champKills"            : p.champKills,
                  "minionKills"           : p.minionKills,
                  "wins"                  : p.wins,
                  "summonerIconID"        : p.summonerIconID,
                  "region"                : p.region,
                  "twitter"               : p.twitter,
                  "widgetID"              : p.widgetID,
                  "facebook"              : p.facebook,
                  "twitch"                : p.twitch,
                  "video"                 : p.video,
                  "picture"               : p.picture,
                  "tier"                  : divisionInfo[2],
                  "subtier"               : divisionInfo[3]
        }
        return player

    @staticmethod
    def getTeamInfo(id):
        team = models.get_model('teams', 'Team')
        try:
            t = team.objects.filter(members=id)[0]
        except ObjectDoesNotExist:
            return ["William's Combs", 21]
        return [t.teamName, t.id]

    @staticmethod
    def getDivisionInfo(id):
        division = models.get_model('divisions', 'Division')
        try:
            player = Player.objects.get(pk=id)
        except ObjectDoesNotExist:
            print("The requested player object in getDivisionInfo doesn't exist.")
        try:
            d = player.soloQueueDivision
            if(d.id == 0):
                raise ObjectDoesNotExist
        except ObjectDoesNotExist:
            return [21, "William's Combs", "Wood", "V"]
        return [d.id, d.divisionName, d.tier, d.subtier]

    def __str__(self):
        return self.playerName 

class Team(models.Model):
    """
    Class model to represent a League of Legends team.
    Contains a team's name, id, members, division, wins
    league ids, logo information, and region.
    """
    teamName = models.CharField(max_length=30)
    members = models.ManyToManyField('players.Player') # many-to-many: multiple players on one team, one player on multiple teams
    wins = models.IntegerField()
    division3v3ID = models.ForeignKey('divisions.Division', related_name='three')
    division5v5ID = models.ForeignKey('divisions.Division', related_name='five')
    logoID = models.TextField()
    region = models.CharField(max_length=5)
    twitter = models.TextField()
    widgetID = models.TextField()
    facebook = models.TextField()
    website = models.TextField()
    video = models.TextField()

    # Reads all team objects
    @staticmethod
    def api_get_all():
        teams = Team.objects.all()
        json = []
        for team in teams:
            members = Team.getPlayersInTeam(team.id)
            members = [member[1] for member in members]
            one = {
                "teamName":         str(team.teamName),
                "id":               team.id,
                "division3v3ID":    team.division3v3ID.id,
                "division5v5ID":    team.division5v5ID.id,
                "logoID":           str(team.logoID),
                "members":          members,
            }
            json.append(one)
        return json

    # Reads one team object
    @staticmethod
    def api_get_one(id):
        try:
            team = Team.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        json = {
            "teamName": str(team.teamName),
            "wins": team.wins,
            "division3v3ID": team.division3v3ID.id,
            "division5v5ID": team.division5v5ID.id,
            "logoID": str(team.logoID),
            "region": str(team.region),
            "twitter": str(team.twitter),
            "widgetID": str(team.widgetID),
            "facebook": str(team.facebook),
            "website": str(team.website),
            "video": str(team.video)
        }
        return json

    # Creates a team object
    # when test=True, is nullipotent and returns the object for inspection
    # note that when testing, the returned team object will have NO members
    #   as adding members requires saving the object to the DB
    @staticmethod
    def api_post(request, test=False):
        division = models.get_model('divisions', 'Division')
        body = request.body
        body = loads(str(body))
        team = Team(
            teamName=body["teamName"],
            wins=body["wins"],
            division3v3ID=division.objects.get(pk=body["division3v3ID"]),
            division5v5ID=division.objects.get(pk=body["division5v5ID"]),
            logoID=body["logoID"],
            region=body["region"],
            twitter=body["twitter"],
            widgetID=body["widgetID"],
            facebook=body["facebook"],
            website=body["website"],
            video=body["video"])
        json = {"teamName": team.teamName, "id": team.id}
        if not test:
            team.save()
            player = models.get_model('players', 'Player')
            # Add members here because associating teams with players requires saving first
            for member in body["players"]:
                try:
                    p = player.objects.get(pk=member[1])
                except ObjectDoesNotExist:
                    raise ValueError
                team.members.add(p)
            team.save()
            json["id"] = team.id
            return json
        else:
            return (json, team)

    # Updates a team object
    # when test=True, is nullipotent and returns the object for inspection
    @staticmethod
    def api_put(id, request, test=False):
        division = models.get_model('divisions', 'Division')
        body = request.body
        body = loads(str(body))
        try:
            team = Team.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        body["division3v3ID"] = division.objects.get(pk=body["division3v3ID"])
        body["division5v5ID"] = division.objects.get(pk=body["division5v5ID"])
        for key in body:
            team.__dict__[key] = body[key]
        if not test:
            team.save()
            player = models.get_model('players', 'Player')
            # Add members here because associating teams with players requires saving first
            for member in body["players"]:
                try:
                    p = player.objects.get(pk=member[1])
                except ObjectDoesNotExist:
                    raise ValueError
                except Exception:
                    raise ValueError
                team.members.add(p)
            team.save()
        else:
            return team

    # Deletes a team object
    # when test=True, is nullipotent and returns the object for inspection
    @staticmethod
    def api_delete(id, test=False):
        try:
            team = Team.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        if not test:
            team.delete()
        else:
            return team
    
    # method that returns a JSON object of the stuff we have to plug-in
    @staticmethod
    def get_by_id(id):
        # create a JSON-like object 
        try:
            t = Team.objects.get(pk=id)
        except ObjectDoesNotExist:
            return {}
        players = Team.getPlayersInTeam(id)
        div3 = Team.getDivisionInfo(id, 3)
        div5 = Team.getDivisionInfo(id, 5)
        team =   {"id"            : id,
                  "teamName"      : t.teamName,
                  "wins"          : t.wins,
                  "division3v3ID" : div3,
                  "division5v5ID" : div5,
                  "logoID"        : t.logoID,
                  "region"        : t.region,
                  "twitter"       : t.twitter,
                  "widgetID"      : t.widgetID,
                  "facebook"      : t.facebook,
                  "website"       : t.website,
                  "video"         : t.video,
                  "players"       : players
        }
        return team

    @staticmethod
    def getPlayersInTeam(id):
        #statement in get is equivalent to SQL's WHERE
        player = models.get_model('players', 'Player')
        players = player.objects.filter(team__id=id)
        listofplayers = []
        it = iter(players)
        for player in it:
            if player.id > 10:
                player.id = 0
            listofplayers.append([player.playerName, player.id])
        while len(listofplayers) < 5:
            listofplayers.append(["william combs", 21])
        return listofplayers

    @staticmethod
    def getDivisionInfo(id, size):
        division = models.get_model('divisions', 'Division')
        try:
            team = Team.objects.get(pk=id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist
        try:
            if(size == 3):
                d = team.division3v3ID
            else:
                d = team.division5v5ID
            if(d.id == 0):
                raise ObjectDoesNotExist
        except ObjectDoesNotExist:
            return [21, "William's Combs"]
        return [d.id, d.divisionName]

    def __str__(self):
        return self.teamName
