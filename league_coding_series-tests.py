import unittest
from django.core.exceptions import ObjectDoesNotExist
from django.test import TestCase
#from urllib.request import urlopen
#from urllib.request import Request
from json import dumps, loads
from players.models import Player
from teams.models import Team
from divisions.models import Division	
from idb.views import no_terms, one_term, multi_terms, get_results_tuple
from mockrequest import MockRequest
import ast

class OurTests (TestCase) :

    fixtures = ['initial_players', 'initial_teams', 'initial_divisions']
   
    #=========
    #API TESTS
    #=========

    # =================
    # PLAYERS
    # =================
    
    # ----------
    # GET Players
    # ----------
 
    def test_api_get_all_data_types(self):
        json = Player.api_get_all()
        self.assertTrue(len(json) == 50)
        for each in json:
            self.assertTrue(len(each) == 3)
            self.assertTrue(type(each["id"]) == int)
            self.assertTrue(type(each["summonerIconID"]) == int)
            self.assertTrue(type(each["playerName"]) == str)   
    

    def test_player_list (self):
        API_response = Player.api_get_all()
        expected_response = [
            {
                "summonerIconID": 0, 
                "id": 11, 
                "playerName": "Bjergsen"
            }, 
            {
                "summonerIconID": 0, 
                "id": 12, 
                "playerName": "WildTurtle"
            }, 
            {
                "summonerIconID": 0, 
                "id": 13, 
                "playerName": "Xpecial"
            }, 
            {
                "summonerIconID": 0, 
                "id": 14, 
                "playerName": "TheOddOne"
            }, 
            {
                "summonerIconID": 0, 
                "id": 15, 
                "playerName": "Meteos"
            }, 
            {
                "summonerIconID": 0, 
                "id": 16, 
                "playerName": "Balls"
            }, 
            {
                "summonerIconID": 0, 
                "id": 17, 
                "playerName": "Sneaky"
            }, 
            {
                "summonerIconID": 0, 
                "id": 18, 
                "playerName": "LemonNation"
            }, 
            {
                "summonerIconID": 0, 
                "id": 19, 
                "playerName": "Cruzerthebruzer"
            }, 
            {
                "summonerIconID": 0, 
                "id": 20, 
                "playerName": "Imaqtpie"
            }, 
            {
                "summonerIconID": 0, 
                "id": 21, 
                "playerName": "Crumbzz"
            }, 
            {
                "summonerIconID": 0, 
                "id": 22, 
                "playerName": "goldenglue"
            }, 
            {
                "summonerIconID": 0, 
                "id": 23, 
                "playerName": "dexter"
            }, 
            {
                "summonerIconID": 0, 
                "id": 24, 
                "playerName": "Link"
            }, 
            {
                "summonerIconID": 0, 
                "id": 25, 
                "playerName": "Aphromoo"
            }, 
            {
                "summonerIconID": 0, 
                "id": 26, 
                "playerName": "Nien"
            }, 
            {
                "summonerIconID": 0, 
                "id": 27, 
                "playerName": "IWillDominate"
            }, 
            {
                "summonerIconID": 0, 
                "id": 28, 
                "playerName": "Quas"
            }, 
            {
                "summonerIconID": 0, 
                "id": 29, 
                "playerName": "Cop"
            }, 
            {
                "summonerIconID": 0, 
                "id": 30, 
                "playerName": "Bunny FuFuu"
            }, 
            {
                "summonerIconID": 0, 
                "id": 31, 
                "playerName": "Krepo"
            }, 
            {
                "summonerIconID": 0, 
                "id": 32, 
                "playerName": "Yellowpete"
            }, 
            {
                "summonerIconID": 0, 
                "id": 33, 
                "playerName": "Innox"
            }, 
            {
                "summonerIconID": 0, 
                "id": 34, 
                "playerName": "Pobelter"
            }, 
            {
                "summonerIconID": 0, 
                "id": 35, 
                "playerName": "NintendudeX"
            }, 
            {
                "summonerIconID": 0, 
                "id": 36, 
                "playerName": "ZionSpartan"
            }, 
            {
                "summonerIconID": 0, 
                "id": 37, 
                "playerName": "Daydreamin"
            }, 
            {
                "summonerIconID": 0, 
                "id": 38, 
                "playerName": "WizFujiiN"
            }, 
            {
                "summonerIconID": 0, 
                "id": 39, 
                "playerName": "Sheep"
            }, 
            {
                "summonerIconID": 0, 
                "id": 40, 
                "playerName": "mancloud"
            }, 
            {
                "summonerIconID": 0, 
                "id": 41, 
                "playerName": "Xmithie"
            }, 
            {
                "summonerIconID": 0, 
                "id": 42, 
                "playerName": "Benny"
            }, 
            {
                "summonerIconID": 0, 
                "id": 43, 
                "playerName": "Nyph"
            }, 
            {
                "summonerIconID": 0, 
                "id": 44, 
                "playerName": "Tabzz"
            }, 
            {
                "summonerIconID": 0, 
                "id": 45, 
                "playerName": "Shook"
            }, 
            {
                "summonerIconID": 0, 
                "id": 46, 
                "playerName": "Wickd"
            }, 
            {
                "summonerIconID": 0, 
                "id": 47, 
                "playerName": "YellOwStaR"
            }, 
            {
                "summonerIconID": 0, 
                "id": 48, 
                "playerName": "Cyanide"
            }, 
            {
                "summonerIconID": 0, 
                "id": 49, 
                "playerName": "sOAZ"
            }, 
            {
                "summonerIconID": 0, 
                "id": 50, 
                "playerName": "Rekkles"
            }, 
            {
                "summonerIconID": 627, 
                "id": 1, 
                "playerName": "Dyrus"
            }, 
            {
                "summonerIconID": 504, 
                "id": 2, 
                "playerName": "d KiWiKid"
            }, 
            {
                "summonerIconID": 28, 
                "id": 3, 
                "playerName": "Froggen"
            }, 
            {
                "summonerIconID": 625, 
                "id": 4, 
                "playerName": "Hai"
            }, 
            {
                "summonerIconID": 621, 
                "id": 5, 
                "playerName": "Doublelift"
            }, 
            {
                "summonerIconID": 624, 
                "id": 6, 
                "playerName": "Voyboy"
            }, 
            {
                "summonerIconID": 9, 
                "id": 7, 
                "playerName": "Snoopeh"
            }, 
            {
                "summonerIconID": 0, 
                "id": 8, 
                "playerName": "Shiphtur"
            }, 
            {
                "summonerIconID": 28, 
                "id": 9, 
                "playerName": "ZuNa"
            }, 
            {
                "summonerIconID": 502, 
                "id": 10, 
                "playerName": "xPeke"
            }
        ]     

        self.assertTrue(sorted(expected_response) == sorted(API_response))

    # ---------
    # GET Player
    # ---------    
    
    def test_get_player (self):
        API_response = Player.api_get_one(2)
        expected_response = {
                'picture': '/static/img/kiwikid.jpg', 
                'video': 'B170s0wQDT8', 
                'widgetID': '451162742070595585', 
                'summonerIconID': '504', 
                'twitter': 'dKiWiKiD', 
                'playerName': 'd KiWiKid', 
                'wins': 407, 
                'soloQueueDivision': 2, 
                'teams': [
                            {
                                'id': 3, 
                                'teamName': u'Team Dignitas'
                            }
                         ], 
                 'facebook': 'KiWiKiDLoL', 
                 'minionKills': 98897, 
                 'summonerLevel': 30, 
                 'region': 'NA', 
                 'champKills': 5953, 
                 'twitch': 'kiwikid', 
                 'id': 2
        }
        self.assertTrue(sorted(API_response) == sorted(expected_response))

    def test_get_player_failure (self):
        self.assertRaises(ObjectDoesNotExist, Player.api_get_one, 51)

    # ----------    
    # POST Player
    # ----------    
    
    def test_make_player (self):
        values = {
	      "playerName": "SuperEric",
	      "soloQueueDivision": 1,
	      "summonerLevel": 30,
	      "champKills": 3715,
	      "minionKills": 90658,
	      "wins": 880,
	      "summonerIconID": 627,
	      "region": "NA",
	      "twitter": "LoLDyrus",
	      "widgetID": "451151635889143808",
	      "facebook": "TSMDYRUSLOL",
	      "twitch": "TSM_Dyrus",
	      "video": "Ey8GhOi2JXU",
	      "picture": "/static/img/dyrus.png"
	    }
        self.assertRaises(ObjectDoesNotExist, Player.api_get_one, 51)
        API_response = Player.api_post(MockRequest(body=values))
        expected_response =  {"playerName": "SuperEric", "id": 51} 
        self.assertTrue(sorted(API_response) == sorted(expected_response))
        self.assertTrue(Player.api_get_one(51).has_key("playerName"))
 
    # ----------        
    # PUT Player
    # ----------    
    def test_edit_player (self):
        values = {
                'twitch': 'TSM_Dyrus', 
                'soloQueueDivision': 1, 
                'picture': '/static/img/dyrus.png', 
                'video': 'Ey8GhOi2JXU', 
                'twitter': 'LoLDyrus', 
                'wins': 880, 
                'minionKills': 10658, 
                'summonerIconID': '627', 
                'facebook': 'TSMDYRUSLOL', 
                'widgetID': '451151635889143808', 
                'region': 'NA', 
                'champKills': 3715, 
                'playerName': 'EDITED GUY', 
                'summonerLevel': 30
        }
        Player.api_put(1, MockRequest(body=values))   
        self.assertTrue(Player.api_get_one(1)["playerName"] == "EDITED GUY")
        self.assertRaises(ObjectDoesNotExist, Player.api_put, 51, MockRequest(body=values))

    # ------------    
    # DELETE Player
    # ------------
    
    def test_delete_player (self):
        self.assertTrue(Player.api_get_one(1).has_key("playerName"))
        Player.api_delete(1)
        self.assertRaises(ObjectDoesNotExist, Player.api_get_one, 1)
        self.assertRaises(ObjectDoesNotExist, Player.api_delete, 1)
        self.assertRaises(ObjectDoesNotExist, Player.api_delete, 51)
    
    # =================
    # TEAMS
    # =================
    
    # --------
    # GET Teams
    # --------
    def test_team_list (self):
        API_response = Team.api_get_all()
        
        expected_response = [
            {
                "logoID": "/static/img/TSM.png", 
                "division5v5ID": 4, 
                "teamName": "Team SoloMid", 
                "members": [
                    1, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 1, 
                "division3v3ID": 9
            }, 
            {
                "logoID": "/static/img/coast.png", 
                "division5v5ID": 4, 
                "teamName": "Team Coast", 
                "members": [
                    8, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 7, 
                "division3v3ID": 9
            }, 
            {
                "logoID": "/static/img/XDG.png", 
                "division5v5ID": 4, 
                "teamName": "XDG Gaming", 
                "members": [
                    9, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 8, 
                "division3v3ID": 9
            }, 
            {
                "logoID": "/static/img/alliance.png", 
                "division5v5ID": 5, 
                "teamName": "Alliance", 
                "members": [
                    3, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 9, 
                "division3v3ID": 10
            }, 
            {
                "logoID": "/static/img/fnatic.png", 
                "division5v5ID": 5, 
                "teamName": "Fnatic", 
                "members": [
                    10, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 10, 
                "division3v3ID": 10
            }, 
            {
                "logoID": "/static/img/Cloud9.png", 
                "division5v5ID": 4, 
                "teamName": "Cloud 9 HyperX", 
                "members": [
                    4, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 2, 
                "division3v3ID": 9
            }, 
            {
                "logoID": "/static/img/dig.png", 
                "division5v5ID": 4, 
                "teamName": "Team Dignitas", 
                "members": [
                    2, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 3, 
                "division3v3ID": 10
            }, 
            {
                "logoID": "/static/img/clg.png", 
                "division5v5ID": 4, 
                "teamName": "Counter Logic Gaming", 
                "members": [
                    5, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 4, 
                "division3v3ID": 9
            }, 
            {
                "logoID": "/static/img/curse.png", 
                "division5v5ID": 4, 
                "teamName": "Curse", 
                "members": [
                    6, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 5, 
                "division3v3ID": 10
            }, 
            {
                "logoID": "/static/img/EG.png", 
                "division5v5ID": 4, 
                "teamName": "Evil Geniuses", 
                "members": [
                    7, 
                    0, 
                    0, 
                    0, 
                    0
                ], 
                "id": 6, 
                "division3v3ID": 9
            }
        ]        
        self.assertTrue(sorted(expected_response) == sorted(API_response))
    
    # -------
    # GET Team
    # -------
    
    def test_get_team (self):
        API_response = Team.api_get_one(2)
        expected_response = {
                "website": "http://cloud9.gg/", 
                "widgetID": "451539878925197312", 
                "twitter": "Cloud9gg", 
                "division3v3ID": 9, 
                "video": "MnyT_NMtMy4", 
                "division5v5ID": 4, 
                "logoID": "/static/img/Cloud9.png", 
                "wins": 17, 
                "region": "NA", 
                "teamName": "Cloud 9 HyperX", 
                "facebook": "cloud9gg"
        }
        self.assertTrue(sorted(expected_response) == sorted(API_response))

    # --------
    # POST Team
    # --------

    def test_make_team (self):
        values = {
              "teamName": "Team Madeupguys",
              "wins": 700,
              "division3v3ID": 1,
              "division5v5ID": 1,
              "logoID": 1,
              "region": "NA",
              "twitter": "https://twitter.com/TeamSoloMid",
              "widgetID": "somebologna",
              "facebook": "https://www.facebook.com/TSMPRO",
              "website": "http://www.solomid.net/",
              "video": "https://www.youtube.com/watch?v=83ST1-OmGQ8",
              "players": [
                ["Dyrus",1],
                ["TheOddOne",2],
                ["Bjergsen",3],
                ["WildTurtle",4],
                ["Xpecial",5]
              ]
            }
        expected_response = { "teamName" : "Team Madeupguys", "id": 11 }
        self.assertRaises(ObjectDoesNotExist, Team.api_get_one, 11)
        API_response = Team.api_post(MockRequest(body=values))
        self.assertTrue(sorted(API_response) == sorted(expected_response))
        self.assertTrue(Team.api_get_one(11)["teamName"] == "Team Madeupguys")

    # --------    
    # PUT Team
    # --------
    
    def test_edit_team (self):
        values = {
              "teamName": "Team Editedguys",
              "wins": 700,
              "division3v3ID": 1,
              "division5v5ID": 1,
              "logoID": 1,
              "region": "NA",
              "twitter": "https://twitter.com/TeamSoloMid",
              "facebook": "https://www.facebook.com/TSMPRO",
              "website": "http://www.solomid.net/",
              "video": "https://www.youtube.com/watch?v=83ST1-OmGQ8",
              "players": [
                ["Dyrus",1],
                ["TheOddOne",2],
                ["Bjergsen",3],
                ["WildTurtle",4],
                ["Xpecial",5]
              ]
            }
        API_response = Team.api_put(1, MockRequest(body=values))
        self.assertTrue(Team.api_get_one(1)["teamName"] == "Team Editedguys")
        self.assertRaises(ObjectDoesNotExist, Team.api_put, 11, MockRequest(body=values))

    # ----------
    # DELETE Team
    # ----------   

    #change this to validate the 204 response instead    
    def test_delete_team (self):
        self.assertTrue(Team.api_get_one(1).has_key("teamName"))
        Team.api_delete(1)
        self.assertRaises(ObjectDoesNotExist, Team.api_get_one, 1)
        self.assertRaises(ObjectDoesNotExist, Team.api_delete, 1)
        self.assertRaises(ObjectDoesNotExist, Team.api_delete, 11)
 
    # =================
    # DIVISIONS
    # =================

    # ------------
    # GET Divisions
    # ------------

    def test_division_list (self):        
        API_response = Division.api_get_all()
        expected_response = [ { "id": 0, "divisionName": "William's Combs" }, { "id": 1, "divisionName": "Hecarim's Duelists" }, { "id": 2, "divisionName": "Vladimir's Maulers" }, { "id": 3, "divisionName": "Lux's Enforcers" }, { "id": 4, "divisionName": "NA LCS" }, { "id": 5, "divisionName": "EU LCS" }, { "id": 6, "divisionName": "Akali's Army" }, { "id": 7, "divisionName": "Gangplank's Backstabbers" }, { "id": 8, "divisionName": "Lucian's Lancers" }, { "id": 9, "divisionName": "Maokai's Musketeers" }, { "id": 10, "divisionName": "Ocelot's Yumpers" } ]
        self.assertTrue(sorted(API_response) == sorted(expected_response))
    
    # -----------    
    # GET Division
    # -----------
    
    def test_get_division (self):

        API_response = Division.api_get_one(1)
        expected_response = {"id": 1,"divisionName": "Hecarim's Duelists","tier": "Challenger","subtier": "I","region": "NA","video": "https://www.youtube.com/watch?v=3GxZ4JnN3Uc","picture": "/static/img/bronze4.png","players": [["Dyrus",1],["xxxSniperxxx",10],["NoScope360qq",201]]    }        
        self.assertTrue(sorted(API_response) == sorted(expected_response))
        
    # ------------
    # POST Division
    # ------------    

    def test_make_division (self):
        values = {
              "divisionName": "Hecarim's Duelists",
              "tier": "Challenger",
              "subtier": "I",
              "region": "NA",
              "video": "https://www.youtube.com/watch?v=3GxZ4JnN3Uc",
              "picture": "/static/bullsiht"
            }
        
        self.assertRaises(ObjectDoesNotExist , Division.api_get_one, 11)
        API_response = Division.api_post(MockRequest(body=values))
        expected_response = { "divisionName" : "Hecarim's Duelists", "picture": "/static/bullsiht"}
        self.assertTrue(sorted(API_response) == sorted(expected_response))
        
    # --------------    
    # DELETE Division
    # --------------    
    
    def test_delete_division (self):
        Division.api_delete(1)
        self.assertRaises(ObjectDoesNotExist, Division.api_get_one, 1)
    
    # ------------    
    # PUT Division
    # ------------
    
    def test_edit_division (self):
        self.assertTrue(Division.api_get_one(1)["divisionName"] == "Hecarim's Duelists")
        values = {
              "divisionName": "Eric's Division",
              "tier": "Challenger",
              "subtier": "I",
              "region": "NA",
              "video": "https://www.youtube.com/watch?v=3GxZ4JnN3Uc",
              "divisionIconID": 1,
              "players": [
                ["Dyrus",1],
                ["xxxSniperxxx",10],
                ["NoScope360qq",201]
              ]
              
              
            }        
        Division.api_put(1, MockRequest(body=values))
        self.assertTrue(Division.api_get_one(1)["divisionName"]=="Eric's Division")

    #============
    #Models Tests
    #============

    #================
    #Player get_by_id
    #================
    
    def test_player_get_by_id_1 (self):
        result = Player.get_by_id(1)
        expected_response = {
        'picture': '/static/img/dyrus.png',
        'video': 'Ey8GhOi2JXU',
        'widgetID': '451151635889143808',
        'subtier': 'I',
        'twitter': 'LoLDyrus',
        'twitch': 'TSM_Dyrus',
        'playerName': 'Dyrus',
        'wins': 880,
        'summonerIconID': 627,
        'teamInfo': [
            'Team SoloMid',
            1
        ],
        'facebook': 'TSMDYRUSLOL',
        'minionKills': 10658,
        'summonerLevel': 30,
        'tier': 'Challenger',
        'region': 'NA',
        'champKills': 3715,
        'soloQueueDivisionName': "Hecarim's Duelists",
        'id': 1,
        'soloQueueDivisionID': 1
        }
        
        """
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(sorted(expected_response)[i] == sorted(result)[i])  
        """
        
        self.assertTrue(sorted(result) == sorted(expected_response))
    
    def test_player_get_by_id_2 (self):
        result = Player.get_by_id(2)
        expected_response = {
        "picture": "/static/img/kiwikid.jpg",
        "video": "B170s0wQDT8",
        "widgetID": "451162742070595585",
        "subtier": "V",
        "twitter": "dKiWiKiD",
        "twitch": "kiwikid",
        "playerName": "d KiWiKid",
        "wins": 407,
        "summonerIconID": 504,
        "teamInfo": [
            "Team Dignitas",
            3
        ],
        "facebook": "KiWiKiDLoL",
        "minionKills": 98897,
        "summonerLevel": 30,
        "tier": "Diamond",
        "region": "NA",
        "champKills": 5953,
        "soloQueueDivisionName": "Vladimir's Maulers",
        "id": 2,
        "soloQueueDivisionID": 2
        }
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
    
    def test_player_get_by_id_3 (self):
        result = Player.get_by_id(3)
        expected_response = {
	    "picture": "/static/img/FROGGEN.jpg",
	    "video": "57s_nwoZlPQ",
	    "widgetID": "451475151570231296",
	    "subtier": "III",
	    "twitter": "FroggenLoL",
	    "twitch": "froggen",
	    "playerName": "Froggen",
	    "wins": 2106,
	    "summonerIconID": 28,
	    "teamInfo": [
		"Alliance",
		9
	    ],
	    "facebook": "Froggen.LoL",
	    "minionKills": 657504,
	    "summonerLevel": 30,
	    "tier": "Bronze",
	    "region": "EUW",
	    "champKills": 37874,
	    "soloQueueDivisionName": "Lux's Enforcers",
	    "id": 3,
	    "soloQueueDivisionID": 3
	    }
            
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        
        #self.assertTrue(result == expected_response)
        
    #--------------------
    # Players getTeamInfo
    #--------------------

    def test_players_getTeamInfo_1 (self):
        result = Player.getTeamInfo(1)
        expected_response = ['Team SoloMid', 1]
        self.assertTrue(result == expected_response)
    
    def test_players_getTeamInfo_2 (self):
        result = Player.getTeamInfo(2)
        expected_response = ['Team Dignitas', 3]
        self.assertTrue(result == expected_response)
    
    def test_players_getTeamInfo_3 (self):
        result = Player.getTeamInfo(3)
        expected_response = ['Alliance', 9]
        self.assertTrue(result == expected_response)
    
    
    #Players getDivisionInfo
    def test_players_getDivisionInfo_1 (self):
        result = Player.getDivisionInfo(1)
        expected_response = [1, "Hecarim's Duelists", 'Challenger', 'I']
        self.assertTrue(result == expected_response)
    
    def test_players_getDivisionInfo_2 (self):
        result = Player.getDivisionInfo(2)
        expected_response = [2, "Vladimir's Maulers", 'Diamond', 'V']
        self.assertTrue(result == expected_response)
        
    def test_players_getDivisionInfo_3 (self):
        result = Player.getDivisionInfo(3)
        expected_response = [3, "Lux's Enforcers", 'Bronze', 'III']
        self.assertTrue(result == expected_response)
                 
    #================
    #Team get_by_id
    #================
    
    def test_team_get_by_id_1 (self):
        result = Team.get_by_id(1) 
        expected_response = {'website': 'http://www.tsm.gg/', 'widgetID': '451540335953342465', 'twitter': 'TeamSoloMid', 'division3v3ID': [9, "Maokai's Musketeers"], 'facebook': 'TSMPRO', 'division5v5ID': [4, 'NA LCS'], 'logoID': '/static/img/TSM.png', 'id': 1, 'wins': 17, 'region': 'NA', 'teamName': 'Team SoloMid', 'players': [['Dyrus', 1], ['Bjergsen', 0], ['WildTurtle', 0], ['Xpecial', 0], ['TheOddOne', 0]], 'video': '83ST1-OmGQ8'}			    
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
    
    
    def test_team_get_by_id_2 (self):
        result = Team.get_by_id(2)      
        expected_response = {'website': 'http://cloud9.gg/', 'widgetID': '451539878925197312', 'twitter': 'Cloud9gg', 'division3v3ID': [9, "Maokai's Musketeers"], 'facebook': 'cloud9gg', 'division5v5ID': [4, 'NA LCS'], 'logoID': '/static/img/Cloud9.png', 'id': 2, 'wins': 17, 'region': 'NA', 'teamName': 'Cloud 9 HyperX', 'players': [['Hai', 4],['Meteos', 0], ['Balls', 0], ['Sneaky', 0], ['LemonNation', 0]], 'video': 'MnyT_NMtMy4'}
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
    
    def test_team_get_by_id_3 (self):
        result = Team.get_by_id(3)  
        
        expected_response = {'website': 'http://www.team-dignitas.net/', 'widgetID': '451540000471932928', 'twitter': 'TeamDignitasd', 'division3v3ID': [10, "Ocelot's Yumpers"], 'facebook': 'teamdignitas', 'division5v5ID': [4, 'NA LCS'], 'logoID': '/static/img/dig.png', 'id': 3, 'wins': 17, 'region': 'NA', 'teamName': 'Team Dignitas', 'players': [['d KiWiKid', 2],['Cruzerthebruzer', 0], ['Imaqtpie', 0], ['Crumbzz', 0], ['goldenglue', 0]], 'video': '21qlCPRR32g'}
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])    
        
        #self.assertTrue(result == expected_response)
        
    #=====================
    #Team getPlayersInTeam
    #=====================
    
    def test_team_getPlayersInTeam_1 (self):
        result = Team.getPlayersInTeam(1)
        expected_response = [['Dyrus', 1],['Bjergsen', 0], ['WildTurtle', 0], ['Xpecial', 0], ['TheOddOne', 0]]
        for i in expected_response:
            self.assertTrue(i in result)
        #self.assertTrue(result == expected_response)
    
    def test_team_getPlayersInTeam_2 (self):
        result = Team.getPlayersInTeam(2)
        expected_response = [ ['Hai', 4],['Meteos', 0], ['Balls', 0], ['Sneaky', 0], ['LemonNation', 0]]
        for i in expected_response:
            self.assertTrue(i in result)
        #self.assertTrue(result == expected_response)
        
    def test_team_getPlayersInTeam_3 (self):
        result = Team.getPlayersInTeam(3)
        expected_response = [['d KiWiKid', 2],['Cruzerthebruzer', 0], ['Imaqtpie', 0], ['Crumbzz', 0], ['goldenglue', 0]]
        for i in expected_response:
            self.assertTrue(i in result)
        #self.assertTrue(result == expected_response)
            
        
    #Team getDivisionInfo
    
    def test_team_getDivisionInfo_1 (self):
        result = Team.getDivisionInfo(1,5)
        expected_response = [4, 'NA LCS']              
        self.assertTrue(result == expected_response)
        
    def test_team_getDivisionInfo_2 (self):
        result = Team.getDivisionInfo(2,5)
        expected_response = [4, 'NA LCS']               
        self.assertTrue(result == expected_response)
        
    def test_team_getDivisionInfo_3 (self):
        result = Team.getDivisionInfo(3,3)
        expected_response = [10, "Ocelot's Yumpers"]               
        self.assertTrue(result == expected_response)
    
    #==================
    #Division get_by_id
    #==================
    
    def test_division_get_by_id_1 (self):
        result = Division.get_by_id(1)
        expected_response = {'picture': '/static/img/challenger.png', 'video': '3GxZ4JnN3Uc', 'players': [['Dyrus', 1, 'players'], ['Snoopeh', 7, 'players'], ['Xpecial', 13, 'players'], ['Cruzerthebruzer', 19, 'players'], ['Aphromoo', 25, 'players'], ['Krepo', 31, 'players'], ['Daydreamin', 37, 'players'], ['Nyph', 43, 'players'], ['sOAZ', 49, 'players']], 'tier': 'Challenger', 'subtier': 'I', 'region': 'NA', 'entitytype': 'Players', 'divisionName': "Hecarim's Duelists", 'id': 1}
        
        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
    
    def test_division_get_by_id_2 (self):
        result = Division.get_by_id(2)
        expected_response = {'picture': '/static/img/diav.png', 'video': 'difd-0y7y4M', 'players': [['d KiWiKid', 2, 'players'], ['Shiphtur', 8, 'players'], ['TheOddOne', 14, 'players'], ['Imaqtpie', 20, 'players'], ['Nien', 26, 'players'], ['Yellowpete', 32, 'players'], ['WizFujiiN', 38, 'players'], ['Tabzz', 44, 'players'], ['Rekkles', 50, 'players']], 'tier': 'Diamond', 'subtier': 'V', 'region': 'NA', 'entitytype': 'Players', 'divisionName': "Vladimir's Maulers", 'id': 2}

        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
    
    def test_division_get_by_id_3 (self):
        result = Division.get_by_id(3)
        expected_response = {'picture': '/static/img/bronzeiii.png', 'video': 'Ci3-OaSXx_4', 'players': [['Froggen', 3, 'players'], ['ZuNa', 9, 'players'], ['Meteos', 15, 'players'], ['Crumbzz', 21, 'players'], ['IWillDominate', 27, 'players'], ['Innox', 33, 'players'], ['Sheep', 39, 'players'], ['Shook', 45, 'players']], 'tier': 'Bronze', 'subtier': 'III', 'region': 'NA', 'entitytype': 'Players', 'divisionName': "Lux's Enforcers", 'id': 3}

        for i in expected_response:                  
            if(type(expected_response[i]) == list):
                for j in expected_response[i]:
                    self.assertTrue(j in result[i])
            else:
                self.assertTrue(expected_response[i] == result[i])  
        
        #self.assertTrue(result == expected_response)
           
    #==========================================
    #Division getPlayersFromSoloQueueDivisionID
    #==========================================
    
    def test_division_getPlayersFromSoloQueueDivisionID_1 (self):
        result = Division.getPlayersFromSoloQueueDivisionID(1)        
        expected_response = [['Dyrus', 1, 'players'], ['Snoopeh', 7, 'players'], ['Xpecial', 13, 'players'], ['Cruzerthebruzer', 19, 'players'], ['Aphromoo', 25, 'players'], ['Krepo', 31, 'players'], ['Daydreamin', 37, 'players'], ['Nyph', 43, 'players'], ['sOAZ', 49, 'players']]
        
        for i in expected_response:
            self.assertTrue(i in result)
        
        #self.assertTrue(result == expected_response)
        
    def test_division_getPlayersFromSoloQueueDivisionID_2 (self):
        result = Division.getPlayersFromSoloQueueDivisionID(2)
        expected_response = [['d KiWiKid', 2, 'players'], ['Shiphtur', 8, 'players'], ['TheOddOne', 14, 'players'], ['Imaqtpie', 20, 'players'], ['Nien', 26, 'players'], ['Yellowpete', 32, 'players'], ['WizFujiiN', 38, 'players'], ['Tabzz', 44, 'players'], ['Rekkles', 50, 'players']]
        
        for i in expected_response:
            self.assertTrue(i in result)
        
        #self.assertTrue(result == expected_response)
        
    def test_division_getPlayersFromSoloQueueDivisionID_3 (self):
        result = Division.getPlayersFromSoloQueueDivisionID(3)
        expected_response = [['Froggen', 3, 'players'], ['ZuNa', 9, 'players'], ['Meteos', 15, 'players'], ['Crumbzz', 21, 'players'], ['IWillDominate', 27, 'players'], ['Innox', 33, 'players'], ['Sheep', 39, 'players'], ['Shook', 45, 'players']]
        
        for i in expected_response:
            self.assertTrue(i in result)
        
        #self.assertTrue(result == expected_response)

    #============================================
    # Search Tests
    #============================================
    
    def test_no_terms (self):
        self.assertTrue(no_terms() == {})

    def test_one_term (self):
        result = one_term("Dyrus")
        self.assertTrue(result.has_key("players"))
        self.assertTrue(result.has_key("player"))
        self.assertTrue(result.has_key("playert"))
        self.assertTrue(result.has_key("playet"))
        self.assertTrue(result.has_key("playerd"))
        self.assertTrue(result.has_key("played"))

        self.assertTrue(result.has_key("teams"))
        self.assertTrue(result.has_key("team"))
        self.assertTrue(result.has_key("teamp"))
        self.assertTrue(result.has_key("teap"))
        self.assertTrue(result.has_key("teamd"))
        self.assertTrue(result.has_key("tead"))

        self.assertTrue(result.has_key("divisions"))
        self.assertTrue(result.has_key("division"))
        self.assertTrue(result.has_key("divisiont"))
        self.assertTrue(result.has_key("divisiot"))
        self.assertTrue(result.has_key("divisionp"))
        self.assertTrue(result.has_key("divisiop"))

        self.assertTrue(result.has_key("term"))

        self.assertTrue(result["players"].first().playerName == "Dyrus")
        self.assertTrue(result["teamp"].first().teamName == "Team SoloMid")
        self.assertTrue(result["divisionp"].first().divisionName == "Hecarim's Duelists")
        
    def test_one_term_2 (self):
        result = one_term("Solomid")
        self.assertTrue(result.has_key("players"))
        self.assertTrue(result.has_key("player"))
        self.assertTrue(result.has_key("playert"))
        self.assertTrue(result.has_key("playet"))
        self.assertTrue(result.has_key("playerd"))
        self.assertTrue(result.has_key("played"))

        self.assertTrue(result.has_key("teams"))
        self.assertTrue(result.has_key("team"))
        self.assertTrue(result.has_key("teamp"))
        self.assertTrue(result.has_key("teap"))
        self.assertTrue(result.has_key("teamd"))
        self.assertTrue(result.has_key("tead"))

        self.assertTrue(result.has_key("divisions"))
        self.assertTrue(result.has_key("division"))
        self.assertTrue(result.has_key("divisiont"))
        self.assertTrue(result.has_key("divisiot"))
        self.assertTrue(result.has_key("divisionp"))
        self.assertTrue(result.has_key("divisiop"))

        self.assertTrue(result.has_key("term"))

        self.assertTrue(result["playert"].first().playerName == "Bjergsen")
        self.assertTrue(result["teams"].first().teamName == "Team SoloMid")
        self.assertTrue(result["divisiont"].first().divisionName == "Maokai's Musketeers")
        
    def test_one_term_3 (self):
        result = one_term("Hecarim")
        self.assertTrue(result.has_key("players"))
        self.assertTrue(result.has_key("player"))
        self.assertTrue(result.has_key("playert"))
        self.assertTrue(result.has_key("playet"))
        self.assertTrue(result.has_key("playerd"))
        self.assertTrue(result.has_key("played"))

        self.assertTrue(result.has_key("teams"))
        self.assertTrue(result.has_key("team"))
        self.assertTrue(result.has_key("teamp"))
        self.assertTrue(result.has_key("teap"))
        self.assertTrue(result.has_key("teamd"))
        self.assertTrue(result.has_key("tead"))

        self.assertTrue(result.has_key("divisions"))
        self.assertTrue(result.has_key("division"))
        self.assertTrue(result.has_key("divisiont"))
        self.assertTrue(result.has_key("divisiot"))
        self.assertTrue(result.has_key("divisionp"))
        self.assertTrue(result.has_key("divisiop"))

        self.assertTrue(result.has_key("term"))

        self.assertTrue(result["playerd"].first().playerName == "Aphromoo")
        self.assertTrue(result["divisions"].first().divisionName == "Hecarim's Duelists")

    def test_multi_terms_no_and (self):
        result = multi_terms(["Dyrus","Shiphtur"])
        self.assertTrue(result.has_key("andplayersname"))
        self.assertTrue(result.has_key("orplayersname"))
        self.assertTrue(result.has_key("andplayersteam"))
        self.assertTrue(result.has_key("orplayersteam"))
        self.assertTrue(result.has_key("andplayersdivs"))
        self.assertTrue(result.has_key("orplayersdivs"))
        self.assertTrue(result.has_key("andplayersnamesi"))
        self.assertTrue(result.has_key("orplayersnamesi"))
        self.assertTrue(result.has_key("andplayersteamsi"))
        self.assertTrue(result.has_key("orplayersteamsi"))
        self.assertTrue(result.has_key("andplayersdivssi"))
        self.assertTrue(result.has_key("orplayersdivssi"))

        self.assertTrue(result.has_key("andteamsname"))
        self.assertTrue(result.has_key("orteamsname"))
        self.assertTrue(result.has_key("andteamsplay"))
        self.assertTrue(result.has_key("orteamsplay"))
        self.assertTrue(result.has_key("andteamsdivs"))
        self.assertTrue(result.has_key("orteamsdivs"))
        self.assertTrue(result.has_key("andteamsnamesi"))
        self.assertTrue(result.has_key("orteamsnamesi"))
        self.assertTrue(result.has_key("andteamsplaysi"))
        self.assertTrue(result.has_key("orteamsplaysi"))
        self.assertTrue(result.has_key("andteamsdivssi"))
        self.assertTrue(result.has_key("orteamsdivssi"))

        self.assertTrue(result.has_key("anddivisionsname"))
        self.assertTrue(result.has_key("ordivisionsname"))
        self.assertTrue(result.has_key("anddivisionsteam"))
        self.assertTrue(result.has_key("ordivisionsteam"))
        self.assertTrue(result.has_key("anddivisionsplay"))
        self.assertTrue(result.has_key("ordivisionsplay"))
        self.assertTrue(result.has_key("anddivisionsnamesi"))
        self.assertTrue(result.has_key("ordivisionsnamesi"))
        self.assertTrue(result.has_key("anddivisionsteamsi"))
        self.assertTrue(result.has_key("ordivisionsteamsi"))
        self.assertTrue(result.has_key("anddivisionsplaysi"))
        self.assertTrue(result.has_key("ordivisionsplaysi"))

        self.assertTrue(result.has_key("terms"))

        self.assertTrue(result["orplayersname"].first().playerName == "Dyrus")
        self.assertTrue(result["orteamsplay"].first().teamName == "Team Coast")
        self.assertTrue(result["ordivisionsplay"].first().divisionName == "Hecarim's Duelists")
        
    def test_multi_terms_no_and_2 (self):
        result = multi_terms(["Cop","op"])
        self.assertTrue(result.has_key("andplayersname"))
        self.assertTrue(result.has_key("orplayersname"))
        self.assertTrue(result.has_key("andplayersteam"))
        self.assertTrue(result.has_key("orplayersteam"))
        self.assertTrue(result.has_key("andplayersdivs"))
        self.assertTrue(result.has_key("orplayersdivs"))
        self.assertTrue(result.has_key("andplayersnamesi"))
        self.assertTrue(result.has_key("orplayersnamesi"))
        self.assertTrue(result.has_key("andplayersteamsi"))
        self.assertTrue(result.has_key("orplayersteamsi"))
        self.assertTrue(result.has_key("andplayersdivssi"))
        self.assertTrue(result.has_key("orplayersdivssi"))

        self.assertTrue(result.has_key("andteamsname"))
        self.assertTrue(result.has_key("orteamsname"))
        self.assertTrue(result.has_key("andteamsplay"))
        self.assertTrue(result.has_key("orteamsplay"))
        self.assertTrue(result.has_key("andteamsdivs"))
        self.assertTrue(result.has_key("orteamsdivs"))
        self.assertTrue(result.has_key("andteamsnamesi"))
        self.assertTrue(result.has_key("orteamsnamesi"))
        self.assertTrue(result.has_key("andteamsplaysi"))
        self.assertTrue(result.has_key("orteamsplaysi"))
        self.assertTrue(result.has_key("andteamsdivssi"))
        self.assertTrue(result.has_key("orteamsdivssi"))

        self.assertTrue(result.has_key("anddivisionsname"))
        self.assertTrue(result.has_key("ordivisionsname"))
        self.assertTrue(result.has_key("anddivisionsteam"))
        self.assertTrue(result.has_key("ordivisionsteam"))
        self.assertTrue(result.has_key("anddivisionsplay"))
        self.assertTrue(result.has_key("ordivisionsplay"))
        self.assertTrue(result.has_key("anddivisionsnamesi"))
        self.assertTrue(result.has_key("ordivisionsnamesi"))
        self.assertTrue(result.has_key("anddivisionsteamsi"))
        self.assertTrue(result.has_key("ordivisionsteamsi"))
        self.assertTrue(result.has_key("anddivisionsplaysi"))
        self.assertTrue(result.has_key("ordivisionsplaysi"))

        self.assertTrue(result.has_key("terms"))

        self.assertTrue(result["andplayersname"].first().playerName == "Cop")
        self.assertTrue(result["andteamsplay"].first().teamName == "Curse")
        self.assertTrue(result["anddivisionsplay"].first().divisionName == "Gangplank's Backstabbers")
        self.assertTrue(result["orplayersname"].first().playerName == "Cop")
        self.assertTrue(result["orteamsplay"].first().teamName == "Curse")
        self.assertTrue(result["ordivisionsplay"].first().divisionName == "Gangplank's Backstabbers")


        
    def test_multi_terms_no_and_3 (self):
        result = multi_terms(["Curse","Solomid"])
        self.assertTrue(result.has_key("andplayersname"))
        self.assertTrue(result.has_key("orplayersname"))
        self.assertTrue(result.has_key("andplayersteam"))
        self.assertTrue(result.has_key("orplayersteam"))
        self.assertTrue(result.has_key("andplayersdivs"))
        self.assertTrue(result.has_key("orplayersdivs"))
        self.assertTrue(result.has_key("andplayersnamesi"))
        self.assertTrue(result.has_key("orplayersnamesi"))
        self.assertTrue(result.has_key("andplayersteamsi"))
        self.assertTrue(result.has_key("orplayersteamsi"))
        self.assertTrue(result.has_key("andplayersdivssi"))
        self.assertTrue(result.has_key("orplayersdivssi"))

        self.assertTrue(result.has_key("andteamsname"))
        self.assertTrue(result.has_key("orteamsname"))
        self.assertTrue(result.has_key("andteamsplay"))
        self.assertTrue(result.has_key("orteamsplay"))
        self.assertTrue(result.has_key("andteamsdivs"))
        self.assertTrue(result.has_key("orteamsdivs"))
        self.assertTrue(result.has_key("andteamsnamesi"))
        self.assertTrue(result.has_key("orteamsnamesi"))
        self.assertTrue(result.has_key("andteamsplaysi"))
        self.assertTrue(result.has_key("orteamsplaysi"))
        self.assertTrue(result.has_key("andteamsdivssi"))
        self.assertTrue(result.has_key("orteamsdivssi"))

        self.assertTrue(result.has_key("anddivisionsname"))
        self.assertTrue(result.has_key("ordivisionsname"))
        self.assertTrue(result.has_key("anddivisionsteam"))
        self.assertTrue(result.has_key("ordivisionsteam"))
        self.assertTrue(result.has_key("anddivisionsplay"))
        self.assertTrue(result.has_key("ordivisionsplay"))
        self.assertTrue(result.has_key("anddivisionsnamesi"))
        self.assertTrue(result.has_key("ordivisionsnamesi"))
        self.assertTrue(result.has_key("anddivisionsteamsi"))
        self.assertTrue(result.has_key("ordivisionsteamsi"))
        self.assertTrue(result.has_key("anddivisionsplaysi"))
        self.assertTrue(result.has_key("ordivisionsplaysi"))

        self.assertTrue(result.has_key("terms"))

        self.assertTrue(result["anddivisionsteam"].first().divisionName == "NA LCS")
        self.assertTrue(result["orteamsname"].first().teamName == "Curse")
        self.assertTrue(result["orplayersteam"].first().playerName == "Bjergsen")
        self.assertTrue(result["ordivisionsteam"].first().divisionName == "Maokai's Musketeers")

# ----
# main
# ----

print("tests.py")
#unittest.main()
print("Done.")
