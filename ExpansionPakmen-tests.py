#!/usr/bin/env python

from json import dumps, loads
from django.test import TestCase
from idb.models import *
from datetime import datetime
import requests
import time

GAME_ID = 0
STUDIO_ID = 1
CONSOLE_ID = 2


def verify_console(response_body):
	return response_body["name"] == "Xbox 360" and \
		response_body["id"] == 1 and \
		response_body["description"] == "The Xbox 360 is a video game console developed by Microsoft, and is the successor to the original Xbox, and it is the second console in the Xbox series. The Xbox 360 competes with Sony's PlayStation 3 and Nintendo's Wii as part of the seventh generation of video game consoles. The Xbox 360 was officially unveiled on MTV on May 12, 2005, with detailed launch and game information divulged later that month at the Electronic Entertainment Expo (E3)." and \
		response_body["release_date"] == "2005-11-22" and \
		3 in response_body["developers"] and \
		response_body["cpu"] == "3.2 GHz PowerPC Tri-Core Xenon" and \
		response_body["memory"] == "512 MB of GDDR3 RAM" and \
		response_body["storage"] == "External" and \
		response_body["graphics"] == "500 MHz ATI Xenos" and \
		1 in response_body["images"] and \
		1 in response_body["games"] and \
		1 in response_body["studios"] and \
		3 in response_body["external_links"] and \
		2 in response_body["videos"] and \
		4 in response_body["citations"]

def verify_studio(response_body):
	return response_body["name"] == "Rockstar North" and \
		response_body["id"] == 1 and \
		response_body["date_of_birth"] == "2002-01-01" and \
		response_body["location"] == "Edinburgh, Scotland, UK" and \
		response_body["description"] == "Rockstar North (formerly DMA Design Ltd) is a British video game developer based in Edinburgh, Scotland, best known for creating the Grand Theft Auto and Lemmings franchises in its earlier guise as DMA." and \
		response_body["map_embed"] == 41 and \
		response_body["handle_id"] == "RockstarGames" and \
		response_body["widget_id"] == "446501614346444800" and \
		1 in response_body["games"] and \
		1 in response_body["consoles"] and \
		42 in response_body["images"] and \
		44 in response_body["external_links"] and \
		43 in response_body["videos"] and \
		45 in response_body["citations"]

def verify_game(response_body):
	return response_body["name"] == "Grand Theft Auto V" and \
		response_body["id"] == 1 and \
		response_body["genre"] == 1 and \
		response_body["publish_date"] == "2013-09-17" and \
		response_body["description"] == "Grand Theft Auto V is an open world, action-adventure video game developed by Rockstar North and published by Rockstar Games. It is the fifteenth title in the Grand Theft Auto series and the first main entry since Grand Theft Auto IV in 2008. Set within the fictional state of San Andreas, based on Southern California, the game's single-player story follows three criminals and their efforts to execute a number of heists while under pressure from a government agency. The game's use of open world design allows the player to freely roam the state's countryside and the city of Los Santos, based on Los Angeles." and \
		response_body["handle_id"] == "RockstarGTAV" and \
		response_body["widget_id"] == "446477885360979968" and \
		1 in response_body["consoles"] and \
		1 in response_body["studios"] and \
		116 in response_body["images"] and \
		118 in response_body["external_links"] and \
		117 in response_body["videos"] and \
		119 in response_body["citations"]

class Test(TestCase):
	# Game endpoint tests
	def test_get_all_games(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(response_body["name"] == "Grand Theft Auto V")
		self.assertTrue(response_body["id"] == 1)

	def test_aaa_post_single_game(self):
		values = dumps({
		    "name": "test",
		    "genre": 1,
		    "publish_date": "2001-01-01",
		    "description": "test",
		    "images": [1],
		    "videos": [1],
		    "handle_id": "1", 
		    "widget_id": "1",
		    "consoles": [1],
		    "studios": [1],
		    "external_links": [1],
		    "citations": [1],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.post("http://expansion-pakmen.herokuapp.com/api/games", data=values, headers=headers)
		response_body = r.json()
		self.assertTrue(r.status_code == 201)
		self.assertTrue('id' in response_body)
		global GAME_ID
		GAME_ID = response_body['id']
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/" + str(GAME_ID))
		response_body = r.json()
		self.assertTrue(response_body['description'] == 'test')
		self.assertTrue(1 in response_body['citations'] and 1 in response_body['images'])
		self.assertTrue(response_body['genre'] == 1)
	
	def test_get_single_game(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/1")
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_game(r.json()))
		
	def test_get_studios_for_single_game(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/1/studios")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_studio(response_body))
		
	def test_get_consoles_for_single_game(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/1/consoles")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_console(response_body))	

	def test_get_media_for_single_game(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/1/media")
		response_body = r.json()
		self.assertTrue(r.status_code == 200)
		self.assertTrue("http://images.eurogamer.net/2013/usgamer/GTA-V-big.jpg" in response_body['images'])
		self.assertTrue("http://www.youtube.com/embed/N-xHcvug3WI" in response_body['videos'])
		self.assertTrue("http://www.rockstargames.com/V/" in response_body['external_links'])
		self.assertTrue("http://en.wikipedia.org/wiki/Grand_Theft_Auto_V" in response_body['citations'])

	def test_b_put_single_game(self):
		values = dumps({
		    "name": "test",
		    "genre": 1,
		    "publish_date": "2001-01-01",
		    "description": 'testing',
		    "images": [2],
		    "videos": [1],
		    "handle_id": "1", 
		    "widget_id": "1",
		    "consoles": [1],
		    "studios": [1],
		    "external_links": [1],
		    "citations": [2, 3],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.put("http://expansion-pakmen.herokuapp.com/api/games/" + str(GAME_ID), data=values, headers=headers)
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/games/" + str(GAME_ID))
		response_body = r.json()
		self.assertTrue(response_body['description'] == 'testing')
		self.assertTrue(2 in response_body['citations'] and 3 in response_body['citations'])

	def test_c_delete_single_game(self):
		r = requests.delete("http://expansion-pakmen.herokuapp.com/api/games/" + str(GAME_ID))
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')


	# # Studio endpoint tests
	def test_get_all_studios(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)		
		self.assertTrue(response_body["name"] == "Rockstar North")
		self.assertTrue(response_body["id"] == 1)

	def test_aaa_post_single_studio(self):
		values = dumps({
		    "name": "test",
		    "date_of_birth": "2001-01-01",
		    "location": "test",
		    "description": "test",
		    "images": [1],
		    "videos": [1],
		    "map_embed": 1,
		    "handle_id": "1",
		    "widget_id": "1",
		    "games": [1],
		    "external_links": [1],
		    "citations": [1],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.post("http://expansion-pakmen.herokuapp.com/api/studios", data=values, headers=headers)
		response_body = loads(r.text)
		self.assertTrue(r.status_code == 201)
		self.assertTrue('id' in response_body)
		global STUDIO_ID
		STUDIO_ID = response_body['id']
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/" + str(STUDIO_ID))
		response_body = r.json()
		self.assertTrue(response_body['description'] == 'test')
		self.assertTrue(1 in response_body['citations'] and 1 in response_body['images'])
		self.assertTrue(response_body['map_embed'] == 1)
	
	def test_get_single_studio(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/1")
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_studio(r.json()))

	def test_get_games_for_single_studio(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/1/games")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_game(response_body))

	def test_get_consoles_for_single_studio(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/1/consoles")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_console(response_body))

	def test_get_media_for_single_studio(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/1/media")
		response_body = r.json()
		self.assertTrue(r.status_code == 200)
		self.assertTrue("http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png" in response_body['images'])
		self.assertTrue("http://www.youtube.com/embed/zMW0WRO0vEk" in response_body['videos'])
		self.assertTrue("http://www.rockstarnorth.com/" in response_body['external_links'])
		self.assertTrue("http://en.wikipedia.org/wiki/Rockstar_North" in response_body['citations'])
	
	def test_b_put_single_studio(self):
		values = dumps({
	        "name": "test",
		    "date_of_birth": "2001-01-01",
		    "location": "test",
		    "description": "testing",
		    "images": [1],
		    "videos": [1],
		    "map_embed": 1,
		    "handle_id": "0",
		    "widget_id": "1",
		    "games": [2],
		    "consoles": [1],
		   	"external_links": [1],
		    "citations": [1],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.put("http://expansion-pakmen.herokuapp.com/api/studios/" + str(STUDIO_ID), data=values, headers=headers)
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/studios/" + str(STUDIO_ID))
		response_body = r.json()
		self.assertTrue(response_body['description'] == 'testing')
		self.assertTrue(2 in response_body['games'] and 1 in response_body['images'])
		self.assertTrue(response_body['handle_id'] == '0')
	
	def test_c_delete_single_studio(self):
		r = requests.delete("http://expansion-pakmen.herokuapp.com/api/studios/" + str(STUDIO_ID))
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')


	# # Console endpoint tests
	def test_get_all_consoles(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(response_body["name"] == "Xbox 360")
		self.assertTrue(response_body["id"] == 1)
	
	def test_aaa_post_single_console(self):
		values = dumps({
		    "name": "test",
		    "description": "test",
		    "release_date": "2001-01-01",
		    "developers": [1],
		    "cpu": 	"test",
		    "memory": "test",
		    "storage": "test",
		    "graphics": "test",
		    "images": [1],
		    "videos": [1],
		    "games": [1],
		    "external_links": [1],
		    "citations": [1, 2],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.post("http://expansion-pakmen.herokuapp.com/api/consoles", data=values, headers=headers)
		response_body = r.json()
		self.assertTrue(r.status_code == 201)
		self.assertTrue('id' in response_body)
		global CONSOLE_ID
		CONSOLE_ID = response_body['id']
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/" + str(CONSOLE_ID))
		response_body = r.json()
		self.assertTrue(response_body['graphics'] == 'test')
		self.assertTrue(2 in response_body['citations'] and 1 in response_body['citations'])

	def test_get_single_console(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/1")
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_console(r.json()))

	def test_get_games_for_single_console(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/1/games")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_game(response_body))

	def test_get_studios_for_single_console(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/1/studios")
		response_body = r.json()[0]
		self.assertTrue(r.status_code == 200)
		self.assertTrue(verify_studio(response_body))

	def test_get_media_for_single_console(self):
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/1/media")
		response_body = r.json()
		self.assertTrue(r.status_code == 200)
		self.assertTrue("http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg" in response_body['images'])
		self.assertTrue("http://www.youtube.com/embed/tAfANb2Cs9Q" in response_body['videos'])
		self.assertTrue("http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_" in response_body['external_links'])
		self.assertTrue("http://en.wikipedia.org/wiki/Xbox_360" in response_body['citations'])
	
	def test_b_put_single_console(self):
		values = dumps({
		    'name': "test",
		    "description": "test",
		    "release_date": "2001-01-01",
		    "developers": [1],
		    "cpu": 	"test",
		    "memory": "test",
		    "storage": "test",
		    "graphics": "testing123",
		    "images": [1],
		    "videos": [1],
		    "games": [1],
		    "external_links": [1],
		    "citations": [2, 3],
		})
		headers = {"Content-Type": "application/json"}
		r = requests.put("http://expansion-pakmen.herokuapp.com/api/consoles/" + str(CONSOLE_ID), data=values, headers=headers)
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')
		r = requests.get("http://expansion-pakmen.herokuapp.com/api/consoles/" + str(CONSOLE_ID))
		response_body = r.json()
		self.assertTrue(response_body['graphics'] == 'testing123')
		self.assertTrue(2 in response_body['citations'] and 3 in response_body['citations'])

	def test_c_delete_single_console(self):
		r = requests.delete("http://expansion-pakmen.herokuapp.com/api/consoles/" + str(CONSOLE_ID))
		response_body = r.text
		self.assertTrue(r.status_code == 204)
		self.assertTrue(response_body == '')

	#MODELS TESTS

	def test_serialize_model_strings1(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		WinNguyenDict = serialize_model_strings(WinNguyen)
		tmp = {'studios': [], 'videos': ['http://www.youtube.com/embed/mushldyN_rw'], 'consoles': [], 'citations': ['http://en.wikipedia.org/wiki/Thief_(video_game)'], 'external_links': ['http://www.thiefgame.com/agegate?locale=us'], 'images': ['http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg']}
		self.assertTrue(str(WinNguyenDict) == str(tmp))
	
	def test_serialize_model_strings2(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		gp360Dict = serialize_model_strings(gp360)
		tmp = {'studios': [], 'videos': ['http://www.youtube.com/embed/tAfANb2Cs9Q'], 'citations': ['http://en.wikipedia.org/wiki/Xbox_360'], 'games': [], 'external_links': ['http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_'], 'images': ['http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg']}
		self.assertTrue(str(gp360Dict) == str(tmp))


	def test_serialize_model_strings3(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		VerStarrNorthDict = serialize_model_strings(VerStarrNorth)
		tmp = {'videos': ['http://www.youtube.com/embed/zMW0WRO0vEk'], 'consoles': [], 'citations': ['http://en.wikipedia.org/wiki/Rockstar_North'], 'games': [], 'external_links': ['http://www.VerStarrNorth.com/'], 'images': ['http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png']}
		self.assertTrue(str(VerStarrNorthDict) == str(tmp))

	def test_serialize_model_ids1(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		WinNguyenDict = serialize_model_ids(WinNguyen)
		tmp = {'studios': [], 'videos': [2], 'consoles': [], 'citations': [4], 'external_links': [3], 'images': [1]}
		self.assertTrue(str(WinNguyenDict) == str(tmp))

	def test_serialize_model_ids2(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		gp360Dict = serialize_model_ids(gp360)
		tmp = {'studios': [], 'videos': [2], 'citations': [4], 'games': [], 'external_links': [3], 'images': [1]}
		self.assertTrue(str(gp360Dict) == str(tmp))

	def test_serialize_model_ids3(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		VerStarrNorthDict = serialize_model_ids(VerStarrNorth)
		tmp = {'videos': [3], 'consoles': [], 'citations': [5], 'games': [], 'external_links': [4], 'images': [2]}
		self.assertTrue(str(VerStarrNorthDict) == str(tmp))


	# Search Tests
	def test_normalize_query_1(self):
		words = normalize_query('')
		self.assertTrue(len(words) == 0)

	def test_normalize_query_2(self):
		words = normalize_query('xbox')
		self.assertTrue(len(words) == 1)
		self.assertTrue(words[0] == 'xbox')

	def test_normalize_query_3(self):
		words = normalize_query('call+of+duty')
		self.assertTrue(len(words) == 3)
		self.assertTrue(words[0] == 'call')
		self.assertTrue(words[1] == 'of')
		self.assertTrue(words[2] == 'duty')

	def test_normalize_query_4(self):
		words = normalize_query('Call+of+Duty')
		self.assertTrue(len(words) == 3)
		self.assertTrue(words[0] == 'call')
		self.assertTrue(words[1] == 'of')
		self.assertTrue(words[2] == 'duty')

	def test_normalize_query_5(self):
		words = normalize_query('CaLL+OF+DUTy')
		self.assertTrue(len(words) == 3)
		self.assertTrue(words[0] == 'call')
		self.assertTrue(words[1] == 'of')
		self.assertTrue(words[2] == 'duty')


	def test_generate_result_string_1(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		model_string = model_to_string(WinNguyen, Game)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'publish_date':[], 'genre':[], 'consoles':[], 'studios':[], 'handle_id':[], 'description':[]}, WinNguyen, Game)
		self.assertTrue(len(result_string) == 0)

	def test_generate_result_string_2(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		model_string = model_to_string(WinNguyen, Game)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'publish_date':[], 'genre':[], 'consoles':[], 'studios':[], 'handle_id':[], 'description':[('eidos', 60)]}, WinNguyen, Game)
		self.assertTrue(len(result_string) == 459)
		self.assertTrue(result_string == "  Description: WinNguyen is a stealth video game developed by <b>Eidos</b> Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.")
		indices = [m.start() for m in re.finditer('<b>',result_string)]
		self.assertTrue(len(indices) == 1)
		self.assertTrue(indices[0] == 62)


	def test_generate_result_string_3(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		model_string = model_to_string(VerStarrNorth, Studio)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'date_of_birth':[], 'location':[], 'games':[], 'consoles':[], 'description':[]}, VerStarrNorth, Studio)
		self.assertTrue(len(result_string) == 0)

	def test_generate_result_string_4(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		model_string = model_to_string(VerStarrNorth, Studio)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[('north', 16)], 'date_of_birth':[], 'location':[], 'games':[], 'consoles':[], 'description':[('north', 38)]}, VerStarrNorth, Studio)
		self.assertTrue(len(result_string) == 82)
		indices = [m.start() for m in re.finditer('<b>',result_string)]
		self.assertTrue(len(indices) == 2)
		self.assertTrue(indices[0] == 18)
		self.assertTrue(indices[1] == 70)

	def test_generate_result_string_5(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		model_string = model_to_string(gp360, Console)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'release_date':[], 'developers':[], 'cpu':[], 'memory':[], 'storage':[], 'graphics':[], 'games':[], 'studios':[], 'description':[]}, gp360, Console)
		self.assertTrue(len(result_string) == 0)

	def test_generate_result_string_6(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		model_string = model_to_string(gp360, Console)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'release_date':[], 'developers':[('downing', 12)], 'cpu':[], 'memory':[], 'storage':[], 'graphics':[], 'games':[], 'studios':[], 'description':[]}, gp360, Console)
		self.assertTrue(len(result_string) == 36)
		indices = [m.start() for m in re.finditer('<b>',result_string)]
		self.assertTrue(len(indices) == 1)
		self.assertTrue(indices[0] == 14)

	def test_search_objects1(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		objects = search_objects('WinNguyen', Game) 
		self.assertTrue(len(objects) == 2)
		self.assertTrue(type(objects) is tuple)
		self.assertTrue(type(objects[0]) is list)
		self.assertTrue(str(objects[0][0][0]) == 'Game object')
		self.assertTrue(str(objects[0][0][1]) == '  Name: <b>WinNguyen</b>  Twitter Handle: <b>WinNguyen</b>  Description: <b>WinNguyen</b> is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic <b>WinNguyen</b> series of stealth games, of which it is the fourth game. Players control Garrett, a master <b>WinNguyen</b>, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.')
	
	def test_search_objects2(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		objects = search_objects('gp+360', Console) 
		self.assertTrue(len(objects) == 2)
		self.assertTrue(type(objects) is tuple)
		self.assertTrue(type(objects[0]) is list)
		self.assertTrue(str(objects[0][0][0]) == 'Console object')
		self.assertTrue(str(objects[0][0][1]) == '  Name: <b>GP</b> <b>360</b>  Description: Description of <b>GP</b> <b>360</b>')

	def test_search_objects3(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		objects = search_objects('ver+starr+north', Studio) 
		self.assertTrue(len(objects) == 2)
		self.assertTrue(type(objects) is tuple)
		self.assertTrue(type(objects[0]) is list)
		self.assertTrue(str(objects[0][0][0]) == 'Studio object')
		self.assertTrue(str(objects[0][0][1]) == '  Name: <b>Ver</b> <b>Starr</b> <b>North</b>  Description: Description of <b>Ver</b> <b>Starr</b> <b>North</b>')


	def test_model_to_string_1(self):
		WinNguyengenre = Genre(name = 'hehe')
		WinNguyengenre.save()
		WinNguyen = Game()
		WinNguyen.name = 'WinNguyen'
		WinNguyen.publish_date = datetime.strptime("2/25/2014", "%m/%d/%Y")
		WinNguyen.genre = WinNguyengenre
		WinNguyen.save()
		WinNguyen.description = 'WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.'
		WinNguyen.handle_id = "WinNguyen"
		WinNguyen.widget_id = "450386712363950080"
		WinNguyenImage = URL(name = 'WinNguyen Image', url = 'http://upload.wikimedia.org/wikipedia/ru/0/02/Thief_box_art.jpg')
		WinNguyenImage.save()
		WinNguyen.images.add(WinNguyenImage)
		WinNguyenVideo = URL(name = 'WinNguyen Video', url = 'http://www.youtube.com/embed/mushldyN_rw')
		WinNguyenVideo.save()
		WinNguyen.videos.add(WinNguyenVideo)
		WinNguyenExternal_Link = URL(name = 'WinNguyen External Link', url = 'http://www.thiefgame.com/agegate?locale=us')
		WinNguyenExternal_Link.save()
		WinNguyen.external_links.add(WinNguyenExternal_Link)
		WinNguyenCitation = URL(name = 'WinNguyen Citation', url = 'http://en.wikipedia.org/wiki/Thief_(video_game)')
		WinNguyenCitation.save()
		WinNguyen.citations.add(WinNguyenCitation)
		WinNguyen.save()
		model_string = model_to_string(WinNguyen, Game)
		self.assertTrue(len(model_string) == 8)
		self.assertTrue('studios' in model_string)
		self.assertTrue(model_string['studios'] == 'Studios: ')
		self.assertTrue('description' in model_string)
		self.assertTrue(model_string['description'] == 'Description: WinNguyen is a stealth video game developed by Eidos Montreal and published by Square Enix. It is a revival of the cult classic WinNguyen series of stealth games, of which it is the fourth game. Players control Garrett, a master WinNguyen, as he intends to steal from the rich. Similar to previous games in the series, players must use stealth in order to overcome challenges, while violence is left as a minimally effective last resort.')
		self.assertTrue('handle_id' in model_string)
		self.assertTrue(model_string['handle_id'] == 'Twitter Handle: WinNguyen')
		self.assertTrue('consoles' in model_string)
		self.assertTrue(model_string['consoles'] == 'Consoles: ')
		self.assertTrue('publish_date' in model_string)
		self.assertTrue(model_string['publish_date'] == 'Publish date: 2014-02-25 00:00:00')
		self.assertTrue('genre' in model_string)
		self.assertTrue(model_string['genre'] == 'Genre: hehe')
		self.assertTrue('id' in model_string)
		self.assertTrue('name' in model_string)
		self.assertTrue(model_string['name'] == 'Name: WinNguyen')


	def test_model_to_string_2(self):
		VerStarrNorth = Studio()
		VerStarrNorth.name = "Ver Starr North"
		VerStarrNorth.date_of_birth = datetime.strptime("2002", "%Y")
		VerStarrNorth.location = "Edinburgh, Scotland, UK"
		VerStarrNorth.description = 'Description of Ver Starr North'
		VerStarrNorthMap = URL(name = 'VerStarrNorth Map', url = 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCVwKH5xYUvm_JobZLSpgYaIFw3nwOlW9k&q=Rockstar+North+Ltd,+Edinburg+United+Kingdom')
		VerStarrNorthMap.save()
		VerStarrNorth.map_embed = VerStarrNorthMap
		VerStarrNorth.save()
		VerStarrNorth.handle_id = "VerStarrGames"
		VerStarrNorth.widget_id = "446501614346444800"
		VerStarrNorthImage = URL(name = 'VerStarrNorth Image', url = 'http://4vector.com/i/free-vector-rockstar-north_031579_rockstar-north.png')
		VerStarrNorthImage.save()
		VerStarrNorth.images.add(VerStarrNorthImage)
		VerStarrNorthVideo = URL(name = 'VerStarrNorth Video', url = 'http://www.youtube.com/embed/zMW0WRO0vEk')
		VerStarrNorthVideo.save()
		VerStarrNorth.videos.add(VerStarrNorthVideo)
		VerStarrNorthExternal_Link = URL(name = 'VerStarrNorth External Link', url = 'http://www.VerStarrNorth.com/')
		VerStarrNorthExternal_Link.save()
		VerStarrNorth.external_links.add(VerStarrNorthExternal_Link)
		VerStarrNorthCitation = URL(name = 'VerStarrNorth Citation', url = 'http://en.wikipedia.org/wiki/Rockstar_North')
		VerStarrNorthCitation.save()
		VerStarrNorth.citations.add(VerStarrNorthCitation)
		VerStarrNorth.save()
		model_string = model_to_string(VerStarrNorth, Game)
		self.assertTrue(len(model_string) == 7)
		self.assertTrue('description' in model_string)
		self.assertTrue(model_string['description'] == 'Description: Description of Ver Starr North')
		self.assertTrue('consoles' in model_string)
		self.assertTrue(model_string['consoles'] == 'Consoles: ')
		self.assertTrue('location' in model_string)
		self.assertTrue(model_string['location'] == 'Location: Edinburgh, Scotland, UK')
		self.assertTrue('date_of_birth' in model_string)
		self.assertTrue(model_string['date_of_birth'] == 'Founded: 2002-01-01 00:00:00')
		self.assertTrue('id' in model_string)
		self.assertTrue('name' in model_string)
		self.assertTrue(model_string['name'] == 'Name: Ver Starr North')

		model_string = model_to_string(VerStarrNorth, Studio)
		result_string = generate_result_string(model_string, \
			{'id':[], 'name':[], 'date_of_birth':[], 'location':[], 'games':[], 'consoles':[], 'description':[]}, VerStarrNorth, Studio)
		self.assertTrue(len(result_string) == 0)


	def test_model_to_string_3(self):
		gpdev = Developer(name = "downing studios")	
		gpdev.save()	
		gp360 = Console()
		gp360.name = "GP 360"
		gp360.release_date = datetime.strptime("11/22/2005", "%m/%d/%Y")
		gp360.save()
		gp360.developers.add(gpdev)
		gp360.description = "Description of GP 360"		
		gp360.cpu = '3.2 GHz PowerPC Tri-Core Xenon'
		gp360.memory = '512 MB of GDDR3 RAM'
		gp360.storage = 'External'
		gp360.graphics = '500 MHz ATI Xenos'
		gp360Image = URL(name = 'gp360 Image', url = 'http://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Xbox-360-arcade.jpg/411px-Xbox-360-arcade.jpg')
		gp360Image.save()
		gp360.images.add(gp360Image)
		gp360Video = URL(name = 'gp360 Video', url = 'http://www.youtube.com/embed/tAfANb2Cs9Q')
		gp360Video.save()
		gp360.videos.add(gp360Video)
		gp360External_Link = URL(name = 'gp360 External Link', url = 'http://www.xbox.com/en-US/xbox-360/why-xbox-360#fbid=TKmUjUvEj6_')
		gp360External_Link.save()
		gp360.external_links.add(gp360External_Link)
		gp360Citation = URL(name = 'gp360 Citation', url = 'http://en.wikipedia.org/wiki/Xbox_360')
		gp360Citation.save()
		gp360.citations.add(gp360Citation)
		gp360.save()
		model_string = model_to_string(gp360, Console)
		self.assertTrue(len(model_string) == 11)
		self.assertTrue('studios' in model_string)
		self.assertTrue(model_string['studios'] == 'Studios: ')
		self.assertTrue('description' in model_string)
		self.assertTrue(model_string['description'] == 'Description: Description of GP 360')
		self.assertTrue('release_date' in model_string)
		self.assertTrue(model_string['release_date'] == 'Release date: 2005-11-22 00:00:00')
		self.assertTrue('graphics' in model_string)
		self.assertTrue(model_string['graphics'] == 'Graphics: 500 MHz ATI Xenos')
		self.assertTrue('storage' in model_string)
		self.assertTrue(model_string['storage'] == 'Storage: External')
		self.assertTrue('games' in model_string)
		self.assertTrue(model_string['games'] == 'Games: ')
		self.assertTrue('memory' in model_string)
		self.assertTrue(model_string['memory'] == 'Memory: 512 MB of GDDR3 RAM')
		self.assertTrue('cpu' in model_string)
		self.assertTrue(model_string['cpu'] == 'CPU: 3.2 GHz PowerPC Tri-Core Xenon')
		self.assertTrue('developers' in model_string)
		self.assertTrue(model_string['developers'] == 'Developers: downing studios')
		self.assertTrue('id' in model_string)
		self.assertTrue('name' in model_string)
		self.assertTrue(model_string['name'] == 'Name: GP 360')

	def test_find_all_occurrences1(self):
		nameDict = {'name' : 'Name: Call of Duty'} 
		nameTerm = ['call', 'of', 'duty']
		found = find_all_occurrences(nameDict, nameTerm)
		self.assertTrue(found == ({'name': [('call', 6), ('of', 11), ('duty', 14)]}, True, True))
		self.assertTrue(found[0] == {'name': [('call', 6), ('of', 11), ('duty', 14)]})
		self.assertTrue(found[1] == True)
		self.assertTrue(found[2] == True)

	def test_find_all_occurrences2(self):
		nameDict = {}
		nameTerm = []
		found = find_all_occurrences(nameDict, nameTerm)
		self.assertTrue(found == ({}, True, False))
		self.assertTrue(found[0] == {})
		self.assertTrue(found[1] == True)
		self.assertTrue(found[2] == False)

	def test_find_all_occurrences3(self):
		nameDict = {'name' : 'Name: Call of Duty', 'description' : 'Description: Call of Duty: Ghosts is a 2013 first-person shooter video game developed by Infinity Ward. It is the tenth primary installment in the Call of Duty series, and the sixth developed by Infinity Ward.'}
		nameTerm = ['call', 'of', 'duty']
		found = find_all_occurrences(nameDict, nameTerm)
		self.assertTrue(found == ({'name': [('call', 6), ('of', 11), ('duty', 14)], 'description': [('call', 13), ('call', 147), ('of', 18), ('of', 152), ('duty', 21), ('duty', 155)]}, True, True))		
		self.assertTrue(found[0] == {'name': [('call', 6), ('of', 11), ('duty', 14)], 'description': [('call', 13), ('call', 147), ('of', 18), ('of', 152), ('duty', 21), ('duty', 155)]})
		self.assertTrue(found[1] == True)
		self.assertTrue(found[2] == True)

	def test_bold_found_words1(self):
		result_string = bold_found_words('Description: Description of Ver Starr North', [('ver', 28), ('starr', 32), ('north', 38)])
		self.assertTrue(result_string == 'Description: Description of <b>Ver</b> <b>Starr</b> <b>North</b>')
		self.assertTrue(type(result_string) is str)
		self.assertTrue(len(result_string) == 64)

	def test_bold_found_words2(self):
		result_string = bold_found_words('', [('', 0)])
		self.assertTrue(result_string == '<b></b>')
		self.assertTrue(type(result_string) is str)
		self.assertTrue(len(result_string) == 7)	

	### PERFORMANCE TESTING ###
	def test_search1(self):
		print()
		print('Performance Testing')
		start = time.time()
		found_entries = None
		query_string, terms = "", []
		game_found_entries_OR, game_found_entries_AND = [], []
		console_found_entries_OR, console_found_entries_AND = [], []
		studio_found_entries_OR, studio_found_entries_AND = [], []
		searchStr = ''

		if (len(searchStr) > 0):
			userQueryString = ''
			query_string = ""
			for term in userQueryString.split():
				if term not in STOP_WORDS:
					query_string += term + " "
			terms = normalize_query(query_string)
			game_found_entries_AND, game_found_entries_OR = search_objects(query_string, Game)
			studio_found_entries_AND, studio_found_entries_OR = search_objects(query_string, Studio)
			console_found_entries_AND, console_found_entries_OR = search_objects(query_string, Console)
		stop = time.time()
		performanceTime = 1000 * (stop - start)
		print()
		print('Performance test1 (NULL search) took: ' + str("%.3f" % performanceTime) + ' ms')

	def test_search2(self):
		start = time.time()
		found_entries = None
		query_string, terms = "", []
		game_found_entries_OR, game_found_entries_AND = [], []
		console_found_entries_OR, console_found_entries_AND = [], []
		studio_found_entries_OR, studio_found_entries_AND = [], []
		searchStr = 'call of duty'

		if (len(searchStr) > 0):
			userQueryString = ''
			query_string = ""
			for term in userQueryString.split():
				if term not in STOP_WORDS:
					query_string += term + " "
			terms = normalize_query(query_string)
			game_found_entries_AND, game_found_entries_OR = search_objects(query_string, Game)
			studio_found_entries_AND, studio_found_entries_OR = search_objects(query_string, Studio)
			console_found_entries_AND, console_found_entries_OR = search_objects(query_string, Console)
		stop = time.time()
		performanceTime = 1000 * (stop - start)
		print()
		print('Performance test2 ("Call of Duty" search) took: ' + str("%.3f" % performanceTime) + ' ms')

	def test_search3(self):
		start = time.time()
		found_entries = None
		query_string, terms = "", []
		game_found_entries_OR, game_found_entries_AND = [], []
		console_found_entries_OR, console_found_entries_AND = [], []
		studio_found_entries_OR, studio_found_entries_AND = [], []
		searchStr = 'Xbox One'

		if (len(searchStr) > 0):
			userQueryString = ''
			query_string = ""
			for term in userQueryString.split():
				if term not in STOP_WORDS:
					query_string += term + " "
			terms = normalize_query(query_string)
			game_found_entries_AND, game_found_entries_OR = search_objects(query_string, Game)
			studio_found_entries_AND, studio_found_entries_OR = search_objects(query_string, Studio)
			console_found_entries_AND, console_found_entries_OR = search_objects(query_string, Console)
		stop = time.time()
		performanceTime = 1000 * (stop - start)
		print()
		print('Performance test3 ("Xbox One" search) took: ' + str("%.3f" % performanceTime) + ' ms')

	def test_search4(self):
		start = time.time()
		found_entries = None
		query_string, terms = "", []
		game_found_entries_OR, game_found_entries_AND = [], []
		console_found_entries_OR, console_found_entries_AND = [], []
		studio_found_entries_OR, studio_found_entries_AND = [], []
		searchStr = 'Bungie'

		if (len(searchStr) > 0):
			userQueryString = ''
			query_string = ""
			for term in userQueryString.split():
				if term not in STOP_WORDS:
					query_string += term + " "
			terms = normalize_query(query_string)
			game_found_entries_AND, game_found_entries_OR = search_objects(query_string, Game)
			studio_found_entries_AND, studio_found_entries_OR = search_objects(query_string, Studio)
			console_found_entries_AND, console_found_entries_OR = search_objects(query_string, Console)
		stop = time.time()
		performanceTime = 1000 * (stop - start)
		print()
		print('Performance test4 ("Bungie" search) took: ' + str("%.3f" % performanceTime) + ' ms') 