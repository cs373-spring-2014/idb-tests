#!/usr/bin/env python3
from django.test import TestCase
from django.utils import unittest
from idb.models import *

from urllib.request import Request, urlopen
from json import dumps, loads
#import unittest

class TestArtistAPI(TestCase):
    #-----------------#
    # ARTIST DATA
    #-----------------#
    
    # 1
    # Displays the information for a specific artist where id is the database primary key
    #
    def test_get_single_artist1(self):
        response = urlopen("http://127.0.0.1:8000/api/artists/Lady_Gaga/")

        self.assertEqual(response.getcode(), 200)

        our_response = response.readall().decode("utf-8")

        our_response = loads(our_response)

        expected_response = {"description": "Stefani Joanne Angelina Germanotta (born March 28, 1986), better known by her stage name Lady Gaga, is an American recording artist, activist, record producer, businesswoman, fashion designer, philanthropist, and actress. She was born, raised, and lives in New York City. Gaga primarily studied at the Convent of the Sacred Heart and briefly attended New York University's Tisch School of the Arts before withdrawing to focus on her musical career. She soon began performing in the rock music scene of Manhattan's Lower East Side. By the end of 2007, record executive and producer Vincent Herbert signed her to his label Streamline Records, an imprint of Interscope Records. Initially working as a songwriter at Interscope Records, her vocal abilities captured the attention of recording artist Akon, who also signed her to Kon Live Distribution, his own label under Interscope.", "imagel": "/static/images/lady_gaga_main.jpg", "band_name": "Lady Gaga", "hometown": "New York City, U.S.", "twitter": "451169598935556097", "labels": ["Cherrytree Records", "Def Jam Recordings", "Interscope Records", "KonLive", "Streamline"], "citations": "http://en.wikipedia.org/wiki/Lady_gaga", "slideshow_images": ["/static/images/lady_gaga_slideshow_0.jpg", "/static/images/lady_gaga_slideshow_1.jpg", "/static/images/lady_gaga_slideshow_2.jpg", "/static/images/lady_gaga_slideshow_3.jpg"], "years_active": "2005 - present", "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fladygaga&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", "members": ["Stefani Germanotta"], "google_map": "https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=New+York+City,+U.S.&aq=&sll=42.331427,-83.045754&sspn=0.258374,0.506058&ie=UTF8&hq=&hnear=New+York,+New+York+County,+New+York&t=m&z=11&ll=40.714353,-74.005973&output=embed", "albums": ["Artpop", "Born This Way", "The Fame"], "album_tiles": ["/static/images/lady_gaga_tile_0.jpg", "/static/images/lady_gaga_tile_1.jpg", "/static/images/lady_gaga_tile_2.jpg", "/static/images/lady_gaga_tile_3.jpg", "/static/images/lady_gaga_tile_4.jpg"], "id": 5, "video_slideshow": ["http://www.youtube.com/embed/EVBsypHzF3U", "http://www.youtube.com/embed/wV1FrqwZyKw"]}

        self.assertTrue(expected_response == our_response)
    # 2
    # Lists the artists in the database
    #
    def test_get_artists(self):
        response = urlopen("http://127.0.0.1:8000/api/artists/")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"band_name": "The Beatles", "hometown": "Liverpool, England", "years_active": "1960-1970"}, {"band_name": "Coldplay", "hometown": "London, England", "years_active": "1996 - present"}, {"band_name": "Dr Dre", "hometown": "Compton, California, U.S.", "years_active": "1984 - 2014"}, {"band_name": "Eminem", "hometown": "Detroit, U.S.", "years_active": "1990 - present"}, {"band_name": "Lady Gaga", "hometown": "New York City, U.S.", "years_active": "2005 - present"}, {"band_name": "Madonna", "hometown": "New York City, U.S.", "years_active": "1979- present"}, {"band_name": "Pink Floyd", "hometown": "London, England", "years_active": "1965 - 1995"}, {"band_name": "Rihanna", "hometown": "Saint Michael, Barbados", "years_active": "2004 - present"}, {"band_name": "2Pac", "hometown": "New York City, U.S.", "years_active": "1988 - 1996"}, {"band_name": "will i am", "hometown": "Los Angeles, U.S.", "years_active": "1997 - present"}]
        expected_response = sorted(expected_response, key=lambda x: x['band_name'])
        our_response = sorted(our_response, key=lambda x: x['band_name'])
        self.assertTrue(expected_response == our_response)

    # 3
    # Create a new entry for artist.
    #

    def test_2_post_single_artist(self):
        values = dumps({
            "members":["John Lennon", "Paul McCartney", "George Harrison", "Ringo Starr"],
            "band_name": "The Beatles",
            "hometown": "Liverpool, England",
            "years_active": "1960-1970",
            "albums": ["Twist and Shout", "Please Please Me", "With the Beatles","Beatlemania! With the Beatles", "Introducing...The Beatles","Meet the Beatles!", "The Beatles' Second Album","The Beatles' Long Tall Sally","A Hard Day's Night","Something New","Beatles for Sale","Beatles '65'","Beatles VI", "Help!","Rubber Soul","Yesterday and Today","Revolver","Sgt.Pepper's Lonely Hearts Club Band","Magical Mystery Tour","The Beatles","Yellow Submarine","Abbey Road","Let It Be"],
            "labels": ["Capitol Records","Parlophone Records", "Swan","Vee-Jay","United Artists", "Apple"],
            "images": "http://2.bp.blogspot.com/_xzXp2k6_nh0/TSCZBEyuudI/AAAAAAAABA4/tnOe-vsHx1w/s1600/TheBeatlesLogo2+-+copia.jpg",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fthebeatles&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", 
            "twitter": "451168834007732224",
            "google_map":"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Liverpool,+England&amp;aq=&amp;sll=30.307761,-97.753401&amp;sspn=1.206926,2.024231&amp;ie=UTF8&amp;hq=&amp;hnear=Liverpool,+Merseyside,+United+Kingdom&amp;t=m&amp;z=12&amp;ll=53.408371,-2.991573&amp;output=embed",
            "description": "The Beatles were an English rock band that formed in Liverpool, in 1960. With John Lennon, Paul McCartney, George Harrison, and Ringo Starr, they became widely regarded as the greatest and most influential act of the rock era.",
            "slideshow_images": ["http://ic.pics.livejournal.com/jonesingjay/12452594/289066/original.jpg",
                                 "http://beatlesfacts.files.wordpress.com/2011/09/hellogoodbye.jpg",
                                 "http://i0.wp.com/djournal.com/wp-content/blogs.dir/42/files/2013/10/Rain-Beatles1.jpg",
                                 "http://images6.fanpop.com/image/photos/33900000/The-Beatles-the-beatles-33951025-1100-824.jpg"],
            "album_tiles": ["http://www.beatlesbible.com/wp/media/canada_twist_and_shout.jpg",
                            "http://upload.wikimedia.org/wikipedia/en/a/a4/PleasePleaseMe.jpg",
                            "http://www.beatlesbible.com/wp/media/with_the_beatles.jpg",
                            "http://www.eskimo.com/~bpentium/beatles/intro/vjintrlp.jpg",
                            "http://0.tqn.com/d/classicrock/1/0/X/J/meetthebeatles.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/r5nARZKS-AY",
                               "http://www.youtube.com/embed/eDdI7GhZSQA"],
            "citation":"http://en.wikipedia.org/wiki/The_Beatles"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/artists/", data=values, headers=headers)
        #request.get_method = lambda: "POST"

        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        our_response = response.readall().decode("utf-8")
        actual_response = '{"band_name": "The Beatles"}'
        self.assertEqual(our_response, actual_response)

    # 4
    # Update an existing artist
    #
    def test_put_artist(self):
        values = dumps({
            "members":["John Lennon", "Paul McCartney", "George Harrison", "Ringo Starr"],
            "band_name": "The Beatles",
            "hometown": "Liverpool, England",
            "years_active": "1960-1970",
            "albums": ["Twist and Shout", "Please Please Me", "With the Beatles","Beatlemania! With the Beatles", "Introducing...The Beatles","Meet the Beatles!", "The Beatles' Second Album","The Beatles' Long Tall Sally","A Hard Day's Night","Something New","Beatles for Sale","Beatles '65'","Beatles VI", "Help!","Rubber Soul","Yesterday and Today","Revolver","Sgt.Pepper's Lonely Hearts Club Band","Magical Mystery Tour","The Beatles","Yellow Submarine","Abbey Road","Let It Be"],
            "labels": ["Capitol Records","Parlophone Records", "Swan","Vee-Jay","United Artists", "Apple"],
            "images": "http://2.bp.blogspot.com/_xzXp2k6_nh0/TSCZBEyuudI/AAAAAAAABA4/tnOe-vsHx1w/s1600/TheBeatlesLogo2+-+copia.jpg",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fthebeatles&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", 
            "twitter": "451168834007732224",
            "google_map":"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Liverpool,+England&amp;aq=&amp;sll=30.307761,-97.753401&amp;sspn=1.206926,2.024231&amp;ie=UTF8&amp;hq=&amp;hnear=Liverpool,+Merseyside,+United+Kingdom&amp;t=m&amp;z=12&amp;ll=53.408371,-2.991573&amp;output=embed",
            "description": "The Beatles were an English rock band that formed in Liverpool, in 1960. With John Lennon, Paul McCartney, George Harrison, and Ringo Starr, they became widely regarded as the greatest and most influential act of the rock era.",
            "slideshow_images": ["http://ic.pics.livejournal.com/jonesingjay/12452594/289066/original.jpg",
                                 "http://beatlesfacts.files.wordpress.com/2011/09/hellogoodbye.jpg",
                                 "http://i0.wp.com/djournal.com/wp-content/blogs.dir/42/files/2013/10/Rain-Beatles1.jpg",
                                 "http://images6.fanpop.com/image/photos/33900000/The-Beatles-the-beatles-33951025-1100-824.jpg"],
            "album_tiles": ["http://www.beatlesbible.com/wp/media/canada_twist_and_shout.jpg",
                            "http://upload.wikimedia.org/wikipedia/en/a/a4/PleasePleaseMe.jpg",
                            "http://www.beatlesbible.com/wp/media/with_the_beatles.jpg",
                            "http://www.eskimo.com/~bpentium/beatles/intro/vjintrlp.jpg",
                            "http://0.tqn.com/d/classicrock/1/0/X/J/meetthebeatles.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/r5nARZKS-AY",
                               "http://www.youtube.com/embed/eDdI7GhZSQA"],
            "citation":"http://en.wikipedia.org/wiki/The_Beatles"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/artists/The_Beatles/", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)
    
    # 5
    # Delete an existing artist
    #
    def test_1_delete_artist(self):
        request = Request("http://127.0.0.1:8000/api/artists/The_Beatles/")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)

    
    # 6
    # List all related albums
    #
    def test_get_artists_album(self):
        response = urlopen("http://127.0.0.1:8000/api/artists/Lady_Gaga/albums")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"year_released": 2008, "labels": ["Cherrytree Records", "Interscope Records"], "name": "The Fame", "artists": ["Lady Gaga"]}]
        self.assertTrue(expected_response == our_response)

    # 7
    # List all related labels
    #
    def test_get_artists_labels(self):
        response = urlopen("http://127.0.0.1:8000/api/artists/Lady_Gaga/labels")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"founded": 2005, "name": "Cherrytree Records", "location": "Santa Monica, CA"}, {"founded": 1983, "name": "Def Jam Recordings", "location": "New York City, NY"}, {"founded": 1989, "name": "Interscope Records", "location": "Santa Monica, CA"}]
        self.assertTrue(expected_response == our_response)
    
class TestAlbumAPI(TestCase):
    #-----------------#
    # ALBUM DATA
    #-----------------#
    
    # 1
    # Displays the information for a specific album where id is the database primary key
    #
    
    def test_get_single_album(self):
        response = urlopen("http://127.0.0.1:8000/api/albums/Warrior/")

        self.assertEqual(response.getcode(), 200)

        our_response = response.readall().decode("utf-8")

        our_response = loads(our_response)

        expected_response = {"genres": ["Party rap"], 
                             "name": "Warrior", 
                             "tracks": ["All That Matters", 
                                        "C'mon", 
                                        "Crazy Kids", 
                                        "Die Young", 
                                        "Dirty Love", 
                                        "Good Warrior", 
                                        "Love into the Light", 
                                        "Only Wanna Dance with You", 
                                        "Supernatural", 
                                        "Thinking of You", 
                                        "Wherever You Are", 
                                        "Wonderland"], 
                             "slideshow_images": ["http://wanderingoak.files.wordpress.com/2012/12/kesha-warrior.jpg", 
                                        "http://www.airrockstar.com/gallery/images/Kesha_Warrior_v9.jpg", 
                                        "http://i.huffpost.com/gen/890451/thumbs/o-KESHA-NEW-ALBUM-WARRIOR-facebook.jpg", 
                                        "http://images.4aus.com/wp-content/uploads/2012/12/kesha-warrior.jpg"], 
                             "labels": ["RCA Records", "Kemosabe"], 
                             "cover": "http://en.wikipedia.org/wiki/File:Kesha_Warrior.jpeg", 
                             "sales": 320000.0, 
                             "peak_chart": 1, 
                             "sellers": "http://www.amazon.com/KE-Kesha-WARRIOR-DELUXE-DECONSTRUCTED/dp/B00AWS25MG", 
                             "producers": ["Ammo", "Benny Blanco", 
                                        "Dr.Luke", "Greg Kurstin", "Kool Kojack", 
                                        "Matt Squire", "Max Martin", "Shellback", "The Flaming Lips"], 
                             "length": "44:27", 
                             "citations": "http://en.wikipedia.org/wiki/Warrior_(Kesha_album)", 
                             "artists": ["Kesha"], 
                             "year_released": 2012, 
                             "video_slideshow": ["http://www.youtube.com/embed/V8NRmMLb3kY", 
                                        "http://www.youtube.com/embed/xdeFB7I0YH4"], 
                             "description": "Warrior is the second studio album by recording artist Kesha."}

        self.assertTrue(expected_response == our_response)
    
    # 2
    # Lists the albums in the database
    #
    def test_get_albums(self):
        response = urlopen("http://127.0.0.1:8000/api/albums")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"year_released": 1991, "labels": ["Interscope Records"], "name": "2Pacalypse Now", "artists": ["2Pac"]}, {"year_released": 2002, "labels": ["Parlophone Records"], "name": "A Rush of Blood to the Head", "artists": ["Coldplay"]}, {"year_released": 2008, "labels": ["Cherrytree Records", "Interscope Records", "Streamline", "Kon Live"], "name": "The Fame", "artists": ["Lady Gaga", "Colby O'Donis", "Space Cowboy", "Flo-rida"]}, {"year_released": 2009, "labels": ["Def Jam Recordings", "SRP"], "name": "Rated R", "artists": ["Rihanna", "Slash", "Jeezy"]}, {"year_released": 1998, "labels": ["Warner Bros Records", "Maverick"], "name": "Ray of Light", "artists": ["Madonna"]}, {"year_released": 2010, "labels": ["Aftermath Entertainment", "Shady"], "name": "Recovery", "artists": ["Lil Wayne"]}, {"year_released": 1973, "labels": ["EMI", "Harvest"], "name": "The Dark Side of the Moon", "artists": ["Pink Floyd"]}, {"year_released": 1964, "labels": ["Capitol Records"], "name": "Twist and Shout", "artists": ["The Beatles"]}, {"year_released": 2012, "labels": ["RCA Records", "Kemosabe"], "name": "Warrior", "artists": ["Kesha"]}, {"year_released": 2009, "labels": ["will.i.am"], "name": "willpower", "artists": ["will i am", "Slash", "Jeezy"]}]
        expected_response = sorted(expected_response, key=lambda x: x['name'])
        self.assertTrue(expected_response == our_response)
    
    # 3
    # Create a new entry for album.
    #
    
    def test_2_post_single_album(self):
        values = dumps({"genres": ["Party rap"], "name": "Warrior", "tracks": ["All That Matters", "C'mon", "Crazy Kids", "Die Young", "Dirty Love", "Good Warrior", "Love into the Light", "Only Wanna Dance with You", "Supernatural", "Thinking of You", "Wherever You Are", "Wonderland"], "slideshow_images": ["http://wanderingoak.files.wordpress.com/2012/12/kesha-warrior.jpg", "http://www.airrockstar.com/gallery/images/Kesha_Warrior_v9.jpg", "http://i.huffpost.com/gen/890451/thumbs/o-KESHA-NEW-ALBUM-WARRIOR-facebook.jpg", "http://images.4aus.com/wp-content/uploads/2012/12/kesha-warrior.jpg"], "labels": ["RCA Records", "Kemosabe"], "cover": "http://en.wikipedia.org/wiki/File:Kesha_Warrior.jpeg", "sales": 320000.0, "peak_chart": 1, "sellers": "http://www.amazon.com/KE-Kesha-WARRIOR-DELUXE-DECONSTRUCTED/dp/B00AWS25MG", "producers": ["Ammo", "Benny Blanco", "Dr.Luke", "Greg Kurstin", "Kool Kojack", "Matt Squire", "Max Martin", "Shellback", "The Flaming Lips"], "length": "44:27", "citations": "http://en.wikipedia.org/wiki/Warrior_(Kesha_album)", "artists": ["Kesha"], "year_released": 2012, "video_slideshow": ["http://www.youtube.com/embed/V8NRmMLb3kY", "http://www.youtube.com/embed/xdeFB7I0YH4"], "id": 9, "description": "Warrior is the second studio album by recording artist Kesha."})
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/albums/", data=values, headers=headers)
        #request.get_method = lambda: "POST"

        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        our_response = response.readall().decode("utf-8")
        actual_response = '{"name": "Warrior"}'
        self.assertEqual(our_response, actual_response)
    
    # 4
    # Update an existing album
    #
    
    def test_put_album(self):
        values = dumps({"genres": ["Party rap"], "name": "Warrior", "tracks": ["All That Matters", "C'mon", "Crazy Kids", "Die Young", "Dirty Love", "Good Warrior", "Love into the Light", "Only Wanna Dance with You", "Supernatural", "Thinking of You", "Wherever You Are", "Wonderland"], "slideshow_images": ["http://wanderingoak.files.wordpress.com/2012/12/kesha-warrior.jpg", "http://www.airrockstar.com/gallery/images/Kesha_Warrior_v9.jpg", "http://i.huffpost.com/gen/890451/thumbs/o-KESHA-NEW-ALBUM-WARRIOR-facebook.jpg", "http://images.4aus.com/wp-content/uploads/2012/12/kesha-warrior.jpg"], "labels": ["RCA Records", "Kemosabe"], "cover": "http://en.wikipedia.org/wiki/File:Kesha_Warrior.jpeg", "sales": 320000.0, "peak_chart": 1, "sellers": "http://www.amazon.com/KE-Kesha-WARRIOR-DELUXE-DECONSTRUCTED/dp/B00AWS25MG", "producers": ["Ammo", "Benny Blanco", "Dr.Luke", "Greg Kurstin", "Kool Kojack", "Matt Squire", "Max Martin", "Shellback", "The Flaming Lips"], "length": "44:27", "citations": "http://en.wikipedia.org/wiki/Warrior_(Kesha_album)", "artists": ["Kesha"], "year_released": 2012, "video_slideshow": ["http://www.youtube.com/embed/V8NRmMLb3kY", "http://www.youtube.com/embed/xdeFB7I0YH4"], "id": 9, "description": "Warrior is the second studio album by recording artist Kesha."})
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/albums/Warrior/", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)
    
    # 5
    # Delete an existing album
    #
    
    def test_1_delete_album(self):
        request = Request("http://127.0.0.1:8000/api/albums/Warrior/")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)
    
    # 6
    # List all related albums
    #
    def test_get_album_artists(self):
        response = urlopen("http://127.0.0.1:8000/api/albums/Warrior/artists")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = []
        self.assertTrue(expected_response == our_response)

    
    # 7
    # List all related labels
    #
    def test_get_albums_labels(self):
        response = urlopen("http://127.0.0.1:8000/api/albums/Warrior/labels")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"founded": 1901, "name": "RCA Records", "location": "New York City, NY"}]
        self.assertTrue(expected_response == our_response)

class TestLabelAPI(TestCase):
#-----------------#
# Label DATA
#-----------------#

    # 1
    # Displays the information for a specific label where id is the database primary key
    #
    def test_0_get_single_label(self):
        response = urlopen("http://127.0.0.1:8000/api/labels/Capitol_Records/")

        self.assertEqual(response.getcode(), 200)

        our_response = response.readall().decode("utf-8")

        our_response = loads(our_response)

        expected_response = {"website": "http://www.capitolrecords.com/", "name": "Capitol Records", "twitter": "451168645687697408", "map_link": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726", "slideshow_images": ["/static/images/capitol_records_slideshow_0.jpg", "/static/images/capitol_records_slideshow_1.jpg", "/static/images/capitol_records_slideshow_2.jpg", "/static/images/capitol_records_slideshow_3.jpg"], "founded": 1942, "parent_company": "Universal Music Group", "citations": "http://en.wikipedia.org/wiki/Capital_Records", "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", "location": "Los Angeles, CA", "artists": ["50_cent", "Avicii", "Katy Perry", "Red Hot Chili Peppers", "The Beach Boys", "The Beatles"], "founders": ["Buddy DeSylva", "Glenn Wallichs", "Johnny Mercer"], "logo": "/static/images/capitol_records_main.jpg", "albums": ["As Far as Siam", "Bad Animals", "Bazooka!!!", "Catarsis", "Honky Tonk", "Twist and Shout"], "video_slideshow": ["http://www.youtube.com/embed/TjMWQff10Vw", "http://www.youtube.com/embed/T2ovD7-yNAY"], "id": 2, "description": "Capitol Records is a major American record label that is part of the Capitol Music Group"}

        self.assertTrue(expected_response == our_response)

    def test_1_get_single_wrong_label(self):
        try:
            response = urlopen("http://127.0.0.1:8000/api/labels/Capitol/")
            assertTrue(False)
        except:
            self.assertTrue(True)

    # 2
    # Lists the labels in the database
    #
    def test_2_get_labels(self):
        response = urlopen("http://127.0.0.1:8000/api/labels/")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [{"founded": 1996, "name": "Aftermath Entertainment", "location": "Santa Monica, CA"}, {"founded": 1942, "name": "Capitol Records", "location": "Los Angeles, CA"}, {"founded": 2005, "name": "Cherrytree Records", "location": "Santa Monica, CA"}, {"founded": 1983, "name": "Def Jam Recordings", "location": "New York City, NY"}, {"founded": 1931, "name": "EMI", "location": "London, England, UK"}, {"founded": 1989, "name": "Interscope Records", "location": "Santa Monica, CA"}, {"founded": 1896, "name": "Parlophone Records", "location": "UK"}, {"founded": 1901, "name": "RCA Records", "location": "New York City, NY"}, {"founded": 1987, "name": "Ruthless Records", "location": "Los Angeles, CA"}, {"founded": 1958, "name": "Warner Bros Records", "location": "Burbank, CA"}]
        self.assertTrue(expected_response == our_response)

    # # 3
    # # Create a new entry for label.
    # #

    def test_6_post_single_label(self):
        values = dumps({    
                        "name": "Capitol Records",
                        "albums": ["Bad Animals",
                                   "Twist and Shout",
                                   "Bazooka!!!",
                                   "Catarsis",
                                   "As Far as Siam",
                                   "Honky Tonk"],
                        "artists": ["The Beatles",
                                    "50_cent",
                                    "Avicii",
                                    "Red Hot Chili Peppers",
                                    "Katy Perry",
                                    "The Beach Boys"],
                        "founded": 1943,
                        "founders": ["Johnny Mercer", "Buddy DeSylva", "Glenn Wallichs"],
                        "location": "Los Angeles, CA",
                        "map": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726",
                        "parent": "Universal Music Group",
                        "website": "http://www.capitolrecords.com/",
                        "logo": "http://upload.wikimedia.org/wikipedia/en/8/8f/CapitolRecords_Logo.png",
                        "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true",
                        "twitter": "451168645687697408",
                        "description": "Capitol Records is a major American record label that is part of the Capitol Music Group",
                        "slideshow_images":["http://www.smartlayover.com/slportal/media/CAP3.jpg",
                                            "http://library.umkc.edu/spec-col/ww2/pacifictheater/images/hope6-sm.jpg",
                                            "http://upload.wikimedia.org/wikipedia/commons/6/60/Robert_Clary_Capitol_Records_circa_1950.JPG",
                                            "http://www.tenhighcoaches.com/images/Capitol%20Records.jpg"],
                        "video_slideshow":["http://www.youtube.com/embed/TjMWQff10Vw",
                                           "http://www.youtube.com/embed/T2ovD7-yNAY"],
                        "citation": "http://en.wikipedia.org/wiki/Capital_Records"
                    })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/labels/", data=values, headers=headers)
        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        our_response = response.readall().decode("utf-8")
        actual_response = '{"name": "Capitol Records"}'
        self.assertEqual(our_response, actual_response)

    # 4
    # Update an existing artist
    #
    def test_7_put_label(self):
        values = dumps({    
                        "name": "Capitol Records",
                        "albums": ["Bad Animals",
                                   "Twist and Shout",
                                   "Bazooka!!!",
                                   "Catarsis",
                                   "As Far as Siam",
                                   "Honky Tonk"],
                        "artists": ["The Beatles",
                                    "50_cent",
                                    "Avicii",
                                    "Red Hot Chili Peppers",
                                    "Katy Perry",
                                    "The Beach Boys"],
                        "founded": 1942,
                        "founders": ["Johnny Mercer", "Buddy DeSylva", "Glenn Wallichs"],
                        "location": "Los Angeles, CA",
                        "map_link": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726",
                        "parent": "Universal Music Group",
                        "website": "http://www.capitolrecords.com/",
                        "logo": "http://upload.wikimedia.org/wikipedia/en/8/8f/CapitolRecords_Logo.png",
                        "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true",
                        "twitter": "451168645687697408",
                        "description": "Capitol Records is a major American record label that is part of the Capitol Music Group",
                        "slideshow_images":["http://www.smartlayover.com/slportal/media/CAP3.jpg",
                                            "http://library.umkc.edu/spec-col/ww2/pacifictheater/images/hope6-sm.jpg",
                                            "http://upload.wikimedia.org/wikipedia/commons/6/60/Robert_Clary_Capitol_Records_circa_1950.JPG",
                                            "http://www.tenhighcoaches.com/images/Capitol%20Records.jpg"],
                        "video_slideshow":["http://www.youtube.com/embed/TjMWQff10Vw",
                                           "http://www.youtube.com/embed/T2ovD7-yNAY"],
                        "citation": "http://en.wikipedia.org/wiki/Capital_Records"
                    })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://127.0.0.1:8000/api/labels/Capitol_Records/", data=values, headers=headers)
        request.get_method = lambda: "PUT"
      
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)
    
    # 5
    # Delete an existing label
    #
    def test_5_delete_label(self):
        request = Request("http://127.0.0.1:8000/api/labels/Capitol_Records/")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)


    # 6
    # List all related albums
    #
    def test_3_get_label_artists(self):
        response = urlopen("http://127.0.0.1:8000/api/labels/Capitol_Records/artists")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
                              {
                                "band_name": "The Beatles",
                                "hometown": "Liverpool, England",
                                "years_active": "1960-1970"
                              }
                            ]
        self.assertTrue(expected_response == our_response)

    # 7
    # List all related labels
    #
    def test_4_get_label_albums(self):
        response = urlopen("http://127.0.0.1:8000/api/labels/Capitol_Records/albums")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
                              {
                                "year_released": 1964,
                                "labels": [
                                  "Capitol Records"
                                ],
                                "name": "Twist and Shout",
                                "artists": [
                                  "The Beatles"
                                ]
                              }
                            ]
        self.assertTrue(expected_response == our_response)

class TestApiary(TestCase):
    #-----------------#
    # ALBUM DATA
    #-----------------#


    # 1
    # Lists the albums in the database
    #
    def test_get_albums(self):
        # Use this URL to access a mockup of the API server.
        response = urlopen("http://music1.apiary-mock.com/api/albums")

        # Validate  HTTP status code
        self.assertEqual(response.getcode(), 200)

        #Read our response, variable-width decode utf-8, accepted by python3
        our_response = response.readall().decode("utf-8")

        #Decoding JSON. Will take a JSON string and turn it into a python dictionary.
        our_response = loads(our_response)

        expected_response = [
            {
                "name": "Twist and Shout",
                "artists": ["The Beatles"],
                "year_released": 1964,
                "labels": ["Capitol Records"]
            }
        ]
        #Validate string
        self.assertTrue(expected_response == our_response)


    # 2
    # Displays the information for a specific album, id is the database primary key
    #
    def test_get_single_album(self):
        # Use this URL to access a mockup of the API server.
        response = urlopen("http://music1.apiary-mock.com/api/albums/{id}")

        # Validate  HTTP status code        
        self.assertEqual(response.getcode(), 200)

        #Read our response, variable-width decode utf-8, accepted by python3
        our_response = response.readall().decode("utf-8")

        #Decoding JSON. Will take a JSON string and turn it into a python dictionary.
        our_response = loads(our_response)
        self.assertTrue(our_response["name"] == "Twist and Shout")
        self.assertTrue(our_response["artists"] == ["The Beatles"])
        self.assertTrue(our_response["year_released"] == 1964)
        self.assertTrue(our_response["tracks"] == ["Anna (Go to Him)",
                                                   "Chains",
                                                   "Boys",
                                                   "Ask Me Why",
                                                   "Please Please Me",
                                                   "Love Me Do",
                                                   "From Me to You",
                                                   "P.S. I Love You",
                                                   "Baby It's You",
                                                   "Do You Want to Know a Secret",
                                                   "Taste of Honey",
                                                   "There's a Place",
                                                   "Twist and Shout",
                                                   "She Loves You"])
        self.assertTrue(our_response["genres"] == ["Pop"])
        self.assertTrue(our_response["peak_chart"] == 0)
        self.assertTrue(our_response["videos"] == "http://www.youtube.com/watch?v=pVlr4g5-r18")
        self.assertTrue(our_response["length"] == "31:30")
        self.assertTrue(our_response["labels"] == ["Capitol Records"])
        self.assertTrue(our_response["producers"] == ["George Martin"])
        self.assertTrue(our_response["sales"] == 300000)
        self.assertTrue(
            our_response["artwork"] == "http://upload.wikimedia.org/wikipedia/en/6/61/BeatlesTwistanShoutSingle.jpg")
        self.assertTrue(our_response[
                            "sellers"] == "http://www.amazon.com/Twist-Shout-Beatles-Canadian-Pressing/dp/B00B7C0ET2/ref=sr_1_15?ie=UTF8&qid=1395101443&sr=8-15&keywords=twist+and+shout+the+beatles")
        self.assertTrue(
            our_response["description"] == "The Beatles' second album released in Canada in mono by Capitol Records.")
        self.assertTrue(
            our_response["slideshow_images"] == ["http://www.prlog.org/12150175-twist-shout-beatles-tribute.png",
                                                 "http://i1.sndcdn.com/artworks-000023437222-oq3lly-original.jpg?435a760",
                                                 "http://www.beatlesbible.com/wp/media/italy_twist_and_shout_01.jpg",
                                                 "http://mobile.collectorsfrenzy.com/gallery/111000157651.jpg"])
        self.assertTrue(our_response["video_slideshow"] == ["http://www.youtube.com/embed/aG9pvAUBKlU",
                                                            "http://www.youtube.com/embed/1q803OveQ2k"])
        self.assertTrue(our_response["citation"] == "http://en.wikipedia.org/wiki/Twist_and_Shout_(album)")



        # 3
        # Create a new entry for album.
        #

    def test_post_single_album(self):
        #Take a python object(dictionary) and serialize to JSON
        values = dumps({
            "name": "Twist and Shout",
            "artists": ["The Beatles"],
            "year_released": 1964,
            "tracks": ["Anna (Go to Him)",
                       "Chains",
                       "Boys",
                       "Ask Me Why",
                       "Please Please Me",
                       "Love Me Do",
                       "From Me to You",
                       "P.S. I Love You",
                       "Baby It's You",
                       "Do You Want to Know a Secret",
                       "Taste of Honey",
                       "There's a Place",
                       "Twist and Shout",
                       "She Loves You"],
            "genres":["Pop"],
            "peak_chart": 0,
            "videos": "http://www.youtube.com/watch?v=pVlr4g5-r18",
            "length": "31:30",
            "labels": ["Capitol Records"],
            "producers": ["George Martin"],
            "sales" : 300000,
            "artwork": "http://upload.wikimedia.org/wikipedia/en/6/61/BeatlesTwistanShoutSingle.jpg",
            "sellers": "http://www.amazon.com/Twist-Shout-Beatles-Canadian-Pressing/dp/B00B7C0ET2/ref=sr_1_15?ie=UTF8&qid=1395101443&sr=8-15&keywords=twist+and+shout+the+beatles",
            "description": "The Beatles' second album released in Canada in mono by Capitol Records.",
            "slideshow_images":["http://www.prlog.org/12150175-twist-shout-beatles-tribute.png","http://i1.sndcdn.com/artworks-000023437222-oq3lly-original.jpg?435a760","http://www.beatlesbible.com/wp/media/italy_twist_and_shout_01.jpg","http://mobile.collectorsfrenzy.com/gallery/111000157651.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/aG9pvAUBKlU","http://www.youtube.com/embed/1q803OveQ2k"],
            "citation":"http://en.wikipedia.org/wiki/Twist_and_Shout_(album)"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/albums", data=values, headers=headers)

        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        #formatting response
        our_response = response.readall().decode("utf-8")

        actual_response = '{ name: \"Twist and Shout\"}'
        self.assertEqual(our_response, actual_response)

    # 4
    # Update an existing album entry.
    #
    def test_put_album(self):
        values = dumps({
            "name": "Twist and Shout",
            "artists": ["The Beatles"],
            "year_released": 1964,
            "tracks": ["Anna (Go to Him)",
                       "Chains",
                       "Boys",
                       "Ask Me Why",
                       "Please Please Me",
                       "Love Me Do",
                       "From Me to You",
                       "P.S. I Love You",
                       "Baby It's You",
                       "Do You Want to Know a Secret",
                       "Taste of Honey",
                       "There's a Place",
                       "Twist and Shout",
                       "She Loves You"],
            "genres":["Pop"],
            "peak_chart": 0,
            "videos": "http://www.youtube.com/watch?v=pVlr4g5-r18",
            "length": "31:30",
            "labels": ["Capitol Records"],
            "producers": ["George Martin"],
            "sales" : 300000,
            "artwork": "http://upload.wikimedia.org/wikipedia/en/6/61/BeatlesTwistanShoutSingle.jpg",
            "sellers": "http://www.amazon.com/Twist-Shout-Beatles-Canadian-Pressing/dp/B00B7C0ET2/ref=sr_1_15?ie=UTF8&qid=1395101443&sr=8-15&keywords=twist+and+shout+the+beatles",
            "description": "The Beatles' second album released in Canada in mono by Capitol Records.",
            "slideshow_images":["http://www.prlog.org/12150175-twist-shout-beatles-tribute.png","http://i1.sndcdn.com/artworks-000023437222-oq3lly-original.jpg?435a760","http://www.beatlesbible.com/wp/media/italy_twist_and_shout_01.jpg","http://mobile.collectorsfrenzy.com/gallery/111000157651.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/aG9pvAUBKlU","http://www.youtube.com/embed/1q803OveQ2k"],
            "citation":"http://en.wikipedia.org/wiki/Twist_and_Shout_(album)"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/albums/{id}", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)

    # 5
    # Delete an existing album
    #
    def test_delete_album(self):
        request = Request("http://music1.apiary-mock.com/api/albums/{id}")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)

    # 6
    # List all related labels
    #
    def test_get_albums_labels(self):
        response = urlopen("http://music1.apiary-mock.com/api/albums/{id}/labels")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "name": "Capitol Records",
                "founded": 1942,
                "location": "Los Angeles, CA"
            }
        ]
        self.assertTrue(expected_response == our_response)


    # 7
    # List all related artists
    #
    def test_get_albums_artists(self):
        response = urlopen("http://music1.apiary-mock.com/api/albums/{id}/artists")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "band_name": "The Beatles",
                "hometown": "Liverpool, England",
                "years_active": "1960-1970"
            }
        ]
        self.assertTrue(expected_response == our_response)

    #-----------------#
    # ARTIST DATA
    #-----------------#
    # 8
    # Lists the artists in the database
    #
    def test_get_artists(self):
        response = urlopen("http://music1.apiary-mock.com/api/artists")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "band_name": "The Beatles",
                "hometown": "Liverpool, England",
                "years_active": "1960-1970"
            }
        ]
        self.assertTrue(expected_response == our_response)


    # 9
    # Displays the information for a specific artist, id is the database primary key
    #
    def test_get_single_artist(self):
        response = urlopen("http://music1.apiary-mock.com/api/artists/{id}")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        self.assertTrue(our_response["members"] == ["John Lennon", "Paul McCartney", "George Harrison", "Ringo Starr"])
        self.assertTrue(our_response["band_name"] == "The Beatles")
        self.assertTrue(our_response["hometown"] == "Liverpool, England")
        self.assertTrue(our_response["years_active"] == "1960-1970")
        self.assertTrue(our_response["albums"] == ["Twist and Shout", "Please Please Me", "With the Beatles",
                                                   "Beatlemania! With the Beatles", "Introducing...The Beatles",
                                                   "Meet the Beatles!", "The Beatles' Second Album",
                                                   "The Beatles' Long Tall Sally", "A Hard Day's Night",
                                                   "Something New", "Beatles for Sale", "Beatles '65'", "Beatles VI",
                                                   "Help!", "Rubber Soul", "Yesterday and Today", "Revolver",
                                                   "Sgt.Pepper's Lonely Hearts Club Band", "Magical Mystery Tour",
                                                   "The Beatles", "Yellow Submarine", "Abbey Road", "Let It Be"])
        self.assertTrue(
            our_response["labels"] == ["Capitol Records", "Parlophone Records", "Swan", "Vee-Jay", "United Artists",
                                       "Apple"])
        self.assertTrue(our_response["images"] == "http://2.bp.blogspot.com/_xzXp2k6_nh0/TSCZBEyuudI/AAAAAAAABA4/tnOe-vsHx1w/s1600/TheBeatlesLogo2+-+copia.jpg")
        self.assertTrue(our_response["facebook"] == "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fthebeatles&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true")
        self.assertTrue(our_response["twitter"] == "451168834007732224")
        self.assertTrue(our_response["google_map"] == "https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Liverpool,+England&amp;aq=&amp;sll=30.307761,-97.753401&amp;sspn=1.206926,2.024231&amp;ie=UTF8&amp;hq=&amp;hnear=Liverpool,+Merseyside,+United+Kingdom&amp;t=m&amp;z=12&amp;ll=53.408371,-2.991573&amp;output=embed")
        self.assertTrue(our_response["description"] == "The Beatles were an English rock band that formed in Liverpool, in 1960. With John Lennon, Paul McCartney, George Harrison, and Ringo Starr, they became widely regarded as the greatest and most influential act of the rock era.")
        self.assertTrue(our_response["slideshow_images"] == [
                                    "http://ic.pics.livejournal.com/jonesingjay/12452594/289066/original.jpg",
                                    "http://beatlesfacts.files.wordpress.com/2011/09/hellogoodbye.jpg",
                                    "http://i0.wp.com/djournal.com/wp-content/blogs.dir/42/files/2013/10/Rain-Beatles1.jpg",
                                    "http://images6.fanpop.com/image/photos/33900000/The-Beatles-the-beatles-33951025-1100-824.jpg"])
        self.assertTrue(
            our_response["album_tiles"] == ["http://www.beatlesbible.com/wp/media/canada_twist_and_shout.jpg",
                                            "http://upload.wikimedia.org/wikipedia/en/a/a4/PleasePleaseMe.jpg",
                                            "http://www.beatlesbible.com/wp/media/with_the_beatles.jpg",
                                            "http://www.eskimo.com/~bpentium/beatles/intro/vjintrlp.jpg",
                                            "http://0.tqn.com/d/classicrock/1/0/X/J/meetthebeatles.jpg"])
        self.assertTrue(our_response["video_slideshow"] == ["http://www.youtube.com/embed/r5nARZKS-AY",
                                                            "http://www.youtube.com/embed/eDdI7GhZSQA"])
        self.assertTrue(our_response["citation"] == "http://en.wikipedia.org/wiki/The_Beatles")

    # 10
    # Create a new entry for artist.
    #

    def test_post_single_artist(self):
        values = dumps({
            "members":["John Lennon", "Paul McCartney", "George Harrison", "Ringo Starr"],
            "band_name": "The Beatles",
            "hometown": "Liverpool, England",
            "years_active": "1960-1970",
            "albums": ["Twist and Shout", "Please Please Me", "With the Beatles","Beatlemania! With the Beatles", "Introducing...The Beatles","Meet the Beatles!", "The Beatles' Second Album","The Beatles' Long Tall Sally","A Hard Day's Night","Something New","Beatles for Sale","Beatles '65'","Beatles VI", "Help!","Rubber Soul","Yesterday and Today","Revolver","Sgt.Pepper's Lonely Hearts Club Band","Magical Mystery Tour","The Beatles","Yellow Submarine","Abbey Road","Let It Be"],
            "labels": ["Capitol Records","Parlophone Records", "Swan","Vee-Jay","United Artists", "Apple"],
            "images": "http://2.bp.blogspot.com/_xzXp2k6_nh0/TSCZBEyuudI/AAAAAAAABA4/tnOe-vsHx1w/s1600/TheBeatlesLogo2+-+copia.jpg",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fthebeatles&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", 
            "twitter": "451168834007732224",
            "google_map":"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Liverpool,+England&amp;aq=&amp;sll=30.307761,-97.753401&amp;sspn=1.206926,2.024231&amp;ie=UTF8&amp;hq=&amp;hnear=Liverpool,+Merseyside,+United+Kingdom&amp;t=m&amp;z=12&amp;ll=53.408371,-2.991573&amp;output=embed",
            "description": "The Beatles were an English rock band that formed in Liverpool, in 1960. With John Lennon, Paul McCartney, George Harrison, and Ringo Starr, they became widely regarded as the greatest and most influential act of the rock era.",
            "slideshow_images": ["http://ic.pics.livejournal.com/jonesingjay/12452594/289066/original.jpg",
                                 "http://beatlesfacts.files.wordpress.com/2011/09/hellogoodbye.jpg",
                                 "http://i0.wp.com/djournal.com/wp-content/blogs.dir/42/files/2013/10/Rain-Beatles1.jpg",
                                 "http://images6.fanpop.com/image/photos/33900000/The-Beatles-the-beatles-33951025-1100-824.jpg"],
            "album_tiles": ["http://www.beatlesbible.com/wp/media/canada_twist_and_shout.jpg",
                            "http://upload.wikimedia.org/wikipedia/en/a/a4/PleasePleaseMe.jpg",
                            "http://www.beatlesbible.com/wp/media/with_the_beatles.jpg",
                            "http://www.eskimo.com/~bpentium/beatles/intro/vjintrlp.jpg",
                            "http://0.tqn.com/d/classicrock/1/0/X/J/meetthebeatles.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/r5nARZKS-AY",
                               "http://www.youtube.com/embed/eDdI7GhZSQA"],
            "citation":"http://en.wikipedia.org/wiki/The_Beatles"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/artists", data=values, headers=headers)

        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        our_response = response.readall().decode("utf-8")

        actual_response = '{ band_name: \"The Beatles\"}'
        self.assertEqual(our_response, actual_response)

    # 11
    # Update an existing artist
    #
    def test_put_artist(self):
        values = dumps({
            "members":["John Lennon", "Paul McCartney", "George Harrison", "Ringo Starr"],
            "band_name": "The Beatles",
            "hometown": "Liverpool, England",
            "years_active": "1960-1970",
            "albums": ["Twist and Shout", "Please Please Me", "With the Beatles","Beatlemania! With the Beatles", "Introducing...The Beatles","Meet the Beatles!", "The Beatles' Second Album","The Beatles' Long Tall Sally","A Hard Day's Night","Something New","Beatles for Sale","Beatles '65'","Beatles VI", "Help!","Rubber Soul","Yesterday and Today","Revolver","Sgt.Pepper's Lonely Hearts Club Band","Magical Mystery Tour","The Beatles","Yellow Submarine","Abbey Road","Let It Be"],
            "labels": ["Capitol Records","Parlophone Records", "Swan","Vee-Jay","United Artists", "Apple"],
            "images": "http://2.bp.blogspot.com/_xzXp2k6_nh0/TSCZBEyuudI/AAAAAAAABA4/tnOe-vsHx1w/s1600/TheBeatlesLogo2+-+copia.jpg",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fthebeatles&width&height=427&colorscheme=light&show_faces=false&header=true&stream=true&show_border=true", 
            "twitter": "451168834007732224",
            "google_map":"https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Liverpool,+England&amp;aq=&amp;sll=30.307761,-97.753401&amp;sspn=1.206926,2.024231&amp;ie=UTF8&amp;hq=&amp;hnear=Liverpool,+Merseyside,+United+Kingdom&amp;t=m&amp;z=12&amp;ll=53.408371,-2.991573&amp;output=embed",
            "description": "The Beatles were an English rock band that formed in Liverpool, in 1960. With John Lennon, Paul McCartney, George Harrison, and Ringo Starr, they became widely regarded as the greatest and most influential act of the rock era.",
            "slideshow_images": ["http://ic.pics.livejournal.com/jonesingjay/12452594/289066/original.jpg",
                                 "http://beatlesfacts.files.wordpress.com/2011/09/hellogoodbye.jpg",
                                 "http://i0.wp.com/djournal.com/wp-content/blogs.dir/42/files/2013/10/Rain-Beatles1.jpg",
                                 "http://images6.fanpop.com/image/photos/33900000/The-Beatles-the-beatles-33951025-1100-824.jpg"],
            "album_tiles": ["http://www.beatlesbible.com/wp/media/canada_twist_and_shout.jpg",
                            "http://upload.wikimedia.org/wikipedia/en/a/a4/PleasePleaseMe.jpg",
                            "http://www.beatlesbible.com/wp/media/with_the_beatles.jpg",
                            "http://www.eskimo.com/~bpentium/beatles/intro/vjintrlp.jpg",
                            "http://0.tqn.com/d/classicrock/1/0/X/J/meetthebeatles.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/r5nARZKS-AY",
                               "http://www.youtube.com/embed/eDdI7GhZSQA"],
            "citation":"http://en.wikipedia.org/wiki/The_Beatles"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/artists/{id}", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)

    # 12
    # Delete an existing artist
    #
    def test_delete_artist(self):
        request = Request("http://music1.apiary-mock.com/api/artists/{id}")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)


    # 13
    # List all related albums
    #
    def test_get_artists_album(self):
        response = urlopen("http://music1.apiary-mock.com/api/artists/{id}/albums")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "name": "Twist and Shout",
                "artists": ["The Beatles"],
                "year_released": 1964,
                "labels": ["Capitol Records"]
            }
        ]
        self.assertTrue(expected_response == our_response)


    # 14
    # List all related labels
    #
    def test_get_artists_labels(self):
        response = urlopen("http://music1.apiary-mock.com/api/artists/{id}/labels")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "name": "Capitol Records",
                "founded": 1942,
                "location": "Los Angeles, CA"
            }
        ]
        self.assertTrue(expected_response == our_response)


    #-----------------#
    # LABEL DATA
    #-----------------#
    # 15
    # Lists the labels in the database
    #
    def test_get_label(self):
        response = urlopen("http://music1.apiary-mock.com/api/labels")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = [
            {
                "name": "Capitol Records",
                "founded": 1942,
                "location": "Los Angeles, CA"
            }
        ]
        self.assertTrue(expected_response == our_response)


    # 16
    # Displays the information for a specific label, id is the database primary key
    #
    def test_get_single_label(self):
        response = urlopen("http://music1.apiary-mock.com/api/labels/{id}")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        self.assertTrue(our_response["name"] == "Capitol Records")
        self.assertTrue(our_response["albums"] == ["Bad Animals",
                                                   "Twist and Shout",
                                                   "Bazooka!!!",
                                                   "Catarsis",
                                                   "As Far as Siam",
                                                   "Honky Tonk"])
        self.assertTrue(our_response["artists"] == ["The Beatles",
                                                    "50_cent",
                                                    "Avicii",
                                                    "Red Hot Chili Peppers",
                                                    "Katy Perry",
                                                    "The Beach Boys"])
        self.assertTrue(our_response["founded"] == 1942)
        self.assertTrue(our_response["founders"] == ["Johnny Mercer", "Buddy DeSylva", "Glenn Wallichs"])
        self.assertTrue(our_response["location"] == "Los Angeles, CA")
        self.assertTrue(our_response[
                            "map"] == "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726")
        self.assertTrue(our_response["parent"] == "Universal Music Group")
        self.assertTrue(our_response["website"] == "http://www.capitolrecords.com/")
        self.assertTrue(our_response["logo"] == "http://upload.wikimedia.org/wikipedia/en/8/8f/CapitolRecords_Logo.png")
        self.assertTrue(our_response[
                            "facebook"] == "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true")
        self.assertTrue(our_response["twitter"] == "451168645687697408")
        self.assertTrue(our_response[
                            "description"] == "Capitol Records is a major American record label that is part of the Capitol Music Group")


    # 17
    # Create a new entry for label.
    #
    def test_post_single_label(self):
        values = dumps({
            "name": "Capitol Records",
            "albums": ["Bad Animals",
                       "Twist and Shout",
                       "Bazooka!!!",
                       "Catarsis",
                       "As Far as Siam",
                       "Honky Tonk"],
            "artists": ["The Beatles",
                        "50_cent",
                        "Avicii",
                        "Red Hot Chili Peppers",
                        "Katy Perry",
                        "The Beach Boys"],
            "founded": 1942,
            "founders": ["Johnny Mercer", "Buddy DeSylva", "Glenn Wallichs"],
            "location": "Los Angeles, CA",
            "map": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726",
            "parent": "Universal Music Group",
            "website": "http://www.capitolrecords.com/",
            "logo": "http://upload.wikimedia.org/wikipedia/en/8/8f/CapitolRecords_Logo.png",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true",
            "twitter": "451168645687697408",
            "description": "Capitol Records is a major American record label that is part of the Capitol Music Group",
            "slideshow_images":["http://www.smartlayover.com/slportal/media/CAP3.jpg",
                                "http://library.umkc.edu/spec-col/ww2/pacifictheater/images/hope6-sm.jpg",
                                "http://upload.wikimedia.org/wikipedia/commons/6/60/Robert_Clary_Capitol_Records_circa_1950.JPG",
                                "http://www.tenhighcoaches.com/images/Capitol%20Records.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/TjMWQff10Vw",
                               "http://www.youtube.com/embed/T2ovD7-yNAY"],
            "citation": "http://en.wikipedia.org/wiki/Capital_Records"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/labels", data=values, headers=headers)

        response = urlopen(request)
        self.assertEqual(response.getcode(), 201)
        #formatting response
        our_response = response.readall().decode("utf-8")
        actual_response = '{ name: \"Capitol Records\"}'
        self.assertEqual(our_response, actual_response)

    # 18
    # Update an existing label
    #
    def test_put_label(self):
        values = dumps({
            "name": "Capitol Records",
            "albums": ["Bad Animals",
                       "Twist and Shout",
                       "Bazooka!!!",
                       "Catarsis",
                       "As Far as Siam",
                       "Honky Tonk"],
            "artists": ["The Beatles",
                        "50_cent",
                        "Avicii",
                        "Red Hot Chili Peppers",
                        "Katy Perry",
                        "The Beach Boys"],
            "founded": 1942,
            "founders": ["Johnny Mercer", "Buddy DeSylva", "Glenn Wallichs"],
            "location": "Los Angeles, CA",
            "map": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3303.6823996225835!2d-118.32631955!3d34.1032758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2bf39303b99b3%3A0x491760938aa0eab!2sCapitol+Records!5e0!3m2!1sen!2sus!4v1395277174726",
            "parent": "Universal Music Group",
            "website": "http://www.capitolrecords.com/",
            "logo": "http://upload.wikimedia.org/wikipedia/en/8/8f/CapitolRecords_Logo.png",
            "facebook": "https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fcapitolrecords&amp;width&amp;height=427&amp;colorscheme=light&amp;show_faces=false&amp;header=true&amp;stream=true&amp;show_border=true",
            "twitter": "451168645687697408",
            "description": "Capitol Records is a major American record label that is part of the Capitol Music Group",
            "slideshow_images":["http://www.smartlayover.com/slportal/media/CAP3.jpg",
                                "http://library.umkc.edu/spec-col/ww2/pacifictheater/images/hope6-sm.jpg",
                                "http://upload.wikimedia.org/wikipedia/commons/6/60/Robert_Clary_Capitol_Records_circa_1950.JPG",
                                "http://www.tenhighcoaches.com/images/Capitol%20Records.jpg"],
            "video_slideshow":["http://www.youtube.com/embed/TjMWQff10Vw",
                               "http://www.youtube.com/embed/T2ovD7-yNAY"],
            "citation": "http://en.wikipedia.org/wiki/Capital_Records"
        })
        headers = {"Content-Type": "application/json"}
        values = values.encode("utf-8")
        request = Request("http://music1.apiary-mock.com/api/labels/{id}", data=values, headers=headers)
        request.get_method = lambda: 'PUT'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)

    # 19
    # Create a new entry for a label.
    #
    def test_delete_labels(self):
        request = Request("http://music1.apiary-mock.com/api/labels/{id}")
        request.get_method = lambda: 'DELETE'
        response = urlopen(request)
        self.assertEqual(response.getcode(), 204)


    # 20
    # List all related albums
    #
    def test_get_labels_albums(self):
        response = urlopen("http://music1.apiary-mock.com/api/labels/{id}/albums")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = \
            [
                {
                    "name": "Twist and Shout",
                    "artists": ["The Beatles"],
                    "year_released": 1964,
                    "labels": ["Capitol Records"]
                }
            ]
        self.assertTrue(expected_response == our_response)


    # 21
    # List all related artists
    #
    def test_get_labels_artists(self):
        response = urlopen("http://music1.apiary-mock.com/api/labels/{id}/artists")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8")
        our_response = loads(our_response)
        expected_response = \
            [
                {
                    "band_name": "The Beatles",
                    "hometown": "Liverpool, England",
                    "years_active": "1960-1970"
                }
            ]
        self.assertTrue(expected_response == our_response)


class TestSearch(TestCase):
    # 1
    # Search single term
    #
    def test_single_search_1(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=lady")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("lady" in our_response)
        self.assertTrue("lady_gaga" in our_response)
        self.assertTrue("the_fame" in our_response)
        self.assertTrue("the_beatles" not in our_response)
        self.assertTrue("2pac" not in our_response)
        self.assertTrue("coldplay" not in our_response)
        self.assertTrue("will_i_am" not in our_response)

    # 2
    # Search single term
    #
    def test_single_search_2(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=beatles")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("beatles" in our_response)
        self.assertTrue("the_beatles" in our_response)
        self.assertTrue("twist_and_shout" in our_response)
        self.assertTrue("lady_gaga" not in our_response)
        self.assertTrue("coldplay" not in our_response)
        self.assertTrue("madonna" not in our_response)

    # 3
    # Search single term
    #
    def test_single_search_3(self):
        query = "somestringthatshouldreallyneverappearanywhereasdkjfsalkjdfsalkjfdsaljfdslkjdfslkdjfsaldkjfskjlueirowqueirxc"
        response = urlopen("http://127.0.0.1:8000/search/?q="+query)
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        self.assertTrue("no results found" in our_response)
        self.assertTrue("list-unstyled" not in our_response)

    # 4
    # Search two terms
    #
    def test_double_search_1(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=lady+gaga")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("lady" in our_response)
        self.assertTrue("gaga" in our_response)
        self.assertTrue("lady_gaga" in our_response)
        self.assertTrue("the_fame" in our_response)
        self.assertTrue("members" not in our_response)
        self.assertTrue("def_jam_recordings" not in our_response)
        self.assertTrue("a_rush_of_blood" not in our_response)

    # 5
    # Search two terms
    #
    def test_double_search_2(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=twist+dre")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("twist" in our_response)
        self.assertTrue("dre" in our_response)
        self.assertTrue("twist_and_shout" in our_response)
        self.assertTrue("dr_dre" in our_response)
        self.assertTrue("aftermath_entertainment" in our_response)
        self.assertTrue("musicdb" not in our_response)
        self.assertTrue("lady_gaga" not in our_response)
        self.assertTrue("cherrytree_records" not in our_response)

    # 6
    # Search two terms
    #
    def test_double_search_3(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=capitol+thisisnotgonnabeanywhere")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("capitol" in our_response)
        self.assertTrue("thisisnotgonnabeanywhere" not in our_response)
        self.assertTrue("capitol_records" in our_response)
        self.assertTrue("twist_and_shout" in our_response)
        self.assertTrue("lady" not in our_response)
        self.assertTrue("RCA" not in our_response)
        self.assertTrue("willpower" not in our_response)

    # 7
    # Search several terms
    #
    def test_multiple_search_1(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=one+two+three+four+five")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("one" in our_response or "two" in our_response or "three" in our_response or "four" in our_response or "five" in our_response)
        self.assertTrue("music expresses that which cannot be put into words" not in our_response)

    # 8
    # Search several terms
    #
    def test_multiple_search_2(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=emi+rated+2pac+cherry")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        our_response = our_response.split("list-unstyled")[1]
        self.assertTrue("emi" in our_response)
        self.assertTrue("rated" in our_response)
        self.assertTrue("2pac" in our_response)
        self.assertTrue("cherry" in our_response)
        self.assertTrue("rated_r" in our_response)
        self.assertTrue("2pacalypse_now" in our_response)
        self.assertTrue("cherrytree_records" in our_response)
        self.assertTrue("musicdb" not in our_response)
        self.assertTrue("will_i_am" not in our_response)
        self.assertTrue("ruthless" not in our_response)

    # 9
    # Search several terms
    #
    def test_multiple_search_3(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=thiswillnotappear+idonthinkthiswillbethere+idontthinkweputthisstringhere")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        self.assertTrue("no results found" in our_response)
        self.assertTrue("list-unstyled" not in our_response)

    # 10
    # Search empty query
    #
    def test_empty_search_1(self):
        response = urlopen("http://127.0.0.1:8000/search/?q=")
        self.assertEqual(response.getcode(), 200)
        our_response = response.readall().decode("utf-8").lower()
        self.assertTrue("no results found" in our_response)
        self.assertTrue("list-unstyled" not in our_response)