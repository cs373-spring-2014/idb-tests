#!/usr/bin/env python3
from django.db import models
# ---------------------------
# Models.py
# Copyright (C) 2014
# Jiarui Hou
# ---------------------------
# Create your models here.

class Type (models.Model):
    """
    Defines the Type of a Pokemon
    """
    name = models.CharField(max_length=50)
    description = models.TextField() 
    image = models.TextField()
    cl = "type"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Pokemon (models.Model):
    """
    Defines 1 type of Pokemon 
    """
    name = models.CharField(max_length=50)
    #A Pokemon can be of 1 or 2 types
    #A Type belongs to many Pokemons
    types = models.ManyToManyField(Type, default = None)
    height = models.FloatField()
    weight = models.FloatField()
    location = models.TextField()
    species = models.CharField(max_length=50)
    description = models.TextField()
    image = models.TextField()
    cl = 'pokemon'
    
    def __str__(self):
        return self.name

    def __unicode__(self):             
        return self.name

class Trainer (models.Model):
    """
    Defines a pokemon Trainer
    """
    name  = models.CharField(max_length=50)
    badge = models.CharField(max_length=50)
    kind = models.CharField(max_length=50)
    location = models.TextField()
    info = models.TextField()
    description = models.TextField()
    image = models.TextField()
    kind = models.CharField(max_length=50)
    pokemon = models.ManyToManyField(Pokemon)
    cl = 'trainer'

    def __str__(self):   
        return self.name  

    def __unicode__(self):   
        return self.name          
