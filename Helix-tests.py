#! usr/bin/env python3

import json, urllib, codecs
import unittest
import re, sys
if (sys.version_info[0] == 2):
  from urllib2 import Request, urlopen
  LOCATION = 'location'      
else:
  from urllib.request import Request, urlopen
  LOCATION = 'Location'      


API_URL = 'http://tranquil-spire-8225.herokuapp.com/api/v1/'
SEARCH_URL = 'http://tranquil-spire-8225.herokuapp.com/search/?q='
SEARCH_ALLRADIO = '&models=idb.pokemon&models=idb.trainer&models=idb.type'

class TestSearch (unittest.TestCase):
  #rock solid
  def test_search_multWords1(self):
    request = Request(SEARCH_URL + "rock+solid" + SEARCH_ALLRADIO)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    searchResults = response.read().decode('utf-8')
    
    #check relevant search results are in string
    assert ("<a href=\"/trainer/3\">Brock</a>" in searchResults)
    assert ("<a href=\"/type/18\">Rock</a>" in searchResults)
    assert ("<a href=\"/pokemon/17\">Ditto</a>" in searchResults)

    #check each result is in correct order
    assert (searchResults.find("<a href=\"/trainer/3\">Brock</a>") < searchResults.find("<a href=\"/type/18\">Rock</a>"))
    assert (searchResults.find("<a href=\"/type/18\">Rock</a>") < searchResults.find("<a href=\"/pokemon/17\">Ditto</a>"))

  #psychic master
  def test_search_multWords2(self):
    request = Request(SEARCH_URL + "psychic+master" + SEARCH_ALLRADIO)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    searchResults = response.read().decode('utf-8')
    
    #check relevant search results are in string
    assert ("<a href=\"/trainer/6\">Sabrina</a>" in searchResults)
    assert ("...<b class=\"highlighted\">Master</b> of <b class=\"highlighted\">Psychic</b> Pok" in searchResults)

    assert ("<a href=\"/type/23\">Psychic</a>" in searchResults)
    assert ("<a href=\"/pokemon/129\">Mewtwo</a>" in searchResults)
    
    #check each result is in correct order
    assert (searchResults.find("<a href=\"/trainer/6\">Sabrina</a>") < searchResults.find("<a href=\"/type/23\">Psychic</a>"))
    assert (searchResults.find("<a href=\"/type/23\">Psychic</a>") < searchResults.find("<a href=\"/pokemon/129\">Mewtwo</a>"))
    assert (searchResults.find("<a href=\"/pokemon/129\">Mewtwo</a>") < searchResults.find("<a href=\"/trainer/10\">Lance</a>"))
    assert (searchResults.find("<a href=\"/trainer/10\">Lance</a>") < searchResults.find("<a href=\"/trainer/9\">Blaine</a>"))
    assert (searchResults.find("<a href=\"/trainer/9\">Blaine</a>") < searchResults.find("<a href=\"/trainer/7\">Koga</a>"))
    assert (searchResults.find("<a href=\"/trainer/7\">Koga</a>") < searchResults.find("<a href=\"/trainer/8\">Giovanni</a>"))
  
  #psychic master
  def test_search_multWords3(self):
    request = Request(SEARCH_URL + "yellow+kakuna" + SEARCH_ALLRADIO)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    searchResults = response.read().decode('utf-8')
    
    #check relevant search results are in string
    assert ("<a href=\"/pokemon/16\">Kakuna</a>" in searchResults)
    assert ("<b class=\"highlighted\">Kakuna</b> is a <b class=\"highlighted\">yellow</b>, cocoon Pokemon. " in searchResults)

    assert ("<a href=\"/pokemon/15\">Beedrill</a>" in searchResults)
    assert ("<a href=\"/pokemon/13\">Abra</a>" in searchResults)
    assert ("<a href=\"/pokemon/18\">Weedle</a>" in searchResults)
    
    #check each result is in correct order
    assert (searchResults.find("<a href=\"/pokemon/16\">Kakuna</a>") < searchResults.find("<a href=\"/pokemon/15\">Beedrill</a>"))
    assert (searchResults.find("<a href=\"/pokemon/15\">Beedrill</a>") < searchResults.find("<a href=\"/pokemon/13\">Abra</a>"))
    assert (searchResults.find("<a href=\"/pokemon/13\">Abra</a>") < searchResults.find("<a href=\"/pokemon/18\">Weedle</a>"))
   
class TestApiary (unittest.TestCase):
  def test_GET_POKEMON(self):
    request = Request(API_URL + "pokemon/")
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    matt = matt["pokemon"].sort(key= lambda x: x["id"])
    #print (matt)
    with codecs.open("./data/pokemon.json",'r','utf-8') as f:
      compare = json.load(f)
      compare = compare["pokemon"].sort(key= lambda x: x["id"])
      assert(compare == matt)

  def test_GET_POKEMON_ID(self):
    request = Request(API_URL + "pokemon/17/")
    response = urlopen(request)

    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )
    matt = json.loads(response.read().decode('utf-8'))
    compare = {
  "description": "In its natural state, Ditto is a light purple or magenta free-form shape blob with vestigial facial features. It also appears to have two vestigial, handless 'arms' protruding from its body. It is capable of transforming into an exact replica of any physical object. However, if Ditto tries to transform into something based on memory, it may get some of the details wrong. The anime has shown that occasionally a Ditto cannot change its face. Ditto will also be unable to remain in a transformed state if it starts laughing. Ditto cannot mimic the strength of another Pokemon, but it will adapt the form and abilities of its foe. When two Ditto meet in the wild, they will attempt to transform into each other. Ditto will also transform into a rock when sleeping to avoid attack. Ditto is never far from civilization or people.",
  "height": 0.3,
  "id": 17,
  "image": "http://cdn.bulbagarden.net/upload/3/36/132Ditto.png",
  "location": "Routes 13, 14, 15, 23, Cerulean Cave",
  "name": "Ditto",
  "species": "Transform",
  "weight": 4.0
}
    assert(compare == matt)

  def test_GET_TRAINER(self):
    request = Request(API_URL + "trainer/")
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    matt = matt["trainer"].sort(key= lambda x: x["id"])
    #print (matt)
    with codecs.open("./data/trainer.json",'r','utf-8') as f:
      compare = json.load(f)
      compare = compare["trainer"].sort(key= lambda x: x["id"])
      assert(compare == matt)

  def test_GET_TRAINER_ID(self):
    request = Request(API_URL + "trainer/10/")
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    compare = {
  "badge": "None",
  "description": "In Generation I and the Generation III remakes, Lance is the fourth and final member of the Elite Four. He must be defeated before facing Blue, the Pokemon League Champion. By the Generation II games, he has proceeded to become Pokemon League Champion, due to Red going to Mt. Silver to train and leaving him his charge as Champion. He raises Dragon-type Pokemon, which are both hard to catch and raise. He prefers the superiority of Dragon-type Pokemon, believing them to be virtually indestructible.\r\nKnown as a cool and heroic Trainer, Lance has a large fan-following. Lance also seems to have a liking for capes, as he is often seen buying identical capes at the Celadon Department Store.\r\nLance's grandfather is the elder of a famous clan of Dragon Masters. Clair is his cousin, and his hometown is Blackthorn City.\r\nIn Generation II and their remakes, the player first encounters Lance at the Lake of Rage where he watches the player battle the Red Gyarados. Lance also appears in Mahogany Town to help the player fight Team Rocket at the Team Rocket HQ. Lance rewards the player with the Whirlpool HM for helping him put an end to Team Rocket's sinister plans.",
  "id": 10,
  "image": "http://cdn.bulbagarden.net/upload/thumb/f/f5/HeartGold_SoulSilver_Lance.png/220px-HeartGold_SoulSilver_Lance.png",
  "info": "Lance  is a Master Dragon-type Trainer, a member of the Kanto Elite Four in Generation I and Generation III and the Pokemon Champion of the Indigo Plateau in Generation II and Generation IV.",
  "kind": "Elite Four",
  "location": "Indigo Plateau",
  "name": "Lance"
}
    assert(compare == matt)

  def test_GET_TYPE(self):
    request = Request(API_URL + "type/")
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    matt = matt["type"].sort(key= lambda x: x["id"])
    #print (matt)
    with codecs.open("./data/type.json",'r','utf-8') as f:
      compare = json.load(f)
      compare = compare["type"].sort(key= lambda x: x["id"])
      assert(compare == matt)

  def test_GET_TYPE_ID(self):
    request = Request(API_URL + "type/17/")
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    compare = {
  "description": "Dragon-type Pokemon are, quite simply, dragons. Their moves involve the use of claws and breath. They are one of only two types, the other being Ghost, to be Super Effective against its own type.",
  "id": 17,
  "image": "http://img4.wikia.nocookie.net/__cb20131123083500/pokemon/images/4/48/DragonIC_Big.png",
  "name": "Dragon"
}
    assert(compare == matt)

  def test_POST_POKEMON(self):
    data = json.dumps({
  "description": "Bulbasaur is a small, quadruped Pokemon with green or bluish green skin and dark patches. Its thick legs each end with three sharp claws. Its eyes have red irises, while the sclera and pupils are white. Bulbasaur has a pair of small, pointed teeth visible when its mouth is open. It has a bulb on its back, grown from a seed planted there at birth. The bulb provides it with energy through photosynthesis as well as from the nutrient-rich seeds contained within. ",
  "height": 0.71,
  "image": "http://cdn.bulbagarden.net/upload/2/21/001Bulbasaur.png",
  "location": "This is a starter Pokemon gotten from Professor Oak.",
  "name": "TEST",
  "species": "Seed",
  "weight": 6.9
})
    headers = {"Content-Type":"application/json"}
    # d = urllib.parse.urlencode(data)
    d = data.encode("UTF-8")
    request = Request(API_URL + "pokemon/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'pokemon/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'pokemon/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    # print (matt)
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

  def test_POST_Trainer(self):
    data = json.dumps({
  "badge": "None",
  "description": "Ash's main goal in life, as described in the first episode, is to be the world's greatest Pokemon Master.\r\n\r\nOne particular characteristic about Ash that has not changed over the course of the series aside from his determination and his passion for raising and training Pokemon is that he is incredibly selfless. He will often (and this has happened for just about every one of his Pokemon) go to extraordinary lengths to earn a Pokemon's trust and respect; and he will often go out of his way to better understand a Pokemon that he sees as troubled. He is also very fair-minded and trusting, a true testament to his good-natured character. ",
  "image": "http://cdn.bulbagarden.net/upload/thumb/0/00/Ash_XY.png/250px-Ash_XY.png",
  "info": "Ash Ketchum is the main character of both the Pokemon anime and The Electric Tale of Pikachu manga, and is believed to be based on the protagonist of the first generation Pokemon games. ",
  "kind": "Trainer",
  "location": "Pallet Town",
  "name": "TEST Ketchum"
})
    headers = {"Content-Type":"application/json"}
    # d = urllib.parse.urlencode(data)
    d = data.encode("UTF-8")
    request = Request(API_URL + "trainer/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'trainer/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'trainer/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    # print (matt)
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )


  def test_POST_Type(self):
    data = json.dumps({
  "description": "Dragon-type Pokemon are, quite simply, dragons. Their moves involve the use of claws and breath. They are one of only two types, the other being Ghost, to be Super Effective against its own type.",
  "image": "http://img4.wikia.nocookie.net/__cb20131123083500/pokemon/images/4/48/DragonIC_Big.png",
  "name": "TEST"
})
    headers = {"Content-Type":"application/json"}
    # d = urllib.parse.urlencode(data)
    d = data.encode("UTF-8")
    request = Request(API_URL + "type/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'type/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'type/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    # print (matt)
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )


  def test_PUT_POKEMON(self):
    # Post some test data
    data = json.dumps({
  "description": "Bulbasaur is a small, quadruped Pokemon with green or bluish green skin and dark patches. Its thick legs each end with three sharp claws. Its eyes have red irises, while the sclera and pupils are white. Bulbasaur has a pair of small, pointed teeth visible when its mouth is open. It has a bulb on its back, grown from a seed planted there at birth. The bulb provides it with energy through photosynthesis as well as from the nutrient-rich seeds contained within. ",
  "height": 0.71,
  "image": "http://cdn.bulbagarden.net/upload/2/21/001Bulbasaur.png",
  "location": "This is a starter Pokemon gotten from Professor Oak.",
  "name": "TEST",
  "species": "Seed",
  "weight": 6.9
})
    headers = {"Content-Type":"application/json"}
    d = data.encode("UTF-8")
    request = Request(API_URL + "pokemon/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    # Get to ensure accuracy
    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'pokemon/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'pokemon/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    # Put for the actual test
    data["location"] = "PUT test"
    d = json.dumps(data)
    d = d.encode("UTF-8")
    request = Request(API_URL + 'pokemon/' + str(post_id) + '/', data=d, headers=headers)
    request.get_method = lambda: 'PUT'
    response = urlopen(request)
    

    assert( response.msg == "NO CONTENT")
    assert( response.getcode() == 204)

    # Get again to verify update
    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('UTF-8'))
    assert(data == matt)

    # Delete entry
    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

  def test_PUT_TRAINER(self):
    # Post some test data
    data = json.dumps({
  "badge": "None",
  "description": "Ash's main goal in life, as described in the first episode, is to be the world's greatest Pokemon Master.\r\n\r\nOne particular characteristic about Ash that has not changed over the course of the series aside from his determination and his passion for raising and training Pokemon is that he is incredibly selfless. He will often (and this has happened for just about every one of his Pokemon) go to extraordinary lengths to earn a Pokemon's trust and respect; and he will often go out of his way to better understand a Pokemon that he sees as troubled. He is also very fair-minded and trusting, a true testament to his good-natured character. ",
  "image": "http://cdn.bulbagarden.net/upload/thumb/0/00/Ash_XY.png/250px-Ash_XY.png",
  "info": "Ash Ketchum is the main character of both the Pokemon anime and The Electric Tale of Pikachu manga, and is believed to be based on the protagonist of the first generation Pokemon games. ",
  "kind": "Trainer",
  "location": "Pallet Town",
  "name": "TEST Ketchum"
})
    headers = {"Content-Type":"application/json"}
    d = data.encode("UTF-8")
    request = Request(API_URL + "trainer/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    # Get to ensure accuracy
    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'trainer/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'trainer/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    # Put for the actual test
    data["location"] = "PUT test"
    d = json.dumps(data)
    d = d.encode("UTF-8")
    request = Request(API_URL + 'trainer/' + str(post_id) + '/', data=d, headers=headers)
    request.get_method = lambda: 'PUT'
    response = urlopen(request)

    assert( response.msg == "NO CONTENT")
    assert( response.getcode() == 204)

    # Get again to verify update
    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('UTF-8'))
    assert(data == matt)

    # Delete entry
    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

  def test_PUT_TYPE(self):
    # Post some test data
    data = json.dumps({
  "description": "Dragon-type Pokemon are, quite simply, dragons. Their moves involve the use of claws and breath. They are one of only two types, the other being Ghost, to be Super Effective against its own type.",
  "image": "http://img4.wikia.nocookie.net/__cb20131123083500/pokemon/images/4/48/DragonIC_Big.png",
  "name": "TEST"
})
    headers = {"Content-Type":"application/json"}
    d = data.encode("UTF-8")
    request = Request(API_URL + "type/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    # Get to ensure accuracy
    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'type/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'type/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    # Put for the actual test
    data["description"] = "PUT test"
    d = json.dumps(data)
    d = d.encode("UTF-8")
    request = Request(API_URL + 'type/' + str(post_id) + '/', data=d, headers=headers)
    request.get_method = lambda: 'PUT'
    response = urlopen(request)

    assert( response.msg == "NO CONTENT")
    assert( response.getcode() == 204)

    # Get again to verify update
    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('UTF-8'))
    assert(data == matt)

    # Delete entry
    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

  def test_DELETE_POKEMON(self):
    # Post some test data
    data = json.dumps({
  "description": "Bulbasaur is a small, quadruped Pokemon with green or bluish green skin and dark patches. Its thick legs each end with three sharp claws. Its eyes have red irises, while the sclera and pupils are white. Bulbasaur has a pair of small, pointed teeth visible when its mouth is open. It has a bulb on its back, grown from a seed planted there at birth. The bulb provides it with energy through photosynthesis as well as from the nutrient-rich seeds contained within. ",
  "height": 0.71,
  "image": "http://cdn.bulbagarden.net/upload/2/21/001Bulbasaur.png",
  "location": "This is a starter Pokemon gotten from Professor Oak.",
  "name": "TEST",
  "species": "Seed",
  "weight": 6.9
})
    headers = {"Content-Type":"application/json"}
    d = data.encode("UTF-8")
    request = Request(API_URL + "pokemon/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    # Get to ensure accuracy
    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'pokemon/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'pokemon/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    # Delete entry
    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

  def test_DELETE_Trainer(self):
    data = json.dumps({
  "badge": "None",
  "description": "Ash's main goal in life, as described in the first episode, is to be the world's greatest Pokemon Master.\r\n\r\nOne particular characteristic about Ash that has not changed over the course of the series aside from his determination and his passion for raising and training Pokemon is that he is incredibly selfless. He will often (and this has happened for just about every one of his Pokemon) go to extraordinary lengths to earn a Pokemon's trust and respect; and he will often go out of his way to better understand a Pokemon that he sees as troubled. He is also very fair-minded and trusting, a true testament to his good-natured character. ",
  "image": "http://cdn.bulbagarden.net/upload/thumb/0/00/Ash_XY.png/250px-Ash_XY.png",
  "info": "Ash Ketchum is the main character of both the Pokemon anime and The Electric Tale of Pikachu manga, and is believed to be based on the protagonist of the first generation Pokemon games. ",
  "kind": "Trainer",
  "location": "Pallet Town",
  "name": "TEST Ketchum"
})
    headers = {"Content-Type":"application/json"}
    # d = urllib.parse.urlencode(data)
    d = data.encode("UTF-8")
    request = Request(API_URL + "trainer/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'trainer/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'trainer/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    # print (matt)
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )


  def test_DELETE_Type(self):
    data = json.dumps({
  "description": "Dragon-type Pokemon are, quite simply, dragons. Their moves involve the use of claws and breath. They are one of only two types, the other being Ghost, to be Super Effective against its own type.",
  "image": "http://img4.wikia.nocookie.net/__cb20131123083500/pokemon/images/4/48/DragonIC_Big.png",
  "name": "TEST"
})
    headers = {"Content-Type":"application/json"}
    # d = urllib.parse.urlencode(data)
    d = data.encode("UTF-8")
    request = Request(API_URL + "type/", data=d, headers=headers)
    response = urlopen(request)
    assert( response.msg == "CREATED")
    assert( response.getcode() == 201)

    loc = dict(response.headers)[LOCATION]
    m = re.search(API_URL + 'type/(\d+)/', loc)
    post_id = int(m.group(1))
    created = API_URL + 'type/' + str(post_id) + '/'

    request = Request(created)
    response = urlopen(request)
    assert( response.msg == 'OK' )
    assert( response.getcode() == 200 )

    matt = json.loads(response.read().decode('utf-8'))
    # print (matt)
    data = json.loads(data)
    data["id"] = post_id
    assert(data == matt)

    request = Request(created)
    request.get_method = lambda: 'DELETE'
    response = urlopen(request)
    response_body = response.read().decode('utf-8')

    assert( response.msg == "NO CONTENT" )
    assert( response.getcode() == 204 )
    assert( response_body == "" )

if __name__ == "__main__":
    print("TestApiary.py")
    unittest.main()
    print("Done.")
